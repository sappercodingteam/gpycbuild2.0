const mongoose = require("mongoose");
const path = require("path");
const User = mongoose.model("User");
const UserInfo = mongoose.model("UserInfo");
const StudentEvent = mongoose.model("StudentEvent");
const CaptainEvent = mongoose.model("CaptainEvent");

const bcrypt = require('bcrypt');
const saltRounds = 10;
const nodemailer = require('nodemailer');
const jwt = require('jsonwebtoken');

module.exports = {
    getAllEvents: (req, res) => {
        let allEvents = [];
        StudentEvent.find({}, (err, studentevents) => {
            if (err) {
                res.json({ err: err });
            } else {
                allEvents = allEvents.concat(studentevents);
                CaptainEvent.find({}, (err, captainevents) => {
                    if (err) {
                        res.json({ err: err });
                    } else {
                        allEvents = allEvents.concat(captainevents);
                        res.json(allEvents);
                    }
                });
            }
        });
    },

    // #EVD550
    deleteEvent: (req, res) => {
        console.log('#EVD551')
        var event_id = req.params.id;
        var identity = req.params.identity;
        if (identity == "captain") {
            CaptainEvent.findOne({ _id: event_id }, (err, captainEvent) => {
                if (err) {
                    console.log('#EV552 ERROR: ', err);
                    res.json({err: '#EV552 ERROR: ', err});
                    } else {
                        User.findOne({_id: captainEvent.created_by}, (err, creator) => {
                            if (err) {
                                console.log('#EV553 ERROR: ', err)
                                res.json({error: '#EV554 ERROR: ', err});
                            } else {
                                CaptainEvent.remove({ _id: event_id }, (err) => {
                                    if (err) {
                                        console.log("#EV555 delete err: ", err);
                                        res.json({error: "#EV555 delete err: ", err});
                                    } else {
                                        var transporter = nodemailer.createTransport({
                                            service: 'gmail',
                                            auth: {
                                                user: 'gpycwsp@gmail.com',
                                                pass: 'WomenRuleGPYC'
                                            }
                                        });
                                    var content = `
                                    <h2>Event Deleted on GPYC Calendar! For ${captainEvent.date}</h2>
                                    <p>Date: ${captainEvent.date}</p>
                                    <p>From Time: ${captainEvent.timeFrom}</p>
                                    <p>To Time: ${captainEvent.timeTo}</p>
                                    <p>Message to the Crew: ${captainEvent.message}</p>
                                    <p>Organizer: ${creator.name}</p>
                                    <p>Organizer Email: ${creator.email}</p>
                                    <p>Organizer Phone Number: ${creator.phone}</p>
                                    <p>There are ${captainEvent.crew.joined_crew} signed up for this event</p> 
                                    <p>Crew on this trip include:</p>
                                    <p>${captainEvent.crew.name1}</p> 
                                    <p>Email (if they included it):${captainEvent.crew.email1} </p> 
                                    <br>    
                                    <p>${captainEvent.crew.name2}</p>
                                    <p>Email (if they included it): ${captainEvent.crew.email2}</p>
                                    <br>
                                    <p>${captainEvent.crew.name3}</p>
                                    <p>Email (if they included it): ${captainEvent.crew.email3}</p>
                                    <br>
                                    <p>${captainEvent.crew.name4}</p>
                                    <p>Email (if they included it): ${captainEvent.crew.email4}</p>
                                    <br>
                                    <p>${captainEvent.crew.name5}</p>
                                    <p>Email (if they included it): ${captainEvent.crew.email5}</p>
                                    <br>
                                    <p>${captainEvent.crew.name6}</p>
                                    <p>Email (if they included it): ${captainEvent.crew.email6}</p>
                                    `;  
                                    var mailOptions = {
                                        from: 'gpycwsp@gmail.com',
                                        to: [captainEvent.crew.email1, captainEvent.crew.email2,  captainEvent.crew.email3, captainEvent.crew.email4, captainEvent.crew.email5, captainEvent.crew.email6],
                                        subject: 'Event Deleted - Great Pond Yacht Club Calendar',
                                        html: content
                                    };
                                    transporter.sendMail(mailOptions, function (error, info) {
                                    if (error) {
                                        console.log('#EV555 ERROR: ', error);
                                        } else {
                                            console.log('#EV556 EMAIL SENT: ' + info.response);
                                            res.redirect(303, '/allevents');
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            } else if (identity == "student") {
                studentEvent.findOne({ _id: event.id }, (err, studentEvent) => {
                    if (err) {
                        console.log('#EV557 ERROR: ', err);
                        res.json({err: '#EV558 ERROR: ', err});
                    } else {
                        User.findOne({_id: studentEvent.created_by}, (err, creator) => {
                            if (err) {
                                console.log('#EV559 ERROR: ', err)
                                res.json({error: '#EV5590 ERROR: ', err});
                                    } else {
                                        studentEvent.remove({ _id: event_id }, (err) => {
                                            if (err) {
                                                console.log("#EV5591 delete err: ", err);
                                            res.json({error: "#EV5592 delete err: ", err});
                                            } else {
                                                var transporter = nodemailer.createTransport({
                                                    service: 'gmail',
                                                    auth: {
                                                        user: 'gpycwsp@gmail.com',
                                                        pass: 'WomenRuleGPYC'
                                                        }
                                                    });
                                                var content = `
                                                <h2>Event Deleted on GPYC Calendar! For ${studentEvent.date}</h2>
                                                <p>Date: ${studentEvent.date}</p>
                                                <p>From Time: ${studentEvent.timeFrom}</p>
                                                <p>To Time: ${studentEvent.timeTo}</p>
                                                <p>Message to the Crew: ${studentEvent.message}</p>
                                                <p>Organizer: ${creator.name}</p>
                                                <p>Organizer Email: ${creator.email}</p>
                                                <p>Organizer Phone Number: ${creator.phone}</p>
                                                <p>There are ${studentEvent.crew.joined_crew} signed up for this event</p> 
                                                <p>Crew on this trip include:</p>
                                                <p>${studentEvent.crew.name1}</p> 
                                                <p>Email (if they included it):${studentEvent.crew.email1} </p> 
                                                <br>    
                                                <p>${studentEvent.crew.name2}</p>
                                                <p>Email (if they included it): ${studentEvent.crew.email2}</p>
                                                <br>
                                                <p>${studentEvent.crew.name3}</p>
                                                <p>Email (if they included it): ${studentEvent.crew.email3}</p>
                                                <br>
                                                <p>${studentEvent.crew.name4}</p>
                                                <p>Email (if they included it): ${studentEvent.crew.email4}</p>
                                                <br>
                                                <p>${studentEvent.crew.name5}</p>
                                                <p>Email (if they included it): ${studentEvent.crew.email5}</p>
                                                <br>
                                                <p>${studentEvent.crew.name6}</p>
                                                <p>Email (if they included it): ${studentEvent.crew.email6}</p>
                                                `;
                                                var mailOptions = {
                                                    from: 'gpycwsp@gmail.com',
                                                    to: [captainEvent.crew.email1, captainEvent.crew.email2,  captainEvent.crew.email3, captainEvent.crew.email4, captainEvent.crew.email5, captainEvent.crew.email6],
                                                    subject: 'Event Deleted - Great Pond Yacht Club Calendar',
                                                    html: content
                                                };
                                                transporter.sendMail(mailOptions, function (error, info) {
                                                if (error) {
                                                    console.log('#EV5593 ERROR: ', error);
                                                } else {
                                                    console.log('#EV5594 EMAIL SENT: ' + info.response);
                                                    res.redirect(303, '/allevents');
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                }, 
    deleteEvents: (req, res) => {
            CaptainEvent.remove({}, (err) => {
                if (err) {
                    console.log("delete err: ", err);
                }
                else {
                    StudentEvent.remove({}, (err) => {
                    res.redirect(303, '/allevents');
                });
        }
    });
},
// #EV500 
    updateEvent: (req, res) => {
        console.log('#EV510 REQ.BODY.DATA: ', req.body.data);
        if (req.body.data.title.includes("Student")) {
            console.log('#EV520 UPDATEEVENT STUDENT');
            StudentEvent.findOne({ _id: req.params.id }, function (err, studentEvent) {
                if (err) {
                    console.log("#EV530 UPDATE EVENT ERR: ", err);
                }
                else {
                    console.log('#EV540 STUDENTEVENT: ', studentEvent);
                    studentEvent.timeFrom = req.body.data.timeFrom;
                    studentEvent.timeTo = req.body.data.timeTo;
                    studentEvent.message = req.body.data.message;
                    studentEvent.crew = req.body.data.crew;
                    studentEvent.save((err) => {
                        if(err) {
                            res.json({error: "#EV550 COULD NOT SAVE STUDENT"})
                            console.log('err: ', err);
                        } else {
                            console.log('#EV560 NO ERROR');
                            User.findOne({created_by: req.body.data.created_by}, (err, login_user) => {
                                if(err) {
                                    res.json({error: "#EV570 COULD NOT FIND THE ORGANIZER ERROR: ", error})
                                    console.log('#EV580 ERR: ', err);
                                } else {
                                    var transporter = nodemailer.createTransport({
                                        service: 'gmail',
                                        auth: {
                                        user: 'gpycwsp@gmail.com',
                                        pass: 'WomenRuleGPYC'
                                        }
                                        });
                                    var content = `
                                        <h2>Event Update on GPYC Calendar! For ${studentEvent.date}</h2>
                                        <p>Date: ${studentEvent.date}</p>
                                        <p>From Time: ${studentEvent.timeFrom}</p>
                                        <p>To Time: ${studentEvent.timeTo}</p>
                                        <p>Message to the Crew: ${studentEvent.message}</p>
                                        <p>Organizer: ${login_user.name}</p>
                                        <p>Organizer Email: ${login_user.email}</p>
                                        <p>Organizer Phone Number: ${login_user.phone}</p>
                                        <p>There are ${req.body.data.crew.joined_crew} signed up for this event</p> 
                                        <p>Crew on this trip include:</p>
                                        <p>${req.body.data.crew.name1}</p> 
                                        <p>Email (if they included it):${req.body.data.crew.email1} </p> 
                                        <br>    
                                        <p>${req.body.data.crew.name2}</p>
                                        <p>Email (if they included it): ${req.body.data.crew.email2}</p>
                                        <br>
                                        <p>${req.body.data.crew.name3}</p>
                                        <p>Email (if they included it): ${req.body.data.crew.email3}</p>
                                        <br>
                                        <p>${req.body.data.crew.name4}</p>
                                        <p>Email (if they included it): ${req.body.data.crew.email4}</p>
                                        <br>
                                        <p>${req.body.data.crew.name5}</p>
                                        <p>Email (if they included it): ${req.body.data.crew.email5}</p>
                                        <br>
                                        <p>${req.body.data.crew.name6}</p>
                                        <p>Email (if they included it): ${req.body.data.crew.email6}</p>
                                        `;  
                                    var mailOptions = {
                                        from: 'gpycwsp@gmail.com',
                                        to: [req.body.data.crew.email1, req.body.data.crew.email2,  req.body.data.crew.email3, req.body.data.crew.email4, req.body.data.crew.email5, req.body.data.crew.email6],
                                        subject: 'Event Update - Great Pond Yacht Club Calendar',
                                        html: content
                                    };
                                    transporter.sendMail(mailOptions, function (error, info) {
                                        if (error) {
                                            console.log('#EV590 ERROR: ', error);
                                        } else {
                                            console.log('#EV591 EMAIL SENT: ' + info.response);
                                            res.redirect(303, '/allevents');
                                        }
                                        })                            
                                    }
                                })
                            }
                        })
                    }
                }) 
            } else if (req.body.data.title.includes("Captain")) {
                console.log('#EV592 UPDATEEVENT STUDENT');
                CaptainEvent.findOne({ _id: req.params.id }, function (err, captainEvent) {
                    if (err) {
                        console.log("update student event err: ", err);
                    }
                    else {
                        console.log('#EV593 studentEvent: ', captainEvent);
                        captainEvent.timeFrom = req.body.data.timeFrom;
                        captainEvent.timeTo = req.body.data.timeTo;
                        captainEvent.message = req.body.data.message;
                        captainEvent.crew = req.body.data.crew;
                        captainEvent.save((err) => {
                            if(err) {
                                res.json({error: "#EV594 Could not save student."})
                                console.log('#EV595 ERR: ', err);
                            } else {
                                console.log('#EV596 NO ERROR');
                                User.findOne({user: req.body.created_by}, (err, login_user) => {
                                    if(err) {
                                        res.json({err: '#EV597: ', err });
                                        console.log('#EV598 ERR: ', err);
                                    } else {
                                        console.log('#EV599 LOGIN_USER: ', login_user);
                                        var transporter = nodemailer.createTransport({
                                            service: 'gmail',
                                            auth: {
                                                user: 'gpycwsp@gmail.com',
                                                pass: 'WomenRuleGPYC'
                                            }
                                        });
                                        var content = `
                                        <h2>Event Update on GPYC Calendar! For ${captainEvent.date}</h2>
                                        <p>Date: ${captainEvent.date}</p>
                                        <p>From Time: ${captainEvent.timeFrom}</p>
                                        <p>To Time: ${captainEvent.timeTo}</p>
                                        <p>Message to the Crew: ${captainEvent.message}</p>
                                        <p>Organizer: ${login_user.name}</p>
                                        <p>Organizer Email: ${login_user.email}</p>
                                        <p>Organizer Phone Number: ${login_user.phone}</p>
                                        <p>There are ${req.body.data.crew.joined_crew} signed up for this event</p> 
                                        <p>Crew on this trip include:</p>
                                        <p>${req.body.data.crew.name1}</p> 
                                        <p>Email (if they included it):${req.body.data.crew.email1} </p> 
                                        <br>    
                                        <p>${req.body.data.crew.name2}</p>
                                        <p>Email (if they included it): ${req.body.data.crew.email2}</p>
                                        <br>
                                        <p>${req.body.data.crew.name3}</p>
                                        <p>Email (if they included it): ${req.body.data.crew.email3}</p>
                                        <br>
                                        <p>${req.body.data.crew.name4}</p>
                                        <p>Email (if they included it): ${req.body.data.crew.email4}</p>
                                        <br>
                                        <p>${req.body.data.crew.name5}</p>
                                        <p>Email (if they included it): ${req.body.data.crew.email5}</p>
                                        <br>
                                        <p>${req.body.data.crew.name6}</p>
                                        <p>Email (if they included it): ${req.body.data.crew.email6}</p>
                                        `;  
                                        var mailOptions = {
                                            from: 'gpycwsp@gmail.com',
                                            to: [req.body.data.crew.email1, req.body.data.crew.email2,  req.body.data.crew.email3, req.body.data.crew.email4, req.body.data.crew.email5, req.body.data.crew.email6],
                                            subject: 'Event Update - Great Pond Yacht Club Calendar',
                                            html: content
                                        };
                                        transporter.sendMail(mailOptions, function (error, info) {
                                            if (error) {
                                                console.log('#EV5991: ', error);
                                            } else {
                                                console.log('#EV5990 EMAIL SENT: ' + info.response);
                                                res.redirect(303, '/allevents');
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
    },

            // #EV100 UPDATE CREW -> SE: ADD_CREW_SUBMIT TS: #CAR1040
    updateCrew: (req, res) => {
        console.log('#EV110 REQ.BODY.DATA (UPDATEEVENT):', req.body.data);
            if (req.body.data.title.includes("Student")) {
                console.log('#EV120 ID AS STUDENT');
                console.log('#EV130 REQ.PARAMS.ID (EVENT): ', req.params.id);
                StudentEvent.findOne({_id: req.params.id}, function (err, studentEvent) {
                    if (err) {
                        res.json('#EV140 CANNOT FIND STUDENT', err);
                        console.log('#EV150 CANNOT FIND STUDENT', err);
                    }
                    else {
                        console.log('#EV160 NO ERROR - STUDENTEVENT: ', studentEvent);
                        if (req.body.data.crew.name1 !== undefined) {
                            studentEvent.crew.name1 = req.body.data.crew.name1;
                            studentEvent.crew.email1 = req.body.data.crew.email1;
                        }
                        if (req.body.data.crew.name2 !== undefined) {
                            studentEvent.crew.name2 = req.body.data.crew.name2;
                            studentEvent.crew.email2 = req.body.data.crew.email2;
                        }
                        if (req.body.data.crew.name3 !== undefined) {
                            studentEvent.crew.name3 = req.body.data.crew.name3;
                            studentEvent.crew.email3 = req.body.data.crew.email3;
                        }
                        if (req.body.data.crew.name4 !== undefined) {
                            studentEvent.crew.name4 = req.body.data.crew.name4;
                            studentEvent.crew.email4 = req.body.data.crew.email4;
                        }
                        if (req.body.data.crew.name5 !== undefined) {
                            studentEvent.crew.name5 = req.body.data.crew.name5;
                            studentEvent.crew.email5 = req.body.data.crew.email5;
                        }
                        if (req.body.data.crew.name6 !== undefined) {
                            studentEvent.crew.name6 = req.body.data.crew.name6;
                            studentEvent.crew.email6 = req.body.data.crew.email6;
                        }
                        if (req.body.data.crew.joined_crew !== undefined) {
                            studentEvent.crew.joined_crew = req.body.data.crew.joined_crew;
                        }
                        studentEvent.save((err) => {
                            if(err) {
                                res.json('#EV170 - CAN NOT SAVE STUDENTEVENT', err);
                                console.log('#EV180 - CAN NOT SAVE STUDENTEVENT - ERROR:', err);
                            } else {
                                console.log('#EV185 STUDENTEVENT SAVED');
                                User.findOne({_id: studentEvent.created_by}, (err, login_user) => {
                                    if(err) {
                                        res.json({error: "#EV190 - COULD NOT FIND ORGANIZER."})
                                        console.log('#EV191 ERR: ', err);
                                    } else {
                                        console.log('#EV192 - USER FOUND - LOGIN_USER: ', login_user);
                                        var transporter = nodemailer.createTransport({
                                            service: 'gmail',
                                            auth: {
                                            user: 'gpycwsp@gmail.com',
                                            pass: 'WomenRuleGPYC'
                                        }
                                    });
                                  var content = `
                                  <h2>Event Update on GPYC Calendar! For ${studentEvent.date}</h2>
                                  <p>Date: ${studentEvent.date}</p>
                                  <p>From Time: ${studentEvent.timeFrom}</p>
                                  <p>To Time: ${studentEvent.timeTo}</p>
                                  <p>Message to the Crew: ${studentEvent.message}</p>
                                  <p>Organizer: ${login_user.name}</p>
                                  <p>Organizer Email: ${login_user.email}</p>
                                  <p>Organizer Phone Number: ${login_user.phone}</p>
                                  <p>There are ${req.body.data.crew.joined_crew} signed up for this event</p> 
                                  <p>Crew on this trip include:</p>
                                  <p>${req.body.data.crew.name1}</p> 
                                  <p>Email (if they included it):${req.body.data.crew.email1} </p> 
                                  <br>    
                                  <p>${req.body.data.crew.name2}</p>
                                  <p>Email (if they included it): ${req.body.data.crew.email2}</p>
                                  <br>
                                  <p>${req.body.data.crew.name3}</p>
                                  <p>Email (if they included it): ${req.body.data.crew.email3}</p>
                                  <br>
                                  <p>${req.body.data.crew.name4}</p>
                                  <p>Email (if they included it): ${req.body.data.crew.email4}</p>
                                  <br>
                                  <p>${req.body.data.crew.name5}</p>
                                  <p>Email (if they included it): ${req.body.data.crew.email5}</p>
                                  <br>
                                  <p>${req.body.data.crew.name6}</p>
                                  <p>Email (if they included it): ${req.body.data.crew.email6}</p>
                                  `;  
                                  var mailOptions = {
                                    from: 'gpycwsp@gmail.com',
                                    to: [req.body.data.crew.email1, req.body.data.crew.email2,  req.body.data.crew.email3, req.body.data.crew.email4, req.body.data.crew.email5, req.body.data.crew.email6],
                                    subject: 'Event Update - Great Pond Yacht Club Calendar',
                                    html: content
                                  };
                                  transporter.sendMail(mailOptions, function (error, info) {
                                    if (error) {
                                      console.log(error);
                                    } else {
                                      console.log('Email sent: ' + info.response);
                                      res.redirect(303, '/allevents');
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
        // #EV200 - UPDATECREW AT CAPTAIN - SE: ADD_CREW_SUBMIT TS: #CAR1040 
     } else if (req.body.data.title.includes("Captain")) {
            console.log('#EV210 CREW');
            CaptainEvent.findOne({ _id: req.params.id }, function (err, captainEvent) {
                if (err) {
                    console.log("#EV220 update student event err: ", err);
                }
                else {
                    captainEvent.crew = req.body.data.crew;
                    // if (req.body.data.crew.email1 !== undefined) {
                    //     captainEvent.crew.name1 = req.body.data.crew.email1;
                    // }
                    // if (req.body.data.crew.name2 !== undefined) {
                    //     captainEvent.crew.name2 = req.body.data.crew.name2;
                    //     captainEvent.crew.email2 = req.body.data.crew.email2;
                    // }
                    // if (req.body.data.crew.name3 !== undefined) {
                    //     captainEvent.crew.name3 = req.body.data.crew.name3;
                    //     captainEvent.crew.email3 = req.body.data.crew.email3;
                    // }
                    // if (req.body.data.crew.name4 !== undefined) {
                    //     captainEvent.crew.name4 = req.body.data.crew.name4;
                    //     captainEvent.crew.email4 = req.body.data.crew.email4;
                    // }
                    // if (req.body.data.crew.name5 !== undefined) {
                    //     captainEvent.crew.name5 = req.body.data.crew.name5;
                    //     captainEvent.crew.email5 = req.body.data.crew.email5;
                    // }
                    // if (req.body.data.crew.name6 !== undefined) {
                    //     captainEvent.crew.name6 = req.body.data.crew.name6;
                    //     captainEvent.crew.email6 = req.body.data.crew.email6;
                    // }
                    captainEvent.save((err) => {
                        if (err) {
                           res.json('#EV230 err: ', err);
                            console.log('#EV240 err: ', err);
                        }
                        else {
                            User.findOne({created_by: req.body.data.created_by}, (err, login_user) => {
                                if(err) {
                                    res.json({error: "#EV250 ERR - Could not find the organizer."})
                                    console.log('#EV260 ERR: ', err);
                                } else {
                                    var transporter = nodemailer.createTransport({
                                        service: 'gmail',
                                        auth: {
                                            user: 'gpycwsp@gmail.com',
                                            pass: 'WomenRuleGPYC'
                                        }
                                    });
                                var content = `
                                    <h2>Event Update on GPYC Calendar! For ${captainEvent.date}</h2>
                                    <p>Date: ${captainEvent.date}</p>
                                    <p>From Time: ${captainEvent.timeFrom}</p>
                                    <p>To Time: ${captainEvent.timeTo}</p>
                                    <p>Message to the Crew: ${captainEvent.message}</p>
                                    <p>Organizer: ${login_user.name}</p>
                                    <p>Organizer Email: ${login_user.email}</p>
                                    <p>Organizer Phone Number: ${login_user.phone}</p>
                                    <p>There are ${req.body.data.crew.joined_crew} signed up for this event</p> 
                                    <p>Crew on this trip include:</p>
                                    <p>${req.body.data.crew.name1}</p> 
                                    <p>Email (if they included it):${req.body.data.crew.email1} </p> 
                                    <br>    
                                    <p>${req.body.data.crew.name2}</p>
                                    <p>Email (if they included it): ${req.body.data.crew.email2}</p>
                                    <br>
                                    <p>${req.body.data.crew.name3}</p>
                                    <p>Email (if they included it): ${req.body.data.crew.email3}</p>
                                    <br>
                                    <p>${req.body.data.crew.name4}</p>
                                    <p>Email (if they included it): ${req.body.data.crew.email4}</p>
                                    <br>
                                    <p>${req.body.data.crew.name5}</p>
                                    <p>Email (if they included it): ${req.body.data.crew.email5}</p>
                                    <br>
                                    <p>${req.body.data.crew.name6}</p>
                                    <p>Email (if they included it): ${req.body.data.crew.email6}</p>
                                    `;  
                                    var mailOptions = {
                                      from: 'gpycwsp@gmail.com',
                                      to: [req.body.data.crew.email1, req.body.data.crew.email2,  req.body.data.crew.email3, req.body.data.crew.email4, req.body.data.crew.email5, req.body.data.crew.email6],
                                      subject: 'Event Update - Great Pond Yacht Club Calendar',
                                      html: content
                                    };
                                    transporter.sendMail(mailOptions, function (error, info) {
                                      if (error) {
                                        res.json('#EV270 ERROR: ', err);
                                        console.log('#EV280 ERROR:', err);
                                      } else {
                                        console.log('#EV290 EMAIL SENT: ' + info.response);
                                        res.redirect(303, '/allevents');
                                      }
                                    })
                                }
                            })
                        }
                    })
                }
            })

        }
    }
}
