const mongoose = require("mongoose");
const path = require("path");
const User = mongoose.model("User");
const UserInfo = mongoose.model("UserInfo");
const StudentEvent = mongoose.model("StudentEvent");
const CaptainEvent = mongoose.model("CaptainEvent");

const bcrypt = require('bcrypt');
const saltRounds = 10;
const nodemailer = require('nodemailer');
const jwt = require('jsonwebtoken');

module.exports = {
    // #ST320 - CREATE STUDENT EVENT 
    create: (req, res) => {
        console.log('#ST321 - REQ.BODY (ID, EVENT): ', req.body);
        User.findOne({_id: req.body.id}, (err, user) => {
            let studentEvent = new StudentEvent(req.body.event);
            studentEvent.created_by = user._id;
            user.created_events.push(studentEvent);
            studentEvent.save((err)=>{
                if(err){
                    console.log('#ST322 CANNOT SAVE NEW EVENT: ', err);
                    res.json('#ST323 CANNOT SAVE NEW EVENT: ', err);
                    } else {
                        user.save((err)=>{
                            if(err){
                                console.log('#ST324 CANNOT SAVE USER: ', err)
                                res.json('#ST325 CAN NOT SAVE USER: ', err);
                            } else {
                                console.log('#ST326 NO ERROR');
                                User.findOne({created_by: req.body.event.created_by}, (err, login_user) => {
                                    if(err) {
                                        console.log('#ST327 CANNOT FIND ORGANIZER ERR: ', err);
                                        res.json('#ST328 CANNOT FIND ORGANIZER ERR:: ', err);
                                    } else {
                                        console.log('#329 - CAN FIND - LOGIN_USER: ', login_user);
                                        var transporter = nodemailer.createTransport({
                                            service: 'gmail',
                                            auth: {
                                                user: 'gpycwsp@gmail.com',
                                                pass: 'WomenRuleGPYC'
                                            }
                                        });
                                        var content = `
                                        <h2>Event Created on GPYC Calendar! For ${studentEvent.date}</h2>
                                        <p>Event Title: ${studentEvent.title}</p>
                                        <p>Date: ${studentEvent.date}</p>
                                        <p>From Time: ${studentEvent.timeFrom}</p>
                                        <p>To Time: ${studentEvent.timeTo}</p>
                                        <p>Message to the Crew: ${studentEvent.message}</p>
                                        <p>Organizer: ${login_user.name}</p>
                                        <p>Organizer Email: ${login_user.email}</p>
                                        <p>Organizer Phone Number: ${login_user.phone}</p>
                                        <p>There are ${req.body.event.crew.joined_crew} signed up for this event</p> 
                                        <p>Crew on this trip include:</p>
                                        <p>${req.body.event.crew.name1}</p> 
                                        <p>Email (if they included it): ${req.body.event.crew.email1} </p> 
                                        <br>    
                                        <p>${req.body.event.crew.name2}</p>
                                        <p>Email (if they included it): ${req.body.event.crew.email2}</p>
                                        <br>
                                        <p>${req.body.event.crew.name3}</p>
                                        <p>Email (if they included it): ${req.body.event.crew.email3}</p>
                                        <br>
                                        <p>${req.body.event.crew.name4}</p>
                                        <p>Email (if they included it): ${req.body.event.crew.email4}</p>
                                        <br>
                                        <p>${req.body.event.crew.name5}</p>
                                        <p>Email (if they included it): ${req.body.event.crew.email5}</p>
                                        <br>
                                        <p>${req.body.event.crew.name6}</p>
                                        <p>Email (if they included it): ${req.body.event.crew.email6}</p>
                                        `;  
                                        var mailOptions = {
                                          from: 'gpycwsp@gmail.com',
                                          to: [req.body.event.crew.email1, req.body.event.crew.email2,  req.body.event.crew.email3, req.body.event.crew.email4, req.body.event.crew.email5, req.body.event.crew.email6],
                                          subject: 'New Seeking a Vessel Event - Great Pond Yacht Club Calendar',
                                          html: content
                                        };
                                        transporter.sendMail(mailOptions, function (error, info) {
                                            if (error) {
                                                console.log('#ST3291 TRANS ERROR: ', error);
                                                } else {
                                                console.log('#ST3292 Email sent: ' + info.response);
                                                res.redirect(303, '/allevents');
                                            }
                                        })
                                    }
                                })
                            }
                        });
                    }
                });
            });
        },
    };