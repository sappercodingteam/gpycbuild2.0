// #CP
const mongoose = require("mongoose");
const path = require("path");

const User = mongoose.model("User");
const UserInfo = mongoose.model("UserInfo");
const studentEvent = mongoose.model("StudentEvent");
const CaptainEvent = mongoose.model("CaptainEvent");

const bcrypt = require('bcrypt');
const saltRounds = 10;
const nodemailer = require('nodemailer');
const jwt = require('jsonwebtoken');

module.exports = {
    create: (req, res) => {
        User.findOne({ _id: req.body.id }, (err, user) => {
                let captainEvent = new CaptainEvent(req.body.event);
                captainEvent.created_by = user._id;
                user.created_events.push(captainEvent);
                    captainEvent.save((err) => {
                        if (err) {
                            console.log('#CP410 CAN NOT SAVE CAPTAIN EVENT: ', err);
                            res.json('#CP420 CAN NOT SAVE CAPTAIN EVENT');
                        } else {
                            user.save((err) => {
                                if(err){
                                    console.log('#ST324 CANNOT SAVE USER: ', err)
                                    res.json('#ST325 CAN NOT SAVE USER: ', err);
                                } else {
                                    console.log('#CP326 NO ERROR');
                                    User.findOne({created_by: req.body.event.created_by}, (err, login_user) => {
                                        if(err) {
                                            console.log('#CP327 CANNOT FIND ORGANIZER ERR: ', err);
                                            res.json('#CP328 CANNOT FIND ORGANIZER ERR:: ', err);
                                            } else {
                                                console.log('#329 - CAN FIND - LOGIN_USER: ', login_user);
                                                var transporter = nodemailer.createTransport({
                                                    service: 'gmail',
                                                    auth: {
                                                        user: 'gpycwsp@gmail.com',
                                                        pass: 'WomenRuleGPYC'
                                                        }
                                                    });
                                                    var content = `
                                                    <h2>Event Created on GPYC Calendar! For ${captainEvent.date}</h2>
                                                    <p>Event Title: ${captainEvent.title}</p>
                                                    <p>Date: ${captainEvent.date}</p>
                                                    <p>From Time: ${captainEvent.timeFrom}</p>
                                                    <p>To Time: ${captainEvent.timeTo}</p>
                                                    <p>Message to the Crew: ${captainEvent.message}</p>
                                                    <p>Organizer: ${login_user.name}</p>
                                                    <p>Organizer Email: ${login_user.email}</p>
                                                    <p>Organizer Phone Number: ${login_user.phone}</p>
                                                    <p>There are ${req.body.event.crew.joined_crew} signed up for this event</p> 
                                                    <p>Crew on this trip include:</p>
                                                    <p>${req.body.event.crew.name1}</p> 
                                                    <p>Email (if they included it): ${req.body.event.crew.email1} </p> 
                                                    <br>    
                                                    <p>${req.body.event.crew.name2}</p>
                                                    <p>Email (if they included it): ${req.body.event.crew.email2}</p>
                                                    <br>
                                                    <p>${req.body.event.crew.name3}</p>
                                                    <p>Email (if they included it): ${req.body.event.crew.email3}</p>
                                                    <br>
                                                    <p>${req.body.event.crew.name4}</p>
                                                    <p>Email (if they included it): ${req.body.event.crew.email4}</p>
                                                    <br>
                                                    <p>${req.body.event.crew.name5}</p>
                                                    <p>Email (if they included it): ${req.body.event.crew.email5}</p>
                                                    <br>
                                                    <p>${req.body.event.crew.name6}</p>
                                                    <p>Email (if they included it): ${req.body.event.crew.email6}</p>
                                                    `;  
                                                    var mailOptions = {
                                                      from: 'gpycwsp@gmail.com',
                                                      to: [req.body.event.crew.email1, req.body.event.crew.email2,  req.body.event.crew.email3, req.body.event.crew.email4, req.body.event.crew.email5, req.body.event.crew.email6],
                                                      subject: 'New Seeking a Crew Event - Great Pond Yacht Club Calendar',
                                                      html: content
                                                    };
                                                    transporter.sendMail(mailOptions, function (error, info) {
                                                        if (error) {
                                                            console.log('#CP3291 TRANS ERROR: ', error);
                                                            } else {
                                                            console.log('#CP3292 Email sent: ' + info.response);
                                                            res.redirect(303, '/allevents');
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    });
                                }
                            });
                        }); 
                    },
                };