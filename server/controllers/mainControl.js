const mongoose = require("mongoose");
const path = require("path");

const User = mongoose.model("User");
const RegCode = mongoose.model("RegCode")
const UserInfo = mongoose.model("UserInfo");
const StudentEvent = mongoose.model("StudentEvent");
const CaptainEvent = mongoose.model("CaptainEvent");

const bcrypt = require('bcrypt');
const saltRounds = 10;
const nodemailer = require('nodemailer');
const jwt = require('jsonwebtoken');
const secret = "captain";

module.exports = {


    getRegcode: (req, res) => {
       let allRegCodes = [];
       RegCode.find({}, (err, regcodes) => {
           if (err) {
               res.json({ err: err });
           } else {
               allRegCodes = allRegCodes.concat(regcodes);
               res.json(allRegCodes);
           }
       });
    },
// MS1300 UPDATEREGCODE 
    updateRegCode: (req, res) => {
      console.log('MS1300 REQ.BODY.REGCODE:', req.body.regcode)   
      var regcode = new RegCode({
        regcode: req.body.regcode.oldregcode
      });
      RegCode.findOne({}, function (err, currentRegcode) {
        if (err) {
          console.log('MS1301 REGCODE ERR: ', err);
        } else {
          console.log("MS1302 CURRENTREGCODE: ", currentRegcode);
          if (currentRegcode.regcode === req.body.regcode.oldregcode) {
            currentRegcode.regcode = req.body.regcode.newregcode;
          } else {
            res.json({err: 'That is not what we have as the current regcode.'})
          }
          currentRegcode.save((err) => {
            if(err) {
              console.log('MS1302 ERR: ', err);
            } else {
              console.log('MS1303 NO ERROR');
              res.redirect(303, '/allregcodes');
            }
          });
        }
      })
    },

    regcode: (req, res) => {
      var regcode = req.params.regcode;
      var regcode = new RegCode({
        regcode: regcode
      })
      regcode.save((err) => {
        if (err) {
          console.log('err', err);
          res.json('Can not save regcode');
        } else {
          res.redirect(303, '/allregcodes');
        }
      });
    },
    deleteRegcode: (req, res) => {
      console.log('deleteRegcode: ', req.params.regcode);
      var regcode = req.params.regcode;
      RegCode.remove({}, (err) => {
          if (err) {
            console.log("delete regcode err: ", err);
          }
          else {
            console.log('SUCCESSFUL DELETE');
            res.redirect(303, '/allregcodes');
          }
        })
     },

    register: (req, res) => {
      console.log('REQ BODY: ', req.body);
      var salt = bcrypt.genSaltSync(saltRounds);
      RegCode.findOne({}, (err, resp) => {
          console.log('RESP: ', resp);
          if (req.body.regcode !== resp.regcode) {  
          res.json({error: "This code is incorrect."});
        } else {
        User.findOne({email: req.body.email}, (err, user) => {
        if(err) {
          console.log('register err: ', err);
        } else {
          if (user === null) {
            var hashed_password = bcrypt.hashSync(req.body.password, salt);
          
            if(req.body.identity === "captain") {
              var new_user = new User({
                name: req.body.name,
                identity: req.body.identity,
                email: req.body.email,
                phone: req.body.phone,
                address: req.body.address,
                experience: req.body.experience,
                boat_name: req.body.boat_name,
                spec: req.body.spec,
                statud: '0'
              });
            }
            else {
              var new_user = new User({
                name: req.body.name,
                identity: req.body.identity,
                email: req.body.email,
                phone: req.body.phone,
                status: '0'
              });
            }
      
            new_user.token = jwt.sign({ email: new_user.email }, secret);
            new_user.save((err) => {
              if(err) {
                console.log("new user save err: ", err);
              }
              else {
                var userinfo = new UserInfo({
                  password: hashed_password,
                  user: new_user._id
                });
                
                userinfo.save((err) => {
                  if(err) {
                    console.log("userinfo save err: ", err);
                  }
                  else {
                    var transporter = nodemailer.createTransport({
                      service: 'gmail',
                      auth: {
                        user: 'gpycwsp@gmail.com',
                        pass: 'WomenRuleGPYC'
                      }
                    });

                    var content = `
                      <h2>Hello ${new_user.name},</h2>
                      <p>Your phone number on file is: ${new_user.phone}</p>
                      <p>You are registered as a: ${new_user.identity}.</p>
                      <p>All emails will be sent to: ${new_user.email}.</p>
                      <p>You have opened a new account in Great Pond Yacht Club. </p><br>
                      <a href="https://greatpondyachtclubwsp.com/activate/${new_user.token}">Please activate your account here: Activate</a>
                      <h3>Greatpondyachtclub team</h3>
                      `
                      // greatpondyachtclubwsp
                      // http://localhost:8000/activate/${new_user.token}
                      // https://greatpondyachtclubwsp.com/activate/${new_user.token}
                    var mailOptions = {
                      from: 'gpycwsp@gmail.com',
                      to: new_user.email,
                      subject: 'Your new Greatpondyachtclub account',
                      html: content
                    };
          
                    transporter.sendMail(mailOptions, function (error, info) {
                      if (error) {
                        console.log(error);
                      } else {
                        console.log('Email sent: ' + info.response);
                      }
                    });
                    res.json({success: "register pending"});
                  }
                })
                
              }
            })
          }
          else {
            res.json({error: "This email has been registered."});
          }
        }
      })
    }
    });
  },

    activate: (req, res) => {
      console.log('ACTIVATE: ', req.params.token);
      User.findOne({token: req.params.token}, (err, user) => {
        if(err) {
          console.log("activate err: ", err);
        }
        else {
          user.status = 1;
          user.save((err) => {
            res.json({user: user});
            var mailToOwner = nodemailer.createTransport({
              service: 'gmail',
              auth: {
                user: 'gpycwsp@gmail.com',
                pass: 'WomenRuleGPYC'
              }
            });

            var content = `
                      <h1>There is a new user account just activated!</h1>
                      <p>User Name: ${user.name}</p>
                      <p>Identity: ${user.identity}</p>
                      `
            var mailOptions = {
              from: 'gpycwsp@gmail.com',
              to: "gpycwsp@gmail.com",
              subject: 'A New User Account Activated!',
              html: content
            };

            mailToOwner.sendMail(mailOptions, function (error, info) {
              if (error) {
                console.log(error);
              } else {
                console.log('Email sent: ' + info.response);
              }
            });
          })
          
        }
      })
    },

    login: (req, res) => {
      User.findOne({email: req.body.email}, (err, login_user) => {
        if(err) {
          console.log("err from login: ", err);
        }
        else {
          if(login_user === null) {
            res.json({error: "Your Email is invalid. Please try again."})
          }
          else if(login_user.status !== 1) {
            res.json({error: "Please activate your account by email.", errorCode: 404});
          }
          else {
            UserInfo.findOne({user: login_user._id}, (err, user) => {
              if(err) {
                console.log("user info err: ", err);
              }
              else {
                bcrypt.compare(req.body.password, user.password, (err, resp) => {
                  if(resp === true) {
                    res.json(login_user);
                  }
                  else {
                    res.json({error: "Your Password is invalid. Please try again."})
                  }
                })
              }
            })
          }
        }
    }
    )}, 
    getAllUsers: (req, res) => {
      let allUsers = [];
      User.find({}, (err, users) => {
        if(err) {
          res.json({err: err});
        } else {
          allUsers = allUsers.concat(users);
          res.json(allUsers);
        }
      });
    },
    deleteUser: (req, res) => {
      var user_email = req.params.email;
      console.log('user_email: ', user_email);
      User.remove({ email: user_email }, (err) => {
          if (err) {
            console.log("delete user err 2: ", err);
          }
          else {
            res.redirect(303, '/alllogin');
          }
        })
     },
     deleteUsers: (req, res) => {
      console.log('user_email: ', req.body);
      User.remove({}, (err) => {
          if (err) {
            console.log("delete user err 2: ", err);
          }
          else {
            res.redirect(303, '/alllogin');
          }
        })
     },

    //  #MC910
     forgetpw: (req, res) => {
      console.log('#MC911 REQ.BODY: ', req.body)
      User.findOne({email: req.body.email }, function (err, user) {
        if (err) {
          console.log('#SE912 ERR:', err);
          res.json({error: '#SE913 ERR', err})
        } else {
          if (!user) {
            res.json("#SE913 THIS EMAIL IS INVALID.");
            res.json({err: "#SE914 THIS EMAIL IS INVALID."});
          } else {
            var transporter = nodemailer.createTransport({
              service: 'gmail',
              auth: {
                user: 'gpycwsp@gmail.com',
                pass: 'WomenRuleGPYC'
              }
            });
  
            var content = `
              Hello,<br><br> You recently requested a password reset link. Please click on the link below to reset your password: <br><br>
              <a href="https://greatpondyachtclubwsp.com/reset/${user.token}">Reset Password</a>
              `
            var mailList = [
              user.email
            ]
  
            var mailOptions = {
              from: 'gpycwsp@gmail.com',
              to: mailList,
              subject: 'Rest Password Link Request',
              html: content
            };
  
            transporter.sendMail(mailOptions, function (error, info) {
              if (error) {
                console.log(error);
              } else {
                console.log('#MC915: Email sent: ' + info.response);
                res.json({succ: "#MC916: success send recovery link."})
              }
            });
          }
        }
      })  
    },

    resetpw(req, res){
      User.findOne({ token: req.body.token }, function(error, user) {
        UserInfo.findOne({user:user._id}, function(err, userInfo){
          if(err){
            console.log("find user info err",err);
          }else{
            var salt = bcrypt.genSaltSync(saltRounds);
            var hashed_password = bcrypt.hashSync(req.body.password, salt);
            userInfo.password = hashed_password;
            userInfo.save((err)=>{
              if(err){
                console.log(err);
              }else{
                res.json("success reset password")
              }
            })
          }
        })

      })
    }
  } 
