var mainroutes = require('../controllers/mainControl.js');
var studentEvent = require('../controllers/studentController');
var captainEvent = require('../controllers/captainController');
var events = require('../controllers/eventController');
var path = require('path');

module.exports = function(app){
  //register
  app.post("/register", function (req, res) {
    mainroutes.register(req, res);
  });
  //login
  app.post("/login", function (req, res) {
    mainroutes.login(req, res);
  });

// Get all users
  app.get('/alllogin', function(req, res) {
    mainroutes.getAllUsers(req, res);
  });

  app.get('/allregcodes', function(req, res) {
    console.log('allregcodes');
    mainroutes.getRegcode(req, res); 
  });

  app.post("/regcode/:regcode", function(req, res) {
    mainroutes.regcode(req, res);
  });
  app.delete("/deleteregcode/:regcode", function(req, res) {
    mainroutes.deleteRegcode(req, res);
  });
// RO1300 UPDATE REGCODE 
  app.put("/regcodeupdate", function(req, res) {
    console.log('RO1300, REQ.BODY.REGCODE: ', req.body.regcode);
    mainroutes.updateRegCode(req, res);
  });

//delete user
    app.delete("/deleteUser/", function(req, res) {
      console.log("APP.DELETE");
      mainroutes.deleteUsers(req, res);
    });

    app.delete("/deleteUsers", function(req, res) {
      console.log("APP.DELETE");
      mainroutes.deleteUsers(req, res);
    });

    //delete user
    app.delete("/deleteUser/:email", function(req, res) {
      console.log("APP.DELETE");
      mainroutes.deleteUser(req, res);
    });

    app.delete("/deleteUsers", function(req, res) {
      console.log("APP.DELETE");
      mainroutes.deleteUsers(req, res);
    });


  app.get("/activate_new/:token", function(req, res) {
    mainroutes.activate(req, res);
  });

  app.delete("/delete/:id/:identity", function(req, res) {
    events.deleteEvent(req, res);
  });


  // #RO320  - CREATE STUDENT EVENT
  app.post('/studentevents', function(req, res){
    studentEvent.create(req, res);
  });

  // #RO410
  app.post('/captainevents', function(req, res){
    captainEvent.create(req, res);
  });


  // get all events
  app.get('/allevents', function(req, res){
    events.getAllEvents(req, res);
  });

  //delete events
  app.delete("/delete/:id/:identity", function(req, res) {
    events.deleteEvent(req, res);
  })
  //delete events
  app.delete("/deleteEvents", function(req, res) {
    events.deleteEvents(req, res);
  })


  //#RO500 UPDATE EVENT
  app.put("/update/:id", function (req, res) {
    events.updateEvent(req, res);
  })

  // #ROP100 UPDATE CREW  -> EV: #EV100, SE: ADD_CREW_SUBMIT TS: #CAR1040 //
  app.put("/updatecrew/:id", function (req, res) {
    events.updateCrew(req, res);
  })

  app.all("*", function (req, res) {
    res.sendFile('index.html', { root: './client/dist' });
  })
  // #R910
    app.post('/forgetpw',(req, res) => {
    console.log('#RO910')
    mainroutes.forgetpw(req, res);
  })

  app.put('/resetpw',(req, res) =>{
    mainroutes.resetpw(req, res);
  })
};

