import { Component, OnInit } from '@angular/core';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { MainService } from '../main.service';

import * as moment from 'moment';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {
  date_display;
  spotsAvailable: '';
  crewlock = {
    crew2_lock: false,
    captain_lock: false
  };
  eventUpdate = {
    date: null,
    title: null,
    timeFrom: null,
    timeTo: null,
    vessel: null,
    spec: null,
    message: null,
    NumOfCrew: null,
    identity: null,
    crew: {
      name1: undefined,
      email1: undefined,
      name2: undefined,
      email2: undefined,
      name3: undefined,
      email3: undefined,
      name4: undefined,
      email4: undefined,
      name5: undefined,
      email5: undefined,
      name6: undefined,
      email6: undefined,
      joined_crew: null,
    }
  };
  crew = {
    name1: undefined,
    email1: undefined,
    name2: undefined,
    email2: undefined,
    name3: undefined,
    email3: undefined,
    name4: undefined,
    email4: undefined,
    name5: undefined,
    email5: undefined,
    name6: undefined,
    email6: undefined,
    joined_crew: null,
  };
  user_data = {
    _id: null
  };
  event_created_by;
  event_id;
  student = {
    title: '',
    date: null,
    timeFrom: null,
    timeTo: null,
    message: '',
    crew: {
      name1: '',
      email1: '',
      name2: '',
      email2: '',
      name3: '',
      email3: '',
      name4: '',
      email4: '',
      name5: '',
      email5: '',
      name6: '',
      email6: '',
      joined_crew: '',
    }
  };
  captain = {
    date: null,
    title: '',
    timeFrom: null,
    timeTo: null,
    vessel: '',
    spec: '',
    NumOfCrew: '',
    message: '',
    crew: {
      name1: '',
      email1: '',
      name2: '',
      email2: '',
      name3: '',
      email3: '',
      name4: '',
      email4: '',
      name5: '',
      email5: '',
      name6: '',
      email6: '',
      joined_crew: null,
    }
  };
  loginUser = 'none';
  logged_user = 'none';
  logged_userId = 'none';
  logged_email = 'none';
  logged_identity = 'none';
  crewemail = '';
  crewname  = '';
  saveTime = {
    start: [],
    to: []
  };

  constructor(private atp: AmazingTimePickerService, private _service: MainService) { }

  ngOnInit() {
    if (this._service.currentUser !== null) {
      this.logged_user = this._service.currentUser.name;
      this.logged_email = this._service.currentUser.email;
      this.logged_userId = this._service.currentUser.id;
      this.logged_identity = this._service.currentUser.identity;
      console.log('THIS.LOGGED_IDENTITY AT INIT: ', this.logged_identity);
    }

    this._service.getAllEvents((res) => {
      this.display_calendar(res);
    });
    this.crewlock.crew2_lock = false;

    this._service.loginstatus.subscribe(
      (data) => {
        console.log('data: ', data);

        if (data[0].user != null) {
          this.user_data = data[0].user;
          this.loginUser = data[0].user.identity;
        }
        if (data[0].user == null) {
          this.loginUser = 'none';
        }
      });
    console.log('loginuser', this.loginUser);

  }

  update_event(events) {
    $('#calendar').fullCalendar('removeEvents');
    $('#calendar').fullCalendar('addEventSource', events);
    $('#calendar').fullCalendar('rerenderEvents');
  }
// #CAT300 SEEKING A VESSEL SUBMIT
  student_submit() {
    if (this.loginUser === 'student') {
      console.log('#CAT310 - LOGINUSER IS STUDENT - THIS._SERVICE.CURRENTUSER: ', this._service.currentUser);
      this.student.title = 'Student ' + this._service.currentUser.name + ' Seeking Vessel!';
      this.student.crew.name1 = this.crew.name1,
      this.student.crew.email1 = this.crew.email1,
      this.student.crew.name2 = this.crew.name2,
      this.student.crew.email2 = this.crew.email2,
      this.student.crew.name3 = this.crew.name3,
      this.student.crew.email3 = this.crew.email3,
      this.student.crew.name4 = this.crew.name4,
      this.student.crew.email4 = this.crew.email4,
      this.student.crew.name5 = this.crew.name5,
      this.student.crew.email5 = this.crew.email5,
      this.student.crew.name6 = this.crew.name6,
      this.student.crew.email6 = this.crew.email6,
      this.student.crew.joined_crew = this.crew.joined_crew,
      console.log('#CAT320 THIS.STUDENT: ', this.student);
      this._service.createStudentEvent(this.student, (res) => {
        console.log('#CAT330 RES: ', res);
        this.closeModal();
        this.update_event(res);
      });
    } else {
        this.captain.title =  'Captain ' + this._service.currentUser.name + ' seeking Vessel!';
        this.captain.timeFrom = this.student.timeFrom;
        this.captain.timeTo = this.student.timeTo;
        this.captain.vessel = '';
        this.captain.spec = '';
        this.captain.message = this.student.message;
        this.captain.NumOfCrew = '';
        this.captain.crew.name1 = this.crew.name1,
        this.captain.crew.email1 = this.crew.email1,
        this.captain.crew.name2 = this.crew.name2,
        this.captain.crew.email2 = this.crew.email2,
        this.captain.crew.name3 = this.crew.name3,
        this.captain.crew.email3 = this.crew.email3,
        this.captain.crew.name4 = this.crew.name4,
        this.captain.crew.email4 = this.crew.email4,
        this.captain.crew.name5 = this.crew.name5,
        this.captain.crew.email5 = this.crew.email5,
        this.captain.crew.name6 = this.crew.name6,
        this.captain.crew.email6 = this.crew.email6,
        this.captain.crew.joined_crew = this.crew.joined_crew,
        console.log('#CAT340 THIS.CAPTAIN: ', this.captain);
        console.log('#CAT350 THIS.CREW: ', this.crew);
        this._service.createCaptainEvent(this.captain, (res) => {
          console.log('#CAT360 RES: ', res);
          this.closeModal();
          this.update_event(res);
          });
        }
      }
      // #CAT400
  captain_submit() {
    this.captain.crew.name1 = this.crew.name1,
    this.captain.crew.email1 = this.crew.email1,
    this.captain.crew.name2 = this.crew.name2,
    this.captain.crew.email2 = this.crew.email2,
    this.captain.crew.name3 = this.crew.name3,
    this.captain.crew.email3 = this.crew.email3,
    this.captain.crew.name4 = this.crew.name4,
    this.captain.crew.email4 = this.crew.email4,
    this.captain.crew.name5 = this.crew.name5,
    this.captain.crew.email5 = this.crew.email5,
    this.captain.crew.name6 = this.crew.name6,
    this.captain.crew.email6 = this.crew.email6,
    this.captain.crew.joined_crew = this.crew.joined_crew,
    console.log('#CAT410 THIS.STUDENT: ', this.captain);
    this.captain.title = 'Captain ' + this._service.currentUser.name + ' Seeking Crew!';
    this._service.createCaptainEvent(this.captain, (res) => {
      console.log('#CAT420 RES: ', res);
      this.closeModal();
      this.update_event(res);
    });
  }
  closeModal() {
    $('.modal').fadeOut();
    this.student = {
      title: '',
      date: null,
      timeFrom: null,
      timeTo: null,
      message: '',
      crew: {
        name1: '',
        email1: '',
        name2: '',
        email2: '',
        name3: '',
        email3: '',
        name4: '',
        email4: '',
        name5: '',
        email5: '',
        name6: '',
        email6: '',
        joined_crew: null,
      }
    };
    this.captain = {
      date: null,
      title: '',
      timeFrom: null,
      timeTo: null,
      vessel: '',
      spec: '',
      NumOfCrew: '',
      message: '',
      crew: {
        name1: '',
        email1: '',
        name2: '',
        email2: '',
        name3: '',
        email3: '',
        name4: '',
        email4: '',
        name5: '',
        email5: '',
        name6: '',
        email6: '',
        joined_crew: null,
      }
    };
    this.saveTime = {
      start: [],
      to: []
    };
  }

  timeFrom() {
    const amazingTimePicker = this.atp.open();
    amazingTimePicker.afterClose().subscribe(time => {
      console.log(time);
      const parts = time.match(/(\d+)\:(\d+)/);
      this.saveTime.start[0] = time;
      this.saveTime.start[1] = parseInt(parts[1]) * 60 + parseInt(parts[2]);
      this.saveTime.start[2] = parseInt(parts[1]);
      this.saveTime.start[3] = parseInt(parts[2]);
      if (this.saveTime.to[1] === undefined || this.saveTime.start[1] <= this.saveTime.to[1]) {
        this.student.timeFrom = time;
        this.captain.timeFrom = time;
        this.eventUpdate.timeFrom = time;
      }
    });
  }
  timeTo() {
    const amazingTimePicker = this.atp.open();
    amazingTimePicker.afterClose().subscribe(time => {
      console.log(time);
      const parts = time.match(/(\d+)\:(\d+)/);
      this.saveTime.to[0] = time;
      this.saveTime.to[1] = parseInt(parts[1]) * 60 + parseInt(parts[2]);
      this.saveTime.to[2] = parseInt(parts[1]);
      this.saveTime.to[3] = parseInt(parts[2]);
      if (this.saveTime.start[1] === undefined || this.saveTime.start[1] <= this.saveTime.to[1]) {
        this.student.timeTo = time;
        this.captain.timeTo = time;
        this.eventUpdate.timeTo = time;
      }
    });
  }
  captainplus() {
    this.crew.name1 = this.logged_user;
    this.crew.email1 = this.logged_email;
    this.crewname = '';
    this.crewemail = '';
  }
  captainminus() {
    this.crew.name1 = 'We need a Captain!';
    this.crew.email1 = '';
    this.crewname = '';
    this.crewemail = '';
  }
  // #CAT400X
  lock_crew() {
    console.log('#CAT400X NUMOFCREW: ', this.eventUpdate.NumOfCrew);
    console.log('lock_crew :', this.crew.joined_crew);
    if (this.crew.joined_crew === 1 && this.crewname !== '') {
      this.crew.name1 = this.logged_user;
      this.crew.email1 = this.logged_email;
      this.crew.name2 = this.crewname;
      this.crew.email2 = this.crewemail;
      this.crewname = '';
      this.crewemail = '';
      this.crew.joined_crew = 2;
      console.log('2');
    } else if (this.crew.joined_crew === 2 && this.crewname !== '') {
      this.crew.name3 = this.crewname;
      this.crew.email3 = this.crewemail;
      this.crewname = '';
      this.crewemail = '';
      this.crew.joined_crew = 3;
      console.log('3');
    } else if (this.crew.joined_crew === 3 && this.crewname !== '') {
      this.crew.name4 = this.crewname;
      this.crew.email4 = this.crewemail;
      this.crewname = '';
      this.crewemail = '';
      this.crew.joined_crew = 4;
      console.log('4');
    } else if (this.crew.joined_crew === 4 && this.crewname !== '') {
      this.crew.name5 = this.crewname;
      this.crew.email5 = this.crewemail;
      this.crewname = '';
      this.crewemail = '';
      this.crew.joined_crew = 5;
      console.log('5');
    } else if (this.crew.joined_crew === 5 && this.crewname !== '') {
      console.log('6');
      this.crew.name6 = this.crewname;
      this.crew.email6 = this.crewemail;
      this.crewname = '';
      this.crewemail = '';
      this.crew.joined_crew = 6;
      this.crewlock.crew2_lock = true;
    }
    if (this.eventUpdate.NumOfCrew <= this.crew.joined_crew && this.eventUpdate.NumOfCrew) {
      console.log('this.eventUpdate.NumOfCrew: ', this.eventUpdate.NumOfCrew);
      console.log('this.crew.joined_crew: ', this.crew.joined_crew);
      console.log('LOCK');
      this.crewlock.crew2_lock = true;
    }
    if (this.captain.NumOfCrew <= this.crew.joined_crew && this.captain.NumOfCrew) {
      this.crewlock.crew2_lock = true;
      }
    }
lock_crew_minus() {
  console.log('JOINED_CREW: ', this.crew.joined_crew);
  console.log('THIS.CREW: ', this.crew);
  if (this.crew.joined_crew === 6) {
    console.log('6');
    this.crew.name6 = undefined;
    this.crew.email6 = undefined;
    this.crew.joined_crew = 5;
    this.crewlock.crew2_lock = false;
  }  else if (this.crew.joined_crew === 5) {
    this.crew.name5 = undefined;
    this.crew.email5 = undefined;
    this.crew.joined_crew = 4;
  } else if (this.crew.joined_crew === 4) {
    this.crew.name4 = undefined;
    this.crew.email4 = undefined;
    this.crew.joined_crew = 3;
  } else if (this.crew.joined_crew === 3) {
    this.crew.name3 = undefined;
    this.crew.email3 = undefined;
    this.crew.joined_crew = 2 ;
  } else if (this.crew.joined_crew === 2 && this.crew.name1 !== 'We need a Captain!') {
    this.crew.name2 = undefined;
    this.crew.email2 = undefined;
    this.crew.joined_crew = 1;
  }
  console.log('THIS.EVENTUPDATE: ', this.eventUpdate);
  if (this.eventUpdate.NumOfCrew > this.crew.joined_crew && this.eventUpdate.NumOfCrew) {
    this.crewlock.crew2_lock = false;
    }
}
  student_req() {
    console.log('this._service.currentUser: ', this._service.currentUser);
    console.log('this._service.currentUser.identity: ', this._service.currentUser.identity);
    this.eventUpdate.NumOfCrew = 6;
    console.log('this.logged_user:', this._service.currentUser);
    this.logged_user = this._service.currentUser.name;
    this.logged_email = this._service.currentUser.email;
    this.logged_identity = this._service.currentUser.identity;
    console.log('THIS.LOGGED_IDENTITY: ', this.logged_identity);
    if (this.logged_identity === 'captain') {
      console.log('captain');
      this.crew = {
        name1: this.logged_user,
        email1: this.logged_email,
        name2: undefined,
        email2: undefined,
        name3: undefined,
        email3: undefined,
        name4: undefined,
        email4: undefined,
        name5: undefined,
        email5: undefined,
        name6: undefined,
        email6: undefined,
        joined_crew: 1,
      };
    } else if (this.logged_identity === 'student') {
      console.log('student');
      this.crew = {
        name1: 'We need a Captain!',
        email1: '',
        name2: this.logged_user,
        email2: this.logged_email,
        name3: undefined,
        email3: undefined,
        name4: undefined,
        email4: undefined,
        name5: undefined,
        email5: undefined,
        name6: undefined,
        email6: undefined,
        joined_crew: 2,
      };
    }
    console.log('THIS CREW: ', this.crew);
    this.crewlock.crew2_lock = false;
    document.getElementById('myModal1').style.display = 'none';
    $('#myModal2').fadeIn();
  }
  captain_req() {
    this.logged_user = this._service.currentUser.name;
    this.logged_email = this._service.currentUser.email;
    this.crew = {
      name1: this.logged_user,
      email1: this.logged_email,
      name2: undefined,
      email2: undefined,
      name3: undefined,
      email3: undefined,
      name4: undefined,
      email4: undefined,
      name5: undefined,
      email5: undefined,
      name6: undefined,
      email6: undefined,
      joined_crew: 1,
    };
    this.crewlock.crew2_lock = false;
    document.getElementById('myModal1').style.display = 'none';
    $('#myModal3').fadeIn();
  }
  display_calendar(eventsData) {
    const userId = this.user_data._id;
    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
      },
      selectable: true,
      selectHelper: true,
      eventLimit: true,
      weekMode: 'liquid',
      url: '#',
      editable: true,
      eventClick: (e) => {

        console.log('this._service.currentUser: ', this._service.currentUser);
        console.log('EVENT CLICKED');
        console.log('E: ', e);
        console.log('E.CREW: ' , e.crew);
        console.log('CREW: ' , this.crew);
        this.eventUpdate = {
          title: e.title,
          date: moment(e.date).format('DD MMM YYYY'),
          timeFrom: e.timeFrom,
          timeTo: e.timeTo,
          vessel: e.vessel,
          spec: e.spec,
          message: e.message,
          NumOfCrew: e.NumOfCrew,
          identity: this._service.currentUser.identity,
          crew: {
            name1: e.crew.name1,
            email1: e.crew.email1,
            name2: e.crew.name2,
            email2: e.crew.email2,
            name3: e.crew.name3,
            email3: e.crew.email3,
            name4: e.crew.name4,
            email4: e.crew.email4,
            name5: e.crew.name5,
            email5: e.crew.email5,
            name6: e.crew.name6,
            email6: e.crew.email6,
            joined_crew: e.crew.joined_crew
          }
        };
        console.log('CLICK');
        this.crew = {
          name1: e.crew.name1,
          email1: e.crew.email1,
          name2:  e.crew.name2,
          email2: e.crew.email2,
          name3:  e.crew.name3,
          email3: e.crew.email3,
          name4:  e.crew.name4,
          email4: e.crew.email4,
          name5:  e.crew.name5,
          email5: e.crew.email5,
          name6:  e.crew.name6,
          email6: e.crew.email6,
          joined_crew: e.crew.joined_crew,
        };
        if (e.crew.joined_crew === 6) {
          console.log(6);
          this.crewlock.crew2_lock = true;
        }
        console.log('CREW: ', this.crew);
        console.log('CREW: ', this.crew);
        console.log('e.joined_crew: ', e.joined_crew);
        console.log('CLICK EVENTUPDATE: ', this.eventUpdate);
        console.log('E.DATEe.date: ', e.date);
        console.log('E.TITLE: ', e.title);
        e.date = moment(e.date).format('DD MMM YYYY');
        this.event_created_by = e.created_by;
        this.event_id = e._id;
        console.log('this.user_data._id: ', this.user_data._id);
        console.log('this.event_created_by: ', this.event_created_by);
        if (this.loginUser === 'none') {
          if (e.title.includes('Captain')) {
            console.log('NOT LOGGED IN CAPTAIN EVENT');
            $('#myModal0').fadeIn();
            $('#date').html(`Setting sail on: ${e.date}`);
            $('#title').html(`${e.title}`);
            $('#timeRange').html(`Set sail between: ${e.timeFrom} to ${e.timeTo}`);
            $('#vessel').html(`Vessel: ${e.vessel}`);
            $('#numCrew').html(`Vessel capacity: ${e.NumOfCrew} crew.`);
            $('#Message').html(`Captains Log (Message): ${e.message}`);
            $('#JoinCrew').html(`Join Captain ${this.crew.name1}'s Crew`);
            $('#name1').html(`Crewman #1: ${this.crew.name1}`);
            $('#email1').html(`Crewman #1's Email: ${this.crew.email1}`);
            $('#name2').html(`Crewman #2: ${this.crew.name2}`);
            $('#email2').html(`Crewman #2's Email: ${this.crew.email2}`);
            $('#name3').html(`Crewman #3: ${this.crew.name3}`);
            $('#email3').html(`Crewman #3's Email: ${this.crew.email3}`);
            $('#name4').html(`Crewman #4: ${this.crew.name4}`);
            $('#email4').html(`Crewman #4's Email: ${this.crew.email4}`);
            $('#name5').html(`Crewman #5: ${this.crew.name5}`);
            $('#email5').html(`Crewman #5's Email: ${this.crew.email5}`);
            $('#name6').html(`Crewman #6: ${this.crew.name6}`);
            $('#email6').html(`Crewman #6's Email: ${this.crew.email6}`);
            $('#joinedcrew').html(`The number of sailors currently signed to this voyage: ${this.crew.joined_crew}`);
          } else if (e.title.includes('Student')) {
            console.log('NOT LOGGED IN STUDENT EVENT');
            $('#myModal0').fadeIn();
            $('#date').html(`The student would like to set sail on: ${e.date}`);
            $('#title').html(`${e.title}`);
            $('#numCrew').html(``);
            $('#timeRange').html(`Setting sail between: ${e.timeFrom} to ${e.timeTo}`);
            $('#Message').html(`Message to Would-be crew: ${e.message}`);
            $('#name1').html(`Crewman #1: ${this.crew.name1}`);
            $('#email1').html(`Crewman #1's Email: ${this.crew.email1}`);
            $('#name2').html(`Crewman #1: ${this.crew.name2}`);
            $('#email2'). html(`Crewman #1's Email: ${this.crew.email2}`);
            $('#name3').html(`Crewman #2: ${this.crew.name3}`);
            $('#email3').html(`Crewman #2's Email: ${this.crew.email3}`);
            $('#name4').html(`Crewman #3: ${this.crew.name4}`);
            $('#email4').html(`Crewman #3's Email: ${this.crew.email4}`);
            $('#name5').html(`Crewman #4: ${this.crew.name5}`);
            $('#email5').html(`Crewman #4's Email: ${this.crew.email5}`);
            $('#name6').html(`Crewman #5: ${this.crew.name6}`);
            $('#email6').html(`Crewman #5's Email: ${this.crew.email6}`);
            $('#joinedcrew').html(`The number of sailors currently signed to this voyage: ${this.crew.joined_crew}`);
          }
        } else {
          if (this.user_data._id === e.created_by) {
            console.log('THIS.USER_DATA._ID: ', this.user_data._id, 'E.CREATED_BY: ', e.created_by, 'E.TITLE: ', e.title);
            if (e.title.includes('Crew')) {
              console.log('#CAM500');
              $('#myModal4').fadeIn();
            } else if (e.title.includes('Vessel')) {
              console.log('#CAM600');
              $('#myModal5').fadeIn();
            }
          } else {
            if (e.title.includes('Captain')) {
              if (e.crew.joined_crew == e.NumOfCrew) {
                this.crewlock.crew2_lock = true;
              }
              console.log('OTHER EVENT CREW');
              $('#myModal0').fadeIn();
              $('#date').html(`Setting sail on: ${e.date}`);
              $('#title').html(`${e.title}`);
              $('#timeRange').html(`Setting sail between: ${e.timeFrom} to ${e.timeTo}`);
              $('#vessel').html(`Vessel: ${e.vessel}`);
              $('#numCrew').html(`Vessel capacity: ${e.NumOfCrew}`);
              $('#Message').html(`Captains Log (Message): ${e.message}`);
              $('#JoinCrew').html(`Join Captain ${this.crew.name1}'s Crew`);
              $('#name1').html(`The Captain: ${this.crew.name1}`);
              $('#email1').html(`The Captain's Email: ${this.crew.email1}`);
              $('#name2').html(`Crewman #2: ${this.crew.name2}`);
              $('#email2').html(`Crewman #2's Email: ${this.crew.email2}`);
              $('#name3').html(`Crewman #3: ${this.crew.name3}`);
              $('#email3').html(`Crewman #3's Email: ${this.crew.email3}`);
              $('#name4').html(`Crewman #4: ${this.crew.name4}`);
              $('#email4').html(`Crewman #4's Email: ${this.crew.email4}`);
              $('#name5').html(`Crewman #5: ${this.crew.name5}`);
              $('#email5').html(`Crewman #5's Email: ${this.crew.email5}`);
              $('#name6').html(`Crewman #5: ${this.crew.name6}`);
              $('#email6').html(`Crewman #6's Email: ${this.crew.email6}`);
              $('#joinedcrew').html(`There are: ${this.crew.joined_crew} sailors signed up.`);
            } else if (e.title.includes('Student')) {
              console.log('OTHER EVENT VESSEL');
              console.log('THIS.CREW.NAME2: ', this.crew.name2);
              $('#myModal0').fadeIn();
              $('#date').html(`Setting sail on: ${e.date}`);
              $('#title').html(`${e.title}`);
              $('#timeRange').html(`Setting sail between: ${e.timeFrom} to ${e.timeTo}`);
              $('#Message').html(`Message to Would-be Captains: ${e.message}`);
              $('#JoinCrew').html(`Join Captain ${this.crew.name1} Crew`);
              $('#JoinCrew').html(`Join Captain ${this.crew.name1}'s Crew`);
              $('#name1').html(`The Captain: ${this.crew.name1}`);
              $('#email1').html(`The Captain's Email: ${this.crew.email1}`);
              $('#name2').html(`Crewman #2: ${this.crew.name2}`);
              $('#email2').html(`Crewman #2's Email: ${this.crew.email2}`);
              $('#name3').html(`Crewman #3: ${this.crew.name3}`);
              $('#email3').html(`Crewman #3's Email: ${this.crew.email3}`);
              $('#name4').html(`Crewman #4: ${this.crew.name4}`);
              $('#email4').html(`Crewman #4's Email: ${this.crew.email4}`);
              $('#name5').html(`Crewman #5: ${this.crew.name5}`);
              $('#email5').html(`Crewman #5's Email: ${this.crew.email5}`);
              $('#name6').html(`Crewman #6: ${this.crew.name6}`);
              $('#email6').html(`Crewman #6's Email: ${this.crew.email6}`);
              if (this.crew.name1 === 'We need a Captain!') {
                $('#joinedcrew').html(`There are: ${this.crew.joined_crew - 1} sailors signed up.`);
              } else {
                $('#joinedcrew').html(`There are: ${this.crew.joined_crew} sailors signed up.`);
              }
            }
          }
        }
      },
      events: eventsData,

      dayClick: (date, jsEvent, view) => {
        if (moment().format('YYYY-MM-DD') === date.format('YYYY-MM-DD') || date.isAfter(moment())) {
          this.student.date = date;
          this.captain.date = date;
          this.date_display = moment(date).format('DD MMM YYYY');
          $('#myModal1').fadeIn();
        }

      },
      eventAfterRender: (event, element, view) => {
        if (event.vessel !== undefined) {
          element.css('background-color', 'rgba(179, 225, 247, 1)');
          element.css('border', 'none');
        } else {
          element.css('background-color', '#CDDC39');
          element.css('border', 'none');
        }
      },
      displayEventTime: false

    });
  }

  req_login(state) {
    if (state === 'reg') {
      const data = [{
        user: null,
        mesg: state
      }];
      this._service.updateLoginStatus(data);
    } else if (state === 'log') {
      const data = [{
        user: null,
        mesg: state
      }];
      this._service.updateLoginStatus(data);
    } else {
      return;
    }
    $('#myModal1').fadeOut();
  }
  delete(eventId) {
    console.log('click', this.event_id);
    this._service.delete_event(this.event_id, this.loginUser, (res) => {
      this.closeModal();
      this.update_event(res);
    });
  }

  // #CAT500
  event_update() {
       this.eventUpdate.crew.name1 = this.crew.name1;
       this.eventUpdate.crew.email1 = this.crew.email1;
       this.eventUpdate.crew.name2 = this.crew.name2;
       this.eventUpdate.crew.email2 = this.crew.email2;
       this.eventUpdate.crew.name3 = this.crew.name3;
       this.eventUpdate.crew.email3 = this.crew.email3;
       this.eventUpdate.crew.name4 = this.crew.name4;
       this.eventUpdate.crew.email4 = this.crew.email4;
       this.eventUpdate.crew.name5 = this.crew.name5;
       this.eventUpdate.crew.email5 = this.crew.email5;
       this.eventUpdate.crew.name6 = this.crew.name6;
       this.eventUpdate.crew.email6 = this.crew.email6;
        this.eventUpdate.crew.joined_crew = this.crew.joined_crew;
    console.log('#CAT501 EVENTUPDATE: ', this.eventUpdate);
    console.log('#CAT502 EVENT_ID: ', this.event_id);
    this._service.event_update(this.event_id, this.eventUpdate, (res) => {
      this.closeModal();
      this.update_event(res);
    });
  }
  // #CAT1000  ADD_CREW_SUBMIT()
  add_crew_submit() {
    this.eventUpdate.crew.name1 = this.crew.name1;
    this.eventUpdate.crew.email1 = this.crew.email1;
    this.eventUpdate.crew.name2 = this.crew.name2;
    this.eventUpdate.crew.email2 = this.crew.email2;
    this.eventUpdate.crew.name3 = this.crew.name3;
    this.eventUpdate.crew.email3 = this.crew.email3;
    this.eventUpdate.crew.name4 = this.crew.name4;
    this.eventUpdate.crew.email4 = this.crew.email4;
    this.eventUpdate.crew.name5 = this.crew.name5;
    this.eventUpdate.crew.email5 = this.crew.email5;
    this.eventUpdate.crew.name6 = this.crew.name6;
    this.eventUpdate.crew.email6 = this.crew.email6;
    this.eventUpdate.crew.joined_crew = this.crew.joined_crew;
     console.log('#CAT1010 THIS.EVENTUPDATE: ', this.eventUpdate);
    console.log('#CAT1020 CREW: ', this.crew);
    console.log('#CAT1030 THIS.EVENT_ID: ', this.event_id);
    // #CAT1040 SUBMIT TO #SE100 ADD_CREW_SUBMIT() //
    this._service.add_crew_submit(this.event_id, this.eventUpdate, (res) => {
      console.log('#CAT1040 #SE100 RES: ', res);
      this.closeModal();
      this.update_event(res);
    });
  }
}
