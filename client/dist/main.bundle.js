webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app-routing.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var navbar_component_1 = __webpack_require__("../../../../../src/app/navbar/navbar.component.ts");
var reg_pending_component_1 = __webpack_require__("../../../../../src/app/reg-pending/reg-pending.component.ts");
var check_email_component_1 = __webpack_require__("../../../../../src/app/check-email/check-email.component.ts");
var forgetpw_component_1 = __webpack_require__("../../../../../src/app/forgetpw/forgetpw.component.ts");
var resetpassword_component_1 = __webpack_require__("../../../../../src/app/resetpassword/resetpassword.component.ts");
var routes = [
    { path: '', component: navbar_component_1.NavbarComponent },
    { path: 'activate/:token', component: reg_pending_component_1.RegPendingComponent },
    { path: 'check_email', component: check_email_component_1.CheckEmailComponent },
    { path: 'forgetpw', component: forgetpw_component_1.ForgetpwComponent },
    { path: 'reset/:token', component: resetpassword_component_1.ResetpasswordComponent },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(routes)],
            exports: [router_1.RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;


/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "\n<router-outlet></router-outlet>\n\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;


/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var app_routing_module_1 = __webpack_require__("../../../../../src/app/app-routing.module.ts");
var forms_1 = __webpack_require__("../../../forms/esm5/forms.js");
var http_1 = __webpack_require__("../../../http/esm5/http.js");
var main_service_1 = __webpack_require__("../../../../../src/app/main.service.ts");
var app_component_1 = __webpack_require__("../../../../../src/app/app.component.ts");
var navbar_component_1 = __webpack_require__("../../../../../src/app/navbar/navbar.component.ts");
var footer_component_1 = __webpack_require__("../../../../../src/app/footer/footer.component.ts");
var header_component_1 = __webpack_require__("../../../../../src/app/header/header.component.ts");
var display_component_1 = __webpack_require__("../../../../../src/app/display/display.component.ts");
var calendar_component_1 = __webpack_require__("../../../../../src/app/calendar/calendar.component.ts");
var amazing_time_picker_1 = __webpack_require__("../../../../amazing-time-picker/amazing-time-picker.es5.js");
var reg_pending_component_1 = __webpack_require__("../../../../../src/app/reg-pending/reg-pending.component.ts");
var check_email_component_1 = __webpack_require__("../../../../../src/app/check-email/check-email.component.ts");
var forgetpw_component_1 = __webpack_require__("../../../../../src/app/forgetpw/forgetpw.component.ts");
var resetpassword_component_1 = __webpack_require__("../../../../../src/app/resetpassword/resetpassword.component.ts");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                navbar_component_1.NavbarComponent,
                footer_component_1.FooterComponent,
                header_component_1.HeaderComponent,
                display_component_1.DisplayComponent,
                calendar_component_1.CalendarComponent,
                reg_pending_component_1.RegPendingComponent,
                check_email_component_1.CheckEmailComponent,
                forgetpw_component_1.ForgetpwComponent,
                resetpassword_component_1.ResetpasswordComponent,
            ],
            imports: [
                platform_browser_1.BrowserModule,
                app_routing_module_1.AppRoutingModule,
                amazing_time_picker_1.AmazingTimePickerModule,
                platform_browser_1.BrowserModule,
                app_routing_module_1.AppRoutingModule,
                forms_1.FormsModule,
                http_1.HttpModule,
            ],
            providers: [main_service_1.MainService],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;


/***/ }),

/***/ "../../../../../src/app/calendar/calendar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".sectionName{\n    text-align: center;\n}\n\n#calendar{\n    width: 80%;\n    margin: auto;\n}\n\n/* The Modal (background) */\n\n.modal {\n    display: none; /* Hidden by default */\n    position: fixed; /* Stay in place */\n    z-index: 1; /* Sit on top */\n    padding-top: 100px; /* Location of the box */\n    left: 0;\n    top: 0;\n    width: 100%; /* Full width */\n    height: 100%; /* Full height */\n    overflow: auto; /* Enable scroll if needed */\n    background-color: rgb(0,0,0); /* Fallback color */\n    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */\n}\n\n/* Modal Content */\n\n.modal-content {\n    background-color: #fefefe;\n    margin: auto;\n    padding: 20px;\n    border: 1px solid #888;\n    width: 60%;\n}\n\n/* The Close Button */\n\n.close {\n    color: #aaaaaa;\n    float: right;\n    font-size: 28px;\n    font-weight: bold;\n}\n\n.close:hover,\n.close:focus {\n    color: #000;\n    text-decoration: none;\n    cursor: pointer;\n}\n\n.butt1 {\n    margin-bottom: 15px;\n}\n\n.butt3 {\n    margin-bottom: 20px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/calendar/calendar.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- CA -->\n\n<h1 class=\"sectionName\">GPYC Events Calendar</h1>\n<div id=\"calendar\"></div>\n\n<!-- #CAM100 NOT LOGGED IN/NOT YOUR EVENT MODAL -> SUBMIT TO: #CAT1000  ADD_CREW_SUBMIT() -->\n<div id=\"myModal0\" class=\"modal\">\n  <div class=\"modal-content\">\n    <h1 id=\"title\"></h1>\n    <p id=\"date\"></p>\n    <p id=\"timeRange\"></p>\n    <p id=\"vessel\"></p>\n    <p id=\"spec\"></p>\n    <p id=\"numCrew\"></p>\n    <p id=\"Message\"></p>\n    <h2>Current Crew for this Vessel:</h2>\n    <p>Captain: {{crew.name1}}</p>\n    <p>Captain Email: {{crew.email1}}</p>\n    <div class=\"hide_email\" *ngIf=\"crew.name2 !== undefined\">\n      <p>Crew #2 Name: {{crew.name2}}</p>\n      <p>Crew #2 Email: {{crew.email2}}</p>\n    </div>\n    <div class=\"hide_email\" *ngIf=\"crew.name3 !== undefined\">\n        <p>Crew #3 Name: {{crew.name3}}</p>\n        <p>Crew #3 Email: {{crew.email3}}</p>\n    </div>\n    <div class=\"hide_email\" *ngIf=\"crew.name4 !== undefined\">\n        <p>Crew #4 Name: {{crew.name4}}</p>\n        <p>Crew #4 Email: {{crew.email4}}</p>\n    </div>\n    <div class=\"hide_email\" *ngIf=\"crew.name5 !== undefined\">\n        <p>Crew #5 Name: {{crew.name5}}</p>\n        <p>Crew #5 Email: {{crew.email5}}</p>\n    </div>\n    <div class=\"hide_email\" *ngIf=\"crew.name6 !== undefined\">\n        <p>Crew #6 Name: {{crew.name6}}</p>\n        <p>Crew #6 Email: {{crew.email6}}</p>\n    </div>\n\n        <form *ngIf=\"loginUser !== 'none'\" (submit)=\"add_crew_submit()\">\n        <!-- #CAM110 SHOWS IF CAPTAIN AND NO CAPTAIN ASSIGNED -->\n        <div id=\"crew_join_form\" *ngIf=\"logged_identity == 'captain'\">\n          <div *ngIf=\"this.crew.name1 === 'We need a Captain!'\">\n            <p>Crew That have joined: {{crew.joined_crew-1}}</p>\n            <button id=\"captainplus\" type=\"button\" (click)=\"captainplus()\" class=\"btn btn-info\"> Captain this Vessel!</button>\n          </div>\n          <button *ngIf=\"this.crew.name1 === logged_user\" id=\"captainminus()\" type=\"button\" (click)=\"captainminus()\" class=\"btn btn-info\">Remove Captain</button>\n        </div>\n\n        <br>\n        <!-- #CAM120 SHOWS IF THERE IS A CAPTAIN ASSIGNED -->\n        <div *ngIf=\"this.crew.name1 !== 'We need a Captain!'\">\n            <p>Crew That have joined: {{crew.joined_crew}}</p>\n        </div>\n\n        <!-- #CAM130 ADD AND JOIN FORM FIELDS-->\n        <div id=\"crew\" class=\"form-group\">\n          <label>Join the Crew</label>\n          <label>Add Crew Name: </label>\n          <input type=\"text\" name=\"crew2_name\" class=\"form-control\" [disabled]=\"crewlock.crew2_lock\" [(ngModel)]=\"crewname\">\n        </div> \n\n        <div id=\"crew\" class=\"form-group\">\n          <label>Add Crew Email:</label>\n          <input type=\"text\" name=\"crew2_email\" class=\"form-control\" [disabled]=\"crewlock.crew2_lock\" [(ngModel)]=\"crewemail\">\n        </div>\n        <button id=\"crewplus_modal0\" type=\"button\" (click)=\"lock_crew()\" class=\"btn btn-info\"> + </button>\n        <button id=\"crewminus_modal0\" type=\"button\" (click)=\"lock_crew_minus()\" class=\"btn btn-info\"> - </button>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-danger\" data-dismiss=\"modal\" (click)=\"closeModal()\">Cancel</button>\n          <button class=\"btn btn-info\">Submit</button>\n        </div>\n        <br>\n      </form>\n    </div>\n</div>\n<!-- #CAM100E END OF NOT LOGGED IN/NOT YOUR EVENT MODAL -->\n\n<!-- #CAM200 CLICK ON EVENT INITIAL MODAL -->\n<div id=\"myModal1\" class=\"modal\">\n  <div class=\"modal-content\">\n    <button *ngIf=\"loginUser == 'student'\" class=\"btn btn-primary butt1\" (click)=\"student_req()\">Seeking a Vessel</button>\n\n    <button *ngIf=\"loginUser == 'captain'\" class=\"btn btn-primary butt1\" (click)=\"student_req()\">Seeking a Vessel</button>\n    <button *ngIf=\"loginUser == 'captain'\" class=\"btn btn-primary butt2\" (click)=\"captain_req()\">Seeking a Crew</button>\n\n    <button *ngIf=\"loginUser == 'none'\" class=\"btn btn-primary butt3\" (click)=\"req_login('log')\">Login</button>\n    <button *ngIf=\"loginUser == 'none'\" class=\"btn btn-primary butt4\" (click)=\"req_login('reg')\">Register</button>\n  </div>\n</div>\n<!-- #CAM200E END OF CLICK ON EVENT INITIAL MODAL -->\n\n<!-- #CAM300 SEEKING A VESSEL EVENT - FROM: STUDENT_REQ() -->\n<div id=\"myModal2\" class=\"modal\">\n  <div class=\"modal-content\">\n    <form (submit)=\"student_submit()\">\n      <h1>Crew Seeking Vessel</h1>\n      <div class=\"form-group\">\n        <label for=\"\">Date</label>\n        <br>\n        <input type=\"text\" class=\"form-control\" name=\"date\" disabled [(ngModel)]=\"date_display\">\n        <br>\n      </div>\n      <div class=\"form-group\">\n        <label>Time From: </label>\n        <br>\n        <input (click)=\"timeFrom()\" class=\"form-control\" value= {{student.timeFrom}}>\n        <br>\n      </div>\n      <div class=\"form-group\">\n        <label>Time To: </label>\n        <br>\n        <input (click)=\"timeTo()\" class=\"form-control\" value= {{student.timeTo}}>\n        <br>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"\">Message to Would-be Captains:</label>\n        <br>\n        <textarea name=\"message\" class=\"form-control\" cols=\"30\" rows=\"3\" [(ngModel)]=\"student.message\"></textarea>\n      </div>\n      <label>Add Other Crew (besides yourself):</label>\n      <p>Captain: {{crew.name1}}</p>\n      <p>Captain Email: {{crew.email1}}</p>\n      <div class=\"hide_email\" *ngIf=\"crew.name2 !== undefined\">\n        <p>Crew #1 Name: {{crew.name2}}</p>\n        <p>Crew #1 Email: {{crew.email2}}</p>\n      </div>\n      <div class=\"hide_email\" *ngIf=\"crew.name3 !== undefined\">\n          <p>Crew #2 Name: {{crew.name3}}</p>\n          <p>Crew #2 Email: {{crew.email3}}</p>\n      </div>\n      <div class=\"hide_email\" *ngIf=\"crew.name4 !== undefined\">\n          <p>Crew #3 Name: {{crew.name4}}</p>\n          <p>Crew #3 Email: {{crew.email4}}</p>\n      </div>\n      <div class=\"hide_email\" *ngIf=\"crew.name5 !== undefined\">\n          <p>Crew #4 Name: {{crew.name5}}</p>\n          <p>Crew #4 Email: {{crew.email5}}</p>\n      </div>\n      <div class=\"hide_email\" *ngIf=\"crew.name6 !== undefined\">\n          <p>Crew #5 Name: {{crew.name6}}</p>\n          <p>Crew #5 Email: {{crew.email6}}</p>\n      </div>\n      <div id=\"crew\" class=\"form-group\">\n        <label>Add Crew Name:</label>\n        <div class=\"hide_email\" *ngIf=\"this.crew.name1 == 'We need a Captain!'\">\n            <p>Crew That have joined: {{crew.joined_crew-1}}</p>\n        </div>\n        <div class=\"hide_email\" *ngIf=\"this.crew.name1 !== 'We need a Captain!'\">\n          <p>Crew That have joined: {{crew.joined_crew}}</p>\n        </div>\n        <input type=\"text\" name=\"crew2_name\" class=\"form-control\" [disabled]=\"crewlock.crew2_lock\" [(ngModel)]=\"crewname\">\n      </div> \n      <div id=\"crew\" class=\"form-group\">\n        <label>Add Crew Email:</label>\n        <input type=\"text\" name=\"crew2_email\" class=\"form-control\" [disabled]=\"crewlock.crew2_lock\" [(ngModel)]=\"crewemail\">\n      </div>\n      <button id=\"crewplus\" type=\"button\" (click)=\"lock_crew()\" class=\"btn btn-info\"> + </button>\n      <button id=\"crewminus\" type=\"button\" (click)=\"lock_crew_minus()\" class=\"btn btn-info\"> - </button>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-danger\" data-dismiss=\"modal\" (click)=\"closeModal()\">Close</button>\n        <button class=\"btn btn-info\">Submit</button>\n      </div>\n      <br/>\n    </form>\n  </div>\n</div>\n<!-- #CAM300E SEEKING A VESSEL EVENT - FROM: STUDENT_REQ() -->\n\n\n<!-- #CAM400 CAPTAIN CREATING EVENT - FROM: CAPTAIN_REQ() -->\n<div id=\"myModal3\" class=\"modal\">\n  <div class=\"modal-content\">\n    <form (submit)=\"captain_submit()\">\n      <h1>Captain Seeking Crew</h1>\n      <div class=\"form-group\">\n        <label for=\"\">Date</label>\n        <input type=\"text\" class=\"form-control\" name=\"date\" disabled [(ngModel)]=\"date_display\">\n      </div>\n      <div class=\"form-group\">\n        <label>Time From: </label>\n        <input (click)=\"timeFrom()\" class=\"form-control\" value= {{captain.timeFrom}}>\n      </div>\n      <div class=\"form-group\">\n        <label>Time To: </label>\n        <input (click)=\"timeTo()\" class=\"form-control\" value= {{captain.timeTo}}>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"\">Vessel:</label>\n        <input type=\"text\" class=\"form-control\" name=\"vessel\" [(ngModel)]=\"captain.vessel\">\n      </div>\n      <div class=\"form-group\">\n        <label for=\"\">Specs:</label>\n        <input type=\"text\" class=\"form-control\" name=\"specs\" [(ngModel)]=\"captain.spec\">\n      </div>\n      <div class=\"form-group\">\n        <label for=\"\">Captains Log (Message):</label>\n        <textarea name=\"message\" class=\"form-control\" rows=\"3\" [(ngModel)]=\"captain.message\"></textarea>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"\">Vessel Capacity (including yourself):</label>\n        <input type=\"text\" class=\"form-control\" name=\"num_of_crew\" [(ngModel)]=\"captain.NumOfCrew\">\n      </div>\n      <label>Add Other Crew (besides yourself):</label>\n      <p>Crew#1: {{crew.name1}}</p>\n      <p>Crew#1 Email: {{crew.email1}}</p>\n      <div class=\"hide_email\" *ngIf=\"crew.name2 !== undefined\">\n        <p>Crew #2 Name: {{crew.name2}}</p>\n        <p>Crew #2 Email: {{crew.email2}}</p>\n      </div>\n      <div class=\"hide_email\" *ngIf=\"crew.name3 !== undefined\">\n          <p>Crew #3 Name: {{crew.name3}}</p>\n          <p>Crew#3 Email: {{crew.email3}}</p>\n      </div>\n      <div class=\"hide_email\" *ngIf=\"crew.name4 !== undefined\">\n          <p>Crew #4 Name: {{crew.name4}}</p>\n          <p>Crew#4 Email: {{crew.email4}}</p>\n      </div>\n      <div class=\"hide_email\" *ngIf=\"crew.name5 !== undefined\">\n          <p>Crew #5 Name: {{crew.name5}}</p>\n          <p>Crew#5 Email: {{crew.email5}}</p>\n      </div>\n      <div class=\"hide_email\" *ngIf=\"crew.name6 !== undefined\">\n          <p>Crew #6 Name: {{crew.name6}}</p>\n          <p>Crew#6 Email: {{crew.email6}}</p>\n      </div>\n      <div id=\"crew\" class=\"form-group\">\n        <label>Add Crew Name:</label>\n        <p>Crew That have joined: {{crew.joined_crew}}</p>\n        <input type=\"text\" name=\"crew2_name\" class=\"form-control\" [disabled]=\"crewlock.crew2_lock\" [(ngModel)]=\"crewname\">\n      </div> \n      <div id=\"crew\" class=\"form-group\">\n        <label>Add Crew Email:</label>\n        <input type=\"text\" name=\"crew2_email\" class=\"form-control\" [disabled]=\"crewlock.crew2_lock\" [(ngModel)]=\"crewemail\">\n      </div>\n      <button id=\"crewplus_captain\" type=\"button\" (click)=\"lock_crew()\" class=\"btn btn-info\"> + </button>\n      <button id=\"crewminus_captain\" type=\"button\" (click)=\"lock_crew_minus()\" class=\"btn btn-info\"> - </button>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-danger\" data-dismiss=\"modal\" (click)=\"closeModal()\">Close</button>\n        <button class=\"btn btn-info\">Submit</button>\n      </div>\n      <br/>\n    </form>\n  </div>\n</div>\n<!-- #CAM400E CAPTAIN CREATING EVENT - FROM: CAPTAIN_REQ() -->\n\n\n<!-- #CAM500 CAPTAIN UPDATE EVENT MODAL- FROM: CAPTAIN_REQ() -->\n<div id=\"myModal4\" class=\"modal\">\n  <div class=\"modal-content\">\n    <form (submit)=\"event_update()\">\n      <h1>Update your event</h1>\n      <div class=\"form-group\">\n        <label for=\"\">Date</label>\n        <input type=\"text\" class=\"form-control\" name=\"date\" disabled [(ngModel)]= \"eventUpdate.date\">\n      </div>\n      <div class=\"form-group\">\n        <label>Time From: </label>\n        <input (click)=\"timeFrom()\" class=\"form-control\" value= {{eventUpdate.timeFrom}} name=\"timeFrom\">\n      </div>\n      <div class=\"form-group\">\n        <label>Time To: </label>\n        <input (click)=\"timeTo()\" class=\"form-control\" value= {{eventUpdate.timeTo}} name=\"timeTo\">\n      </div>\n      <div class=\"form-group\">\n        <label for=\"\">Vessel:</label>\n        <input type=\"text\" class=\"form-control\" name=\"vessel\" [(ngModel)]=\"eventUpdate.vessel\">\n      </div>\n      <div class=\"form-group\">\n        <label for=\"\">Specs:</label>\n        <input type=\"text\" class=\"form-control\" name=\"specs\" [(ngModel)]=\"eventUpdate.spec\">\n      </div>\n      <div class=\"form-group\">\n        <label for=\"\">Captains Log (Message):</label>\n        <textarea name=\"message\" class=\"form-control\" rows=\"3\" [(ngModel)]=\"eventUpdate.message\"></textarea>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"\">Number of Crew You Seek:</label>\n        <input type=\"number\" class=\"form-control\" name=\"num_of_crew\" [(ngModel)]=\"eventUpdate.NumOfCrew\">\n      </div>\n      <label>Add Other Crew (besides yourself):</label>\n      <p>Crew#1: {{logged_user}}</p>\n      <p>Crew#1 Email: {{logged_email}}</p>\n      <div class=\"hide_email\" *ngIf=\"crew.name2 !== undefined\">\n        <p>Crew #2 Name: {{crew.name2}}</p>\n        <p>Crew #2 Email: {{crew.email2}}</p>\n      </div>\n      <div class=\"hide_email\" *ngIf=\"crew.name3 !== undefined\">\n          <p>Crew #3 Name: {{crew.name3}}</p>\n          <p>Crew#3 Email: {{crew.email3}}</p>\n      </div>\n      <div class=\"hide_email\" *ngIf=\"crew.name4 !== undefined\">\n          <p>Crew #4 Name: {{crew.name4}}</p>\n          <p>Crew#4 Email: {{crew.email4}}</p>\n      </div>\n      <div class=\"hide_email\" *ngIf=\"crew.name5 !== undefined\">\n          <p>Crew #5 Name: {{crew.name5}}</p>\n          <p>Crew#5 Email: {{crew.email5}}</p>\n      </div>\n      <div class=\"hide_email\" *ngIf=\"crew.name6 !== undefined\">\n          <p>Crew #6 Name: {{crew.name6}}</p>\n          <p>Crew#6 Email: {{crew.email6}}</p>\n          <p>Crew That have joined: {{crew.joined_crew}}</p>\n      </div>\n    <br><br> \n      <div id=\"crew\" class=\"form-group\">\n        <label>Add Crew Name:</label>\n        <input type=\"text\" name=\"crew2_name\" class=\"form-control\" [disabled]=\"crewlock.crew2_lock\" [(ngModel)]=\"crewname\">\n      </div> \n      <div id=\"crew\" class=\"form-group\">\n        <label>Add Crew Email:</label>\n        <input type=\"text\" name=\"crew2_email\" class=\"form-control\" [disabled]=\"crewlock.crew2_lock\" [(ngModel)]=\"crewemail\">\n      </div>\n      <button id=\"crewplus_update\" type=\"button\" (click)=\"lock_crew()\" class=\"btn btn-info\"> + </button>\n      <button id=\"crewminus_update\" type=\"button\" (click)=\"lock_crew_minus()\" class=\"btn btn-info\"> - </button>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-danger\" data-dismiss=\"modal\" (click)=\"closeModal()\">Close</button>\n        <button class=\"btn btn-info\" >Update</button>\n      </div>\n      <br/>\n    </form>\n    <!-- #CAD550 -->\n    <button id=\"deleteBtn\" *ngIf=\"user_data._id === event_created_by\" (click)=\"delete(event_id)\" class=\"btn btn-danger\">Delete</button>\n  </div>\n</div>\n<!-- #CAM500E CAPTAIN UPDATE EVENT MODAL  -->\n\n\n<!-- #CAM600 STUDENT UPDATE EVENT MODAL  -->\n<div id=\"myModal5\" class=\"modal\">\n  <div class=\"modal-content\">\n    <form (submit)=\"event_update()\">\n      <h1>Update your event</h1>\n      <div class=\"form-group\">\n        <label for=\"\">Date</label>\n        <input type=\"text\" class=\"form-control\" name=\"date\" disabled [(ngModel)]=\"eventUpdate.date\">\n      </div>\n      <div class=\"form-group\">\n        <label>Time From: </label>\n        <input (click)=\"timeFrom()\" class=\"form-control\" [(ngModel)]=\"eventUpdate.timeFrom\" name=\"timeFrom\">\n      </div>\n      <div class=\"form-group\">\n        <label>Time To: </label>\n        <input (click)=\"timeTo()\" class=\"form-control\" [(ngModel)]=\"eventUpdate.timeTo\" name=\"timeTo\">\n      </div>\n      <div class=\"form-group\">\n        <label for=\"\">Captains Log (Message):</label>\n        <textarea name=\"message\" class=\"form-control\" rows=\"3\" [(ngModel)]=\"eventUpdate.message\"></textarea>\n      </div>\n      <label>Add Other Crew (besides yourself):</label>\n      <p>Captain: {{crew.name1}}</p>\n      <p>Captain Email: {{crew.email1}}</p>\n      <div class=\"hide_email\" *ngIf=\"crew.name2 !== undefined\">\n        <p>Crew #1 Name: {{crew.name2}}</p>\n        <p>Crew #1 Email: {{crew.email2}}</p>\n      </div>\n      <div class=\"hide_email\" *ngIf=\"crew.name3 !== undefined\">\n          <p>Crew #2 Name: {{crew.name3}}</p>\n          <p>Crew #2 Email: {{crew.email3}}</p>\n      </div>\n      <div class=\"hide_email\" *ngIf=\"crew.name4 !== undefined\">\n          <p>Crew #3 Name: {{crew.name4}}</p>\n          <p>Crew #3 Email: {{crew.email4}}</p>\n      </div>\n      <div class=\"hide_email\" *ngIf=\"crew.name5 !== undefined\">\n          <p>Crew #4 Name: {{crew.name5}}</p>\n          <p>Crew #4 Email: {{crew.email5}}</p>\n      </div>\n      <div class=\"hide_email\" *ngIf=\"crew.name6 !== undefined\">\n          <p>Crew #5 Name: {{crew.name6}}</p>\n          <p>Crew #5 Email: {{crew.email6}}</p>\n      </div>\n    <br><br> \n      <div id=\"crew\" class=\"form-group\">\n        <label>Add Crew Name:</label>\n        <div *ngIf=\"this.crew.name1 !== 'We need a Captain!'\">\n          <p>Crew That have joined: {{crew.joined_crew}}</p>\n        </div>\n        <div *ngIf=\"this.crew.name1 === 'We need a Captain!'\">\n            <p>Crew That have joined: {{crew.joined_crew-1}}</p>\n        </div>\n        <input type=\"text\" name=\"crew2_name\" class=\"form-control\" [disabled]=\"crewlock.crew2_lock\" [(ngModel)]=\"crewname\">\n      </div> \n      <div id=\"crew\" class=\"form-group\">\n        <label>Add Crew Email:</label>\n        <input type=\"text\" name=\"crew2_email\" class=\"form-control\" [disabled]=\"crewlock.crew2_lock\" [(ngModel)]=\"crewemail\">\n      </div>\n      <button id=\"crewplus_event\" type=\"button\" (click)=\"lock_crew()\" class=\"btn btn-info\"> + </button>\n      <button id=\"crewminus_event\" type=\"button\" (click)=\"lock_crew_minus()\" class=\"btn btn-info\"> - </button>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-danger\" data-dismiss=\"modal\" (click)=\"closeModal()\">Close</button>\n        <button class=\"btn btn-info\">Submit</button>\n      </div>\n      <br/>\n    </form>\n    <button id=\"deleteBtn\" *ngIf=\"user_data._id == event_created_by\" (click)=\"delete(event_id)\" class=\"btn btn-danger\">Delete</button>\n  </div>\n</div>\n<!-- #CAM600E STUDENT UPDATE EVENT MODAL  -->"

/***/ }),

/***/ "../../../../../src/app/calendar/calendar.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var amazing_time_picker_1 = __webpack_require__("../../../../amazing-time-picker/amazing-time-picker.es5.js");
var main_service_1 = __webpack_require__("../../../../../src/app/main.service.ts");
var moment = __webpack_require__("../../../../moment/moment.js");
var CalendarComponent = /** @class */ (function () {
    function CalendarComponent(atp, _service) {
        this.atp = atp;
        this._service = _service;
        this.crewlock = {
            crew2_lock: false,
            captain_lock: false
        };
        this.eventUpdate = {
            date: null,
            title: null,
            timeFrom: null,
            timeTo: null,
            vessel: null,
            spec: null,
            message: null,
            NumOfCrew: null,
            identity: null,
            crew: {
                name1: undefined,
                email1: undefined,
                name2: undefined,
                email2: undefined,
                name3: undefined,
                email3: undefined,
                name4: undefined,
                email4: undefined,
                name5: undefined,
                email5: undefined,
                name6: undefined,
                email6: undefined,
                joined_crew: null,
            }
        };
        this.crew = {
            name1: undefined,
            email1: undefined,
            name2: undefined,
            email2: undefined,
            name3: undefined,
            email3: undefined,
            name4: undefined,
            email4: undefined,
            name5: undefined,
            email5: undefined,
            name6: undefined,
            email6: undefined,
            joined_crew: null,
        };
        this.user_data = {
            _id: null
        };
        this.student = {
            title: '',
            date: null,
            timeFrom: null,
            timeTo: null,
            message: '',
            crew: {
                name1: '',
                email1: '',
                name2: '',
                email2: '',
                name3: '',
                email3: '',
                name4: '',
                email4: '',
                name5: '',
                email5: '',
                name6: '',
                email6: '',
                joined_crew: '',
            }
        };
        this.captain = {
            date: null,
            title: '',
            timeFrom: null,
            timeTo: null,
            vessel: '',
            spec: '',
            NumOfCrew: '',
            message: '',
            crew: {
                name1: '',
                email1: '',
                name2: '',
                email2: '',
                name3: '',
                email3: '',
                name4: '',
                email4: '',
                name5: '',
                email5: '',
                name6: '',
                email6: '',
                joined_crew: null,
            }
        };
        this.loginUser = 'none';
        this.logged_user = 'none';
        this.logged_userId = 'none';
        this.logged_email = 'none';
        this.logged_identity = 'none';
        this.crewemail = '';
        this.crewname = '';
        this.saveTime = {
            start: [],
            to: []
        };
    }
    CalendarComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this._service.currentUser !== null) {
            this.logged_user = this._service.currentUser.name;
            this.logged_email = this._service.currentUser.email;
            this.logged_userId = this._service.currentUser.id;
            this.logged_identity = this._service.currentUser.identity;
            console.log('THIS.LOGGED_IDENTITY AT INIT: ', this.logged_identity);
        }
        this._service.getAllEvents(function (res) {
            _this.display_calendar(res);
        });
        this.crewlock.crew2_lock = false;
        this._service.loginstatus.subscribe(function (data) {
            console.log('data: ', data);
            if (data[0].user != null) {
                _this.user_data = data[0].user;
                _this.loginUser = data[0].user.identity;
            }
            if (data[0].user == null) {
                _this.loginUser = 'none';
            }
        });
        console.log('loginuser', this.loginUser);
    };
    CalendarComponent.prototype.update_event = function (events) {
        $('#calendar').fullCalendar('removeEvents');
        $('#calendar').fullCalendar('addEventSource', events);
        $('#calendar').fullCalendar('rerenderEvents');
    };
    // #CAT300 SEEKING A VESSEL SUBMIT
    CalendarComponent.prototype.student_submit = function () {
        var _this = this;
        if (this.loginUser === 'student') {
            console.log('#CAT310 - LOGINUSER IS STUDENT - THIS._SERVICE.CURRENTUSER: ', this._service.currentUser);
            this.student.title = 'Student ' + this._service.currentUser.name + ' Seeking Vessel!';
            this.student.crew.name1 = this.crew.name1,
                this.student.crew.email1 = this.crew.email1,
                this.student.crew.name2 = this.crew.name2,
                this.student.crew.email2 = this.crew.email2,
                this.student.crew.name3 = this.crew.name3,
                this.student.crew.email3 = this.crew.email3,
                this.student.crew.name4 = this.crew.name4,
                this.student.crew.email4 = this.crew.email4,
                this.student.crew.name5 = this.crew.name5,
                this.student.crew.email5 = this.crew.email5,
                this.student.crew.name6 = this.crew.name6,
                this.student.crew.email6 = this.crew.email6,
                this.student.crew.joined_crew = this.crew.joined_crew,
                console.log('#CAT320 THIS.STUDENT: ', this.student);
            this._service.createStudentEvent(this.student, function (res) {
                console.log('#CAT330 RES: ', res);
                _this.closeModal();
                _this.update_event(res);
            });
        }
        else {
            this.captain.title = 'Captain ' + this._service.currentUser.name + ' seeking Vessel!';
            this.captain.timeFrom = this.student.timeFrom;
            this.captain.timeTo = this.student.timeTo;
            this.captain.vessel = '';
            this.captain.spec = '';
            this.captain.message = this.student.message;
            this.captain.NumOfCrew = '';
            this.captain.crew.name1 = this.crew.name1,
                this.captain.crew.email1 = this.crew.email1,
                this.captain.crew.name2 = this.crew.name2,
                this.captain.crew.email2 = this.crew.email2,
                this.captain.crew.name3 = this.crew.name3,
                this.captain.crew.email3 = this.crew.email3,
                this.captain.crew.name4 = this.crew.name4,
                this.captain.crew.email4 = this.crew.email4,
                this.captain.crew.name5 = this.crew.name5,
                this.captain.crew.email5 = this.crew.email5,
                this.captain.crew.name6 = this.crew.name6,
                this.captain.crew.email6 = this.crew.email6,
                this.captain.crew.joined_crew = this.crew.joined_crew,
                console.log('#CAT340 THIS.CAPTAIN: ', this.captain);
            console.log('#CAT350 THIS.CREW: ', this.crew);
            this._service.createCaptainEvent(this.captain, function (res) {
                console.log('#CAT360 RES: ', res);
                _this.closeModal();
                _this.update_event(res);
            });
        }
    };
    // #CAT400
    CalendarComponent.prototype.captain_submit = function () {
        var _this = this;
        this.captain.crew.name1 = this.crew.name1,
            this.captain.crew.email1 = this.crew.email1,
            this.captain.crew.name2 = this.crew.name2,
            this.captain.crew.email2 = this.crew.email2,
            this.captain.crew.name3 = this.crew.name3,
            this.captain.crew.email3 = this.crew.email3,
            this.captain.crew.name4 = this.crew.name4,
            this.captain.crew.email4 = this.crew.email4,
            this.captain.crew.name5 = this.crew.name5,
            this.captain.crew.email5 = this.crew.email5,
            this.captain.crew.name6 = this.crew.name6,
            this.captain.crew.email6 = this.crew.email6,
            this.captain.crew.joined_crew = this.crew.joined_crew,
            console.log('#CAT410 THIS.STUDENT: ', this.captain);
        this.captain.title = 'Captain ' + this._service.currentUser.name + ' Seeking Crew!';
        this._service.createCaptainEvent(this.captain, function (res) {
            console.log('#CAT420 RES: ', res);
            _this.closeModal();
            _this.update_event(res);
        });
    };
    CalendarComponent.prototype.closeModal = function () {
        $('.modal').fadeOut();
        this.student = {
            title: '',
            date: null,
            timeFrom: null,
            timeTo: null,
            message: '',
            crew: {
                name1: '',
                email1: '',
                name2: '',
                email2: '',
                name3: '',
                email3: '',
                name4: '',
                email4: '',
                name5: '',
                email5: '',
                name6: '',
                email6: '',
                joined_crew: null,
            }
        };
        this.captain = {
            date: null,
            title: '',
            timeFrom: null,
            timeTo: null,
            vessel: '',
            spec: '',
            NumOfCrew: '',
            message: '',
            crew: {
                name1: '',
                email1: '',
                name2: '',
                email2: '',
                name3: '',
                email3: '',
                name4: '',
                email4: '',
                name5: '',
                email5: '',
                name6: '',
                email6: '',
                joined_crew: null,
            }
        };
        this.saveTime = {
            start: [],
            to: []
        };
    };
    CalendarComponent.prototype.timeFrom = function () {
        var _this = this;
        var amazingTimePicker = this.atp.open();
        amazingTimePicker.afterClose().subscribe(function (time) {
            console.log(time);
            var parts = time.match(/(\d+)\:(\d+)/);
            _this.saveTime.start[0] = time;
            _this.saveTime.start[1] = parseInt(parts[1]) * 60 + parseInt(parts[2]);
            _this.saveTime.start[2] = parseInt(parts[1]);
            _this.saveTime.start[3] = parseInt(parts[2]);
            if (_this.saveTime.to[1] === undefined || _this.saveTime.start[1] <= _this.saveTime.to[1]) {
                _this.student.timeFrom = time;
                _this.captain.timeFrom = time;
                _this.eventUpdate.timeFrom = time;
            }
        });
    };
    CalendarComponent.prototype.timeTo = function () {
        var _this = this;
        var amazingTimePicker = this.atp.open();
        amazingTimePicker.afterClose().subscribe(function (time) {
            console.log(time);
            var parts = time.match(/(\d+)\:(\d+)/);
            _this.saveTime.to[0] = time;
            _this.saveTime.to[1] = parseInt(parts[1]) * 60 + parseInt(parts[2]);
            _this.saveTime.to[2] = parseInt(parts[1]);
            _this.saveTime.to[3] = parseInt(parts[2]);
            if (_this.saveTime.start[1] === undefined || _this.saveTime.start[1] <= _this.saveTime.to[1]) {
                _this.student.timeTo = time;
                _this.captain.timeTo = time;
                _this.eventUpdate.timeTo = time;
            }
        });
    };
    CalendarComponent.prototype.captainplus = function () {
        this.crew.name1 = this.logged_user;
        this.crew.email1 = this.logged_email;
        this.crewname = '';
        this.crewemail = '';
    };
    CalendarComponent.prototype.captainminus = function () {
        this.crew.name1 = 'We need a Captain!';
        this.crew.email1 = '';
        this.crewname = '';
        this.crewemail = '';
    };
    // #CAT400X
    CalendarComponent.prototype.lock_crew = function () {
        console.log('#CAT400X NUMOFCREW: ', this.eventUpdate.NumOfCrew);
        console.log('lock_crew :', this.crew.joined_crew);
        if (this.crew.joined_crew === 1 && this.crewname !== '') {
            this.crew.name1 = this.logged_user;
            this.crew.email1 = this.logged_email;
            this.crew.name2 = this.crewname;
            this.crew.email2 = this.crewemail;
            this.crewname = '';
            this.crewemail = '';
            this.crew.joined_crew = 2;
            console.log('2');
        }
        else if (this.crew.joined_crew === 2 && this.crewname !== '') {
            this.crew.name3 = this.crewname;
            this.crew.email3 = this.crewemail;
            this.crewname = '';
            this.crewemail = '';
            this.crew.joined_crew = 3;
            console.log('3');
        }
        else if (this.crew.joined_crew === 3 && this.crewname !== '') {
            this.crew.name4 = this.crewname;
            this.crew.email4 = this.crewemail;
            this.crewname = '';
            this.crewemail = '';
            this.crew.joined_crew = 4;
            console.log('4');
        }
        else if (this.crew.joined_crew === 4 && this.crewname !== '') {
            this.crew.name5 = this.crewname;
            this.crew.email5 = this.crewemail;
            this.crewname = '';
            this.crewemail = '';
            this.crew.joined_crew = 5;
            console.log('5');
        }
        else if (this.crew.joined_crew === 5 && this.crewname !== '') {
            console.log('6');
            this.crew.name6 = this.crewname;
            this.crew.email6 = this.crewemail;
            this.crewname = '';
            this.crewemail = '';
            this.crew.joined_crew = 6;
            this.crewlock.crew2_lock = true;
        }
        if (this.eventUpdate.NumOfCrew <= this.crew.joined_crew && this.eventUpdate.NumOfCrew) {
            console.log('this.eventUpdate.NumOfCrew: ', this.eventUpdate.NumOfCrew);
            console.log('this.crew.joined_crew: ', this.crew.joined_crew);
            console.log('LOCK');
            this.crewlock.crew2_lock = true;
        }
        if (this.captain.NumOfCrew <= this.crew.joined_crew && this.captain.NumOfCrew) {
            this.crewlock.crew2_lock = true;
        }
    };
    CalendarComponent.prototype.lock_crew_minus = function () {
        console.log('JOINED_CREW: ', this.crew.joined_crew);
        console.log('THIS.CREW: ', this.crew);
        if (this.crew.joined_crew === 6) {
            console.log('6');
            this.crew.name6 = undefined;
            this.crew.email6 = undefined;
            this.crew.joined_crew = 5;
            this.crewlock.crew2_lock = false;
        }
        else if (this.crew.joined_crew === 5) {
            this.crew.name5 = undefined;
            this.crew.email5 = undefined;
            this.crew.joined_crew = 4;
        }
        else if (this.crew.joined_crew === 4) {
            this.crew.name4 = undefined;
            this.crew.email4 = undefined;
            this.crew.joined_crew = 3;
        }
        else if (this.crew.joined_crew === 3) {
            this.crew.name3 = undefined;
            this.crew.email3 = undefined;
            this.crew.joined_crew = 2;
        }
        else if (this.crew.joined_crew === 2 && this.crew.name1 !== 'We need a Captain!') {
            this.crew.name2 = undefined;
            this.crew.email2 = undefined;
            this.crew.joined_crew = 1;
        }
        console.log('THIS.EVENTUPDATE: ', this.eventUpdate);
        if (this.eventUpdate.NumOfCrew > this.crew.joined_crew && this.eventUpdate.NumOfCrew) {
            this.crewlock.crew2_lock = false;
        }
    };
    CalendarComponent.prototype.student_req = function () {
        console.log('this._service.currentUser: ', this._service.currentUser);
        console.log('this._service.currentUser.identity: ', this._service.currentUser.identity);
        this.eventUpdate.NumOfCrew = 6;
        console.log('this.logged_user:', this._service.currentUser);
        this.logged_user = this._service.currentUser.name;
        this.logged_email = this._service.currentUser.email;
        this.logged_identity = this._service.currentUser.identity;
        console.log('THIS.LOGGED_IDENTITY: ', this.logged_identity);
        if (this.logged_identity === 'captain') {
            console.log('captain');
            this.crew = {
                name1: this.logged_user,
                email1: this.logged_email,
                name2: undefined,
                email2: undefined,
                name3: undefined,
                email3: undefined,
                name4: undefined,
                email4: undefined,
                name5: undefined,
                email5: undefined,
                name6: undefined,
                email6: undefined,
                joined_crew: 1,
            };
        }
        else if (this.logged_identity === 'student') {
            console.log('student');
            this.crew = {
                name1: 'We need a Captain!',
                email1: '',
                name2: this.logged_user,
                email2: this.logged_email,
                name3: undefined,
                email3: undefined,
                name4: undefined,
                email4: undefined,
                name5: undefined,
                email5: undefined,
                name6: undefined,
                email6: undefined,
                joined_crew: 2,
            };
        }
        console.log('THIS CREW: ', this.crew);
        this.crewlock.crew2_lock = false;
        document.getElementById('myModal1').style.display = 'none';
        $('#myModal2').fadeIn();
    };
    CalendarComponent.prototype.captain_req = function () {
        this.logged_user = this._service.currentUser.name;
        this.logged_email = this._service.currentUser.email;
        this.crew = {
            name1: this.logged_user,
            email1: this.logged_email,
            name2: undefined,
            email2: undefined,
            name3: undefined,
            email3: undefined,
            name4: undefined,
            email4: undefined,
            name5: undefined,
            email5: undefined,
            name6: undefined,
            email6: undefined,
            joined_crew: 1,
        };
        this.crewlock.crew2_lock = false;
        document.getElementById('myModal1').style.display = 'none';
        $('#myModal3').fadeIn();
    };
    CalendarComponent.prototype.display_calendar = function (eventsData) {
        var _this = this;
        var userId = this.user_data._id;
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
            },
            selectable: true,
            selectHelper: true,
            eventLimit: true,
            weekMode: 'liquid',
            url: '#',
            editable: true,
            eventClick: function (e) {
                console.log('this._service.currentUser: ', _this._service.currentUser);
                console.log('EVENT CLICKED');
                console.log('E: ', e);
                console.log('E.CREW: ', e.crew);
                console.log('CREW: ', _this.crew);
                _this.eventUpdate = {
                    title: e.title,
                    date: moment(e.date).format('DD MMM YYYY'),
                    timeFrom: e.timeFrom,
                    timeTo: e.timeTo,
                    vessel: e.vessel,
                    spec: e.spec,
                    message: e.message,
                    NumOfCrew: e.NumOfCrew,
                    identity: _this._service.currentUser.identity,
                    crew: {
                        name1: e.crew.name1,
                        email1: e.crew.email1,
                        name2: e.crew.name2,
                        email2: e.crew.email2,
                        name3: e.crew.name3,
                        email3: e.crew.email3,
                        name4: e.crew.name4,
                        email4: e.crew.email4,
                        name5: e.crew.name5,
                        email5: e.crew.email5,
                        name6: e.crew.name6,
                        email6: e.crew.email6,
                        joined_crew: e.crew.joined_crew
                    }
                };
                console.log('CLICK');
                _this.crew = {
                    name1: e.crew.name1,
                    email1: e.crew.email1,
                    name2: e.crew.name2,
                    email2: e.crew.email2,
                    name3: e.crew.name3,
                    email3: e.crew.email3,
                    name4: e.crew.name4,
                    email4: e.crew.email4,
                    name5: e.crew.name5,
                    email5: e.crew.email5,
                    name6: e.crew.name6,
                    email6: e.crew.email6,
                    joined_crew: e.crew.joined_crew,
                };
                if (e.crew.joined_crew === 6) {
                    console.log(6);
                    _this.crewlock.crew2_lock = true;
                }
                console.log('CREW: ', _this.crew);
                console.log('CREW: ', _this.crew);
                console.log('e.joined_crew: ', e.joined_crew);
                console.log('CLICK EVENTUPDATE: ', _this.eventUpdate);
                console.log('E.DATEe.date: ', e.date);
                console.log('E.TITLE: ', e.title);
                e.date = moment(e.date).format('DD MMM YYYY');
                _this.event_created_by = e.created_by;
                _this.event_id = e._id;
                console.log('this.user_data._id: ', _this.user_data._id);
                console.log('this.event_created_by: ', _this.event_created_by);
                if (_this.loginUser === 'none') {
                    if (e.title.includes('Captain')) {
                        console.log('NOT LOGGED IN CAPTAIN EVENT');
                        $('#myModal0').fadeIn();
                        $('#date').html("Setting sail on: " + e.date);
                        $('#title').html("" + e.title);
                        $('#timeRange').html("Set sail between: " + e.timeFrom + " to " + e.timeTo);
                        $('#vessel').html("Vessel: " + e.vessel);
                        $('#numCrew').html("Vessel capacity: " + e.NumOfCrew + " crew.");
                        $('#Message').html("Captains Log (Message): " + e.message);
                        $('#JoinCrew').html("Join Captain " + _this.crew.name1 + "'s Crew");
                        $('#name1').html("Crewman #1: " + _this.crew.name1);
                        $('#email1').html("Crewman #1's Email: " + _this.crew.email1);
                        $('#name2').html("Crewman #2: " + _this.crew.name2);
                        $('#email2').html("Crewman #2's Email: " + _this.crew.email2);
                        $('#name3').html("Crewman #3: " + _this.crew.name3);
                        $('#email3').html("Crewman #3's Email: " + _this.crew.email3);
                        $('#name4').html("Crewman #4: " + _this.crew.name4);
                        $('#email4').html("Crewman #4's Email: " + _this.crew.email4);
                        $('#name5').html("Crewman #5: " + _this.crew.name5);
                        $('#email5').html("Crewman #5's Email: " + _this.crew.email5);
                        $('#name6').html("Crewman #6: " + _this.crew.name6);
                        $('#email6').html("Crewman #6's Email: " + _this.crew.email6);
                        $('#joinedcrew').html("The number of sailors currently signed to this voyage: " + _this.crew.joined_crew);
                    }
                    else if (e.title.includes('Student')) {
                        console.log('NOT LOGGED IN STUDENT EVENT');
                        $('#myModal0').fadeIn();
                        $('#date').html("The student would like to set sail on: " + e.date);
                        $('#title').html("" + e.title);
                        $('#numCrew').html("");
                        $('#timeRange').html("Setting sail between: " + e.timeFrom + " to " + e.timeTo);
                        $('#Message').html("Message to Would-be crew: " + e.message);
                        $('#name1').html("Crewman #1: " + _this.crew.name1);
                        $('#email1').html("Crewman #1's Email: " + _this.crew.email1);
                        $('#name2').html("Crewman #1: " + _this.crew.name2);
                        $('#email2').html("Crewman #1's Email: " + _this.crew.email2);
                        $('#name3').html("Crewman #2: " + _this.crew.name3);
                        $('#email3').html("Crewman #2's Email: " + _this.crew.email3);
                        $('#name4').html("Crewman #3: " + _this.crew.name4);
                        $('#email4').html("Crewman #3's Email: " + _this.crew.email4);
                        $('#name5').html("Crewman #4: " + _this.crew.name5);
                        $('#email5').html("Crewman #4's Email: " + _this.crew.email5);
                        $('#name6').html("Crewman #5: " + _this.crew.name6);
                        $('#email6').html("Crewman #5's Email: " + _this.crew.email6);
                        $('#joinedcrew').html("The number of sailors currently signed to this voyage: " + _this.crew.joined_crew);
                    }
                }
                else {
                    if (_this.user_data._id === e.created_by) {
                        console.log('THIS.USER_DATA._ID: ', _this.user_data._id, 'E.CREATED_BY: ', e.created_by, 'E.TITLE: ', e.title);
                        if (e.title.includes('Crew')) {
                            console.log('#CAM500');
                            $('#myModal4').fadeIn();
                        }
                        else if (e.title.includes('Vessel')) {
                            console.log('#CAM600');
                            $('#myModal5').fadeIn();
                        }
                    }
                    else {
                        if (e.title.includes('Captain')) {
                            if (e.crew.joined_crew == e.NumOfCrew) {
                                _this.crewlock.crew2_lock = true;
                            }
                            console.log('OTHER EVENT CREW');
                            $('#myModal0').fadeIn();
                            $('#date').html("Setting sail on: " + e.date);
                            $('#title').html("" + e.title);
                            $('#timeRange').html("Setting sail between: " + e.timeFrom + " to " + e.timeTo);
                            $('#vessel').html("Vessel: " + e.vessel);
                            $('#numCrew').html("Vessel capacity: " + e.NumOfCrew);
                            $('#Message').html("Captains Log (Message): " + e.message);
                            $('#JoinCrew').html("Join Captain " + _this.crew.name1 + "'s Crew");
                            $('#name1').html("The Captain: " + _this.crew.name1);
                            $('#email1').html("The Captain's Email: " + _this.crew.email1);
                            $('#name2').html("Crewman #2: " + _this.crew.name2);
                            $('#email2').html("Crewman #2's Email: " + _this.crew.email2);
                            $('#name3').html("Crewman #3: " + _this.crew.name3);
                            $('#email3').html("Crewman #3's Email: " + _this.crew.email3);
                            $('#name4').html("Crewman #4: " + _this.crew.name4);
                            $('#email4').html("Crewman #4's Email: " + _this.crew.email4);
                            $('#name5').html("Crewman #5: " + _this.crew.name5);
                            $('#email5').html("Crewman #5's Email: " + _this.crew.email5);
                            $('#name6').html("Crewman #5: " + _this.crew.name6);
                            $('#email6').html("Crewman #6's Email: " + _this.crew.email6);
                            $('#joinedcrew').html("There are: " + _this.crew.joined_crew + " sailors signed up.");
                        }
                        else if (e.title.includes('Student')) {
                            console.log('OTHER EVENT VESSEL');
                            console.log('THIS.CREW.NAME2: ', _this.crew.name2);
                            $('#myModal0').fadeIn();
                            $('#date').html("Setting sail on: " + e.date);
                            $('#title').html("" + e.title);
                            $('#timeRange').html("Setting sail between: " + e.timeFrom + " to " + e.timeTo);
                            $('#Message').html("Message to Would-be Captains: " + e.message);
                            $('#JoinCrew').html("Join Captain " + _this.crew.name1 + " Crew");
                            $('#JoinCrew').html("Join Captain " + _this.crew.name1 + "'s Crew");
                            $('#name1').html("The Captain: " + _this.crew.name1);
                            $('#email1').html("The Captain's Email: " + _this.crew.email1);
                            $('#name2').html("Crewman #2: " + _this.crew.name2);
                            $('#email2').html("Crewman #2's Email: " + _this.crew.email2);
                            $('#name3').html("Crewman #3: " + _this.crew.name3);
                            $('#email3').html("Crewman #3's Email: " + _this.crew.email3);
                            $('#name4').html("Crewman #4: " + _this.crew.name4);
                            $('#email4').html("Crewman #4's Email: " + _this.crew.email4);
                            $('#name5').html("Crewman #5: " + _this.crew.name5);
                            $('#email5').html("Crewman #5's Email: " + _this.crew.email5);
                            $('#name6').html("Crewman #6: " + _this.crew.name6);
                            $('#email6').html("Crewman #6's Email: " + _this.crew.email6);
                            if (_this.crew.name1 === 'We need a Captain!') {
                                $('#joinedcrew').html("There are: " + (_this.crew.joined_crew - 1) + " sailors signed up.");
                            }
                            else {
                                $('#joinedcrew').html("There are: " + _this.crew.joined_crew + " sailors signed up.");
                            }
                        }
                    }
                }
            },
            events: eventsData,
            dayClick: function (date, jsEvent, view) {
                if (moment().format('YYYY-MM-DD') === date.format('YYYY-MM-DD') || date.isAfter(moment())) {
                    _this.student.date = date;
                    _this.captain.date = date;
                    _this.date_display = moment(date).format('DD MMM YYYY');
                    $('#myModal1').fadeIn();
                }
            },
            eventAfterRender: function (event, element, view) {
                if (event.vessel !== undefined) {
                    element.css('background-color', 'rgba(179, 225, 247, 1)');
                    element.css('border', 'none');
                }
                else {
                    element.css('background-color', '#CDDC39');
                    element.css('border', 'none');
                }
            },
            displayEventTime: false
        });
    };
    CalendarComponent.prototype.req_login = function (state) {
        if (state === 'reg') {
            var data = [{
                    user: null,
                    mesg: state
                }];
            this._service.updateLoginStatus(data);
        }
        else if (state === 'log') {
            var data = [{
                    user: null,
                    mesg: state
                }];
            this._service.updateLoginStatus(data);
        }
        else {
            return;
        }
        $('#myModal1').fadeOut();
    };
    CalendarComponent.prototype.delete = function (eventId) {
        var _this = this;
        console.log('click', this.event_id);
        this._service.delete_event(this.event_id, this.loginUser, function (res) {
            _this.closeModal();
            _this.update_event(res);
        });
    };
    // #CAT500
    CalendarComponent.prototype.event_update = function () {
        var _this = this;
        this.eventUpdate.crew.name1 = this.crew.name1;
        this.eventUpdate.crew.email1 = this.crew.email1;
        this.eventUpdate.crew.name2 = this.crew.name2;
        this.eventUpdate.crew.email2 = this.crew.email2;
        this.eventUpdate.crew.name3 = this.crew.name3;
        this.eventUpdate.crew.email3 = this.crew.email3;
        this.eventUpdate.crew.name4 = this.crew.name4;
        this.eventUpdate.crew.email4 = this.crew.email4;
        this.eventUpdate.crew.name5 = this.crew.name5;
        this.eventUpdate.crew.email5 = this.crew.email5;
        this.eventUpdate.crew.name6 = this.crew.name6;
        this.eventUpdate.crew.email6 = this.crew.email6;
        this.eventUpdate.crew.joined_crew = this.crew.joined_crew;
        console.log('#CAT501 EVENTUPDATE: ', this.eventUpdate);
        console.log('#CAT502 EVENT_ID: ', this.event_id);
        this._service.event_update(this.event_id, this.eventUpdate, function (res) {
            _this.closeModal();
            _this.update_event(res);
        });
    };
    // #CAT1000  ADD_CREW_SUBMIT()
    CalendarComponent.prototype.add_crew_submit = function () {
        var _this = this;
        this.eventUpdate.crew.name1 = this.crew.name1;
        this.eventUpdate.crew.email1 = this.crew.email1;
        this.eventUpdate.crew.name2 = this.crew.name2;
        this.eventUpdate.crew.email2 = this.crew.email2;
        this.eventUpdate.crew.name3 = this.crew.name3;
        this.eventUpdate.crew.email3 = this.crew.email3;
        this.eventUpdate.crew.name4 = this.crew.name4;
        this.eventUpdate.crew.email4 = this.crew.email4;
        this.eventUpdate.crew.name5 = this.crew.name5;
        this.eventUpdate.crew.email5 = this.crew.email5;
        this.eventUpdate.crew.name6 = this.crew.name6;
        this.eventUpdate.crew.email6 = this.crew.email6;
        this.eventUpdate.crew.joined_crew = this.crew.joined_crew;
        console.log('#CAT1010 THIS.EVENTUPDATE: ', this.eventUpdate);
        console.log('#CAT1020 CREW: ', this.crew);
        console.log('#CAT1030 THIS.EVENT_ID: ', this.event_id);
        // #CAT1040 SUBMIT TO #SE100 ADD_CREW_SUBMIT() //
        this._service.add_crew_submit(this.event_id, this.eventUpdate, function (res) {
            console.log('#CAT1040 #SE100 RES: ', res);
            _this.closeModal();
            _this.update_event(res);
        });
    };
    CalendarComponent = __decorate([
        core_1.Component({
            selector: 'app-calendar',
            template: __webpack_require__("../../../../../src/app/calendar/calendar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/calendar/calendar.component.css")]
        }),
        __metadata("design:paramtypes", [amazing_time_picker_1.AmazingTimePickerService, main_service_1.MainService])
    ], CalendarComponent);
    return CalendarComponent;
}());
exports.CalendarComponent = CalendarComponent;


/***/ }),

/***/ "../../../../../src/app/check-email/check-email.component.css":
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__("../../../../css-loader/lib/url/escape.js");
exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/* @import url(https://fonts.googleapis.com/css?family=Roboto:400,900,500); */\n.fullscreen {\n  background-image: url(" + escape(__webpack_require__("../../../../../src/assets/images/confirmPic.png")) + ");\n  height: 100vh;\n  width: 100vw;\n  position: relative;\n  background-size: cover;\n  background-repeat: no-repeat;\n  display: grid;\n  justify-items: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n@media screen and (max-width: 700px) {\n  .fullscreen {\n    background-image: url(" + escape(__webpack_require__("../../../../../src/assets/images/confirmPic.png")) + ");\n  }  \n}\n.fullscreen h1 {\n  font-family: 'Lucida Sans Unicode', 'Lucida Sans Regular', 'Lucida Grande',  Geneva, Verdana, sans-serif;\n  color: white;\n  position: relative;\n  /* top: -77px; */\n}\n.fullscreen h2 {\n  font-family: 'Lucida Sans Unicode', 'Lucida Sans Regular', 'Lucida Grande',  Geneva, Verdana, sans-serif;\n  color: white;\n  position: relative;\n  /* top: -77px; */\n}\n\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/check-email/check-email.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"fullscreen\">\n  <h1>Confirmation Email Sent!</h1>\n  <h2>Please check your confirmation email in your mail box. Thanks!</h2>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/check-email/check-email.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var CheckEmailComponent = /** @class */ (function () {
    function CheckEmailComponent() {
    }
    CheckEmailComponent.prototype.ngOnInit = function () {
    };
    CheckEmailComponent = __decorate([
        core_1.Component({
            selector: 'app-check-email',
            template: __webpack_require__("../../../../../src/app/check-email/check-email.component.html"),
            styles: [__webpack_require__("../../../../../src/app/check-email/check-email.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], CheckEmailComponent);
    return CheckEmailComponent;
}());
exports.CheckEmailComponent = CheckEmailComponent;


/***/ }),

/***/ "../../../../../src/app/display/display.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".container {\n    margin-top: 20px; \n    display: grid;\n    grid-gap: 20px;\n}\n\n/* Top Container */\n\n.top-container {\n    display: grid;\n    padding: 1em;\n    grid-gap: 20px;\n    grid-template-areas: \n    'top-box-a showcase showcase'\n    'top-box-b showcase showcase';\n}\n\n/* Showcasa */\n\n.showcase h1 {\n    font-size: 4rem;\n    margin-bottom: 0;\n}\n\n.showcase p {\n    font-size: 1.3rem;\n    margin-top: 0;\n}\n\nimg {\n    width: 100%;\n    background-size: cover;\n    background-position: center;\n    border: 5px solid #eee; \n    -webkit-box-shadow: 20px 15px 20px rgba(0, 0, 0, .5); \n            box-shadow: 20px 15px 20px rgba(0, 0, 0, .5);\n    -webkit-transform: rotate(-5deg);\n            transform: rotate(-5deg);\n}\n\n/* Top Box */\n\n.content {\n    display: grid;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-shadow: 0 1px 25px rgba(104, 104, 104, 0.8);\n            box-shadow: 0 1px 25px rgba(104, 104, 104, 0.8);\n    padding: 1.5rem;\n    border-radius: 10px;\n    font-size: 1rem;\n}\n\n@media screen and (max-width: 700px) {\n    .top-container {\n      grid-template-areas:\n        'showcase showcase',\n        'top-box-a top-box-b'; \n    }\n    \n    .showcase {\n        width: 100%;\n    }\n\n    .showcase h1 {\n        font-size: 2.5rem;\n    }\n\n    .showcase button {\n        font-size: 1rem;\n        padding: 2px;\n        width:200px;\n  }\n}\n\n@media screen and (max-width: 800px) {\n    .autoplay {\n        display: inline-block !important;\n        font-size: 1rem !important;\n     }\n\n     .top-container {\n        grid-template-areas: \n        'top-box-a'\n        'top-box-b'\n        'showcase'; \n     }\n    \n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/display/display.component.html":
/***/ (function(module, exports) {

module.exports = "<body>\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-md-8\">\n        <p class=\"content\">\n          <strong>Welcome</strong>\n          to the Great Pond Yacht Club women’s learn-to-sail program. This program is designed to give women the tools and\n          know-how to sail with confidence in whatever capacity they choose. </p>\n        <p class=\"content\">\n          <strong>Mentorship</strong>\n          The pressure-free mentor approach and flexible schedule will give you the freedom to set your own learning pace.\n          The sailing mentors (captains) are GPYC members that look forward to getting to know you and helping you learn;\n          whether your goals are to become a comfortable mate or to take the helm and captain your first race. We can help!</p>\n      </div>\n      <div class=\"col-md-4\">\n        <div class=\"imgAbt\">\n          <img width=\"300\" height=\"300\" src=\"../../assets/images/rainbow.jpg\" />\n        </div>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col-md-8\">\n        <p class=\"content\">\n          <strong>How it works</strong>\n          All potential students first create a profile with basic information. There is no commitment to sail at this time.\n          However, to sign up for a sail, all students must have a WSP Registration code.</p>\n        <p class=\"content\">\n          <strong>Captains</strong>\n          The captains are your mentors and are offering the sailing instruction on the day and time listed on the events calendar.\n        </p>\n      </div>\n\n      <div class=\"col-md-4\">\n        <div class=\"imgAbt\">\n          <img width=\"300\" height=\"300\" src=\"../../assets/images/whiteboat.jpg\" />\n        </div>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col-md-8\">\n        <p class=\"content\">\n          <strong>Events Calendar</strong>\n          On our events calendar you will see names of members who have offered to Captain a vessel or who have requested a Captain. If you are logged in, clicking on the event will\n          take you to a popout displaying the event specifics from the captain or student. Events created by captains appear in blue, and events created by students appear in green. \n          As long as the boat is not already at capacity, you may add your name and email (optional) to the event. Up to 6 people may sign up for an event. Be aware that for flexibility, any signed-in member may remove any crew-member, so don't delete \n          your friends by mistake! If you add your email, you will receive an email when the event changes.</p>\n        <p class=\"content\">\n          <strong>Request a Captain</strong>\n          Students may request a Captain/vessel on the calendar. Just click on the day within the calendar that\n          you are hoping to sail. For example, if you are hoping to sail on Tuesday, July 9, click on the day within the calendar, then 'seeking a vessel' when it pops up, \n          and then fill in the specifics of when you would like to sail, and anyone you would like to bring! If you already have access to a vessel, just note that in the message. If you want to request a specific person to be your captain/crew, enter \n          something like 'want to Captain?' under the name field, and the persons email, and they will get an email!</p>\n      </div>\n      <div class=\"col-md-4\">\n        <div class=\"imgAbt\">\n          <img width=\"300\" height=\"300\" src=\"../../assets/images/boat.jpg\" />\n        </div>\n      </div>\n    </div>\n  </div>\n</body>"

/***/ }),

/***/ "../../../../../src/app/display/display.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var DisplayComponent = /** @class */ (function () {
    function DisplayComponent() {
    }
    DisplayComponent.prototype.ngOnInit = function () {
    };
    DisplayComponent = __decorate([
        core_1.Component({
            selector: 'app-display',
            template: __webpack_require__("../../../../../src/app/display/display.component.html"),
            styles: [__webpack_require__("../../../../../src/app/display/display.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], DisplayComponent);
    return DisplayComponent;
}());
exports.DisplayComponent = DisplayComponent;


/***/ }),

/***/ "../../../../../src/app/footer/footer.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "html,\nbody {\n    margin: 0;\n    display: table;\n}\n\n.wrapper {\n    padding: 20px;\n    height: auto;\n}\n\nfooter {\n    background-color: black;\n    display: tabl-row;\n    height: auto;\n}\n\nimg {\n    width: 62px;\n    height: 80px;\n    margin: auto;\n    background: white;\n}\n\np {\n  color: gold;\n  font-size: 1rem;\n  font-weight: bold;\n}\n\nfooter p a {\n  color: gold;\n  text-decoration: underline;\n  font-size: 1rem;\n  font-weight: bold;\n}\n\n.bottom {\n  margin: 0;\n  padding: 0;\n}\n\n.footerP {\n  padding-top: 20px;\n}\n\n/* \n\n* {\n  margin: 0;\n  padding: 0;\n  -moz-box-sizing: border-box;\n  -webkit-box-sizing: border-box;\n  box-sizing: border-box;\n}\n\nhtml, body {\n  font-family: Raleway, sans-serif;\n  background-color: #fff;\n}\n\nfooter {\n  width: 100%;\n  display: inline-block;\n  margin-bottom: -6px;\n  margin-top: 25px;\n  height: 70px;\n  background-color: #999;\n}\nfooter .copyright {\n  width: 50%;\n  float: left;\n}\n@media (max-width: 600px) {\n  footer .copyright {\n    width: 100%;\n  }\n}\nfooter .copyright p {\n  padding-left: 10%;\n  color: white;\n  font-size: 0.8em;\n  line-height: 70px;\n  text-transform: capitalize;\n  letter-spacing: 1px;\n}\n@media (max-width: 600px) {\n  footer .copyright p {\n    text-align: center;\n    padding: 0;\n  }\n}\nfooter .social {\n  width: 50%;\n  float: right;\n}\n@media (max-width: 600px) {\n  footer .social {\n    width: 100%;\n  }\n}\nfooter .social a {\n  float: left;\n  line-height: 70px;\n  text-decoration: none;\n  color: white;\n  text-align: center;\n  font-weight: bold;\n  -moz-transition: all, 0.3s;\n  -o-transition: all, 0.3s;\n  -webkit-transition: all, 0.3s;\n  transition: all, 0.3s;\n  font-size: 18px;\n}\nfooter .social a:hover {\n  background-color: #222;\n  -moz-transition: all, 0.3s;\n  -o-transition: all, 0.3s;\n  -webkit-transition: all, 0.3s;\n  transition: all, 0.3s;\n}\n\n.linked {\n  background-color: #1E83AE;\n  font-size: 1.2em;\n  width: 20%;\n}\n\n.face {\n  background-color: #3D5B94;\n  font-size: 1.2em;\n  width: 20%;\n}\n\n.tweet {\n  background-color: #3DACDD;\n  font-size: 1.2em;\n  width: 20%;\n}\n\n.support {\n  background-color: #6dcd9a;\n  font-size: 0.8em;\n  width: 40%;\n}\n\nimg{\n    width: 58px;\n    height: 58px;\n    margin: auto;\n} */", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/footer/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<footer class=\" page-footer font-small stylish-color-dark pt-4 mt-4\">\n    <div class=\"wrapper container text-center text-md-left\">\n      <div class=\"row bottom\">\n        <div class=\"col-md-4 footerP\">\n          <p>GPYC WSP supports Vets in Tech</p>\n        </div>\n        <div class=\"row col-md-2 mx-auto\">\n          <img alt=\"logo\" src=\"../../assets/images/sapper_coding_logo.svg\">\n        </div>\n        <div class=\"col-md-4 footerP\">\n          <p><a href=\"https://sappercoding.com/home\">This site was made by Sapper Coding</a> </p>\n        </div>\n      </div>\n    </div>\n  </footer>\n<!-- \n  <footer>\n    <div class=\"copyright\">\n      <p><img src=\"../../assets/images/brand.png\" alt=\"\">&copy 2018 - GPYC SAILING PROGRAM</p>\n      \n    </div>\n    <div class=\"social\">\n      <a href=\"#\" class=\"support\">Contact Us</a>\n      <a href=\"#\" class=\"face\">f</a>\n      <a href=\"#\" class=\"tweet\">t</a>\n      <a href=\"#\" class=\"linked\">in</a>\n    </div>\n  </footer> -->"

/***/ }),

/***/ "../../../../../src/app/footer/footer.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        core_1.Component({
            selector: 'app-footer',
            template: __webpack_require__("../../../../../src/app/footer/footer.component.html"),
            styles: [__webpack_require__("../../../../../src/app/footer/footer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());
exports.FooterComponent = FooterComponent;


/***/ }),

/***/ "../../../../../src/app/forgetpw/forgetpw.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".centered form {\n    position: fixed;\n    top: 50%;\n    left: 50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%);\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/forgetpw/forgetpw.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"centered\">\n    <!-- #PW100 -->\n      <form (submit)=\"forgetpw()\" id=\"login-form\" role=\"form\">\n        <h2>Forgot your account's password? Enter your email address and we'll send you a recovery link.</h2>\n    \n        <div class=\"form-group\">\n          <label for=\"\">Your Email: </label>\n          <input type=\"text\" class=\"form-control\" id=\"login-username\" placeholder=\"Enter Email...\" name=\"email\" [(ngModel)]=\"inputEmail\"\n            #log_email=\"ngModel\" required>\n          <div *ngIf=\"log_email.invalid && (log_email.dirty || log_email.touched)\">\n            <div class=\"alert alert-dismissible alert-danger aler_log\" role=\"alert\">\n              <div *ngIf=\"log_email.errors.required\">\n                <span> Email is required.</span>\n              </div>\n            </div>\n          </div>\n          <p *ngIf = \"error_message !== null\">{{error_message}}</p>\n        </div>\n    \n        <div class=\"modal-footer\">\n          <button id=\"btn-login\" class=\"btn btn-success\">Send Recovery Email</button>\n        </div>\n        <p *ngIf=\"getEmail==true\">A recovery link has been send to your Email</p>\n        \n        <br/>\n      </form>\n    </div>"

/***/ }),

/***/ "../../../../../src/app/forgetpw/forgetpw.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var main_service_1 = __webpack_require__("../../../../../src/app/main.service.ts");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var ForgetpwComponent = /** @class */ (function () {
    function ForgetpwComponent(_service, _router) {
        this._service = _service;
        this._router = _router;
        this.error_message = null;
        this.getEmail = false;
    }
    ForgetpwComponent.prototype.ngOnInit = function () {
    };
    // #PWT900
    ForgetpwComponent.prototype.forgetpw = function () {
        var _this = this;
        console.log('#PWT100 this.inputEmail: ', this.inputEmail);
        this._service.forgetpw(this.inputEmail, function (res) {
            if (res.err) {
                _this.error_message = res.err;
            }
            else if (res.succ === 'success send recovery link.') {
                _this.getEmail = true;
            }
        });
    };
    ForgetpwComponent = __decorate([
        core_1.Component({
            selector: 'app-forgetpw',
            template: __webpack_require__("../../../../../src/app/forgetpw/forgetpw.component.html"),
            styles: [__webpack_require__("../../../../../src/app/forgetpw/forgetpw.component.css")]
        }),
        __metadata("design:paramtypes", [main_service_1.MainService, router_1.Router])
    ], ForgetpwComponent);
    return ForgetpwComponent;
}());
exports.ForgetpwComponent = ForgetpwComponent;


/***/ }),

/***/ "../../../../../src/app/header/header.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "body {\n    margin: 0;\n}\n\n#wrapper {\n    display: grid;\n    background: url(/assets/images/cover.jpg) no-repeat center center;\n    -webkit-background-attachment: fixed;\n    background-attachment: fixed;\n    background-size: cover;\n    padding: 0;\n    position: relative;\n    height: 100vh;\n    width: 100%;\n}\n\n#service {\n    background-color: #1a1e23;\n    color: bisque;\n}\n\n#content h2 {\n    font-size: 3rem;\n}\n\n#content h3 {\n    font-size: 3rem;\n}\n\n.container {\n    padding: 10px;\n}\n\n.wordWrapper {\n    text-align: center;\n    /* display: flex; */\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    left: 50% !important;\n    margin: auto;\n}\n\n.center {\n    margin: auto;\n}\n\nbutton {\n    background-color: Transparent;\n    background-repeat: no-repeat;\n    cursor: pointer;\n    overflow: hidden;\n    color: #fff;\n    margin: 3vh 1vw !important;\n    padding: 0.6rem 1.3rem;\n    border: 2px solid #fff;\n    font-size: 2em;\n    width: 350px;\n}\n\nh1 {\n    color: #fff;\n    font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;\n    font-size: 3.5rem;\n    font-weight: bold;\n    border-right: 4px solid rgba(255, 255, 255, 0);\n    text-shadow: 2px 2px 0 rgba(0, 0, 0, .75);\n    overflow: hidden;\n    white-space: nowrap;\n    -webkit-animation: typing 10s steps(27) forwards, blink-caret .8s step-end infinite;\n            animation: typing 10s steps(27) forwards, blink-caret .8s step-end infinite;\n    -webkit-animation-iteration-count: infinite;\n            animation-iteration-count: infinite;\n}\n\n.arrow {\n    color: white;\n}\n\n/* Scroll Up Arrow */\n\n#return-to-top {\n    font-size: 50px;\n    color: #fff;\n    position: fixed;\n    border: 5px solid #fff;\n    bottom: 20px;\n    right: 20px;\n    background: rgba(0, 0, 0, 0.5);\n    cursor: pointer;\n    width: 1.5em;\n    height: 1.5em;\n    text-align: center;\n    border-radius: 50%;\n    margin-bottom: 137px;\n    margin-right: -18px;\n}\n\n#return-to-top:hover {\n    border: 1px solid;\n    -webkit-box-shadow: inset 0 0 20px rgba(255, 255, 255, .5), 0 0 20px rgba(90, 47, 47, 0.2);\n            box-shadow: inset 0 0 20px rgba(255, 255, 255, .5), 0 0 20px rgba(90, 47, 47, 0.2);\n    outline-color: rgba(161, 204, 7, 0);\n    outline-offset: 15px;\n    text-shadow: 1px 1px 2px #427388;\n}\n\n/* @keyframes blink-caret {\n    from,\n    to {\n        border-color: transparent;\n    }\n    50% {\n        border-color: #fff;\n    }\n} */\n\n/* @keyframes typing {\n    0% {\n        width: 0%;\n    }\n    30% {\n        width: 100%;\n    }\n    80% {\n        width: 100%;\n    }\n    90% {\n        width: 0%;\n    }\n    100% {\n        width: 0%;\n    }\n} */\n\n@media only screen and (max-width: 330px) {\n    h1 {\n        font-size: 17px !important;\n    }\n\n    .wordWrapper button {\n        font-size: 1em;\n        width: 295px;\n    }\n}\n\n@media only screen and (width: 360px) {\n    h1 {\n        font-size: 1.25rem !important;\n    }\n}\n\n@media only screen and (width: 375px) {\n    h1 {\n        font-size: 1.25rem !important;\n    }\n}\n\n@media only screen and (width: 414px) {\n    h1 {\n        font-size: 1.25rem !important;\n    }\n}\n\n@media only screen and (width: 411px) {\n    h1 {\n        font-size: 1.25rem !important;\n    }\n}\n\n@media only screen and (width: 600px) {\n     h1 {\n        font-size: 1.25rem !important;\n    }\n}\n\n@media only screen and (width: 768px) {\n    h1 {\n        font-size: 2rem !important;\n    }\n    .btn_event_calendar{\n        font-size: 15px;\n        width: 156px;\n    }\n    .btn_jump_login {\n        font-size: 15px;\n        width: 156px;        \n    }\n}\n\n@media only screen and (min-width: 400px) {\n    h1 {\n        font-size: 1.2rem !important;\n    }\n}\n\n@media only screen and (min-width: 500px) {\n    h1 {\n        font-size: 1.5rem !important;\n    }\n}\n\n@media only screen and (min-width: 600px) {\n    h1 {\n        font-size: 2rem !important;\n    }\n}\n\n@media only screen and (min-width: 700px) {\n    h1 {\n        font-size: 2.3rem !important;\n    }\n}\n\n@media only screen and (min-width: 800px) {\n    h1 {\n        font-size: 2.5rem !important;\n    }\n}\n\n@media only screen and (min-width: 992px) {\n    h1 {\n        font-size: 3.5rem !important;\n    }\n}\n\n@media only screen and (min-width: 1024px) {\n    h1 {\n        font-size: 3.5rem !important;\n    }\n}\n\n@media only screen and (min-width: 1100px) {\n    h1 {\n        font-size: 3.5rem !important;\n    }\n}\n\n@media only screen and (min-width: 1200px) {\n    h1 {\n        font-size: 3.5rem !important;\n    }\n}\n\n\n\n\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/header/header.component.html":
/***/ (function(module, exports) {

module.exports = "<html lang=\"en\">\n<head>\n  <meta charset=\"UTF-8\">\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n  <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n  <title>Document</title>\n</head>\n\n<body>\n  <section id=\"wrapper\">\n    <header class=\"wordWrapper\">\n      <h1>GPYC WOMEN'S SAILING PROGRAM</h1>\n      <button type=\"submit\" class=\"btn btn-primary btn_event_calendar\" (click) =\"scrollToCalendar()\">Events Calendar</button>\n      <button type=\"submit\" class=\"btn btn-primary btn_jump_login\" (click) =\"display_login()\" *ngIf = \"currentuser == ''\">Jump to Login</button>\n\n    </header>\n\n  </section>\n\n  <section id=\"service\">\n    <div class=\"container\">\n      <div class=\"row justify-content-md-center\">\n        <div id=\"content\" class=\"col-lg-8\">\n          <h2 class=\"text-center mt-4\">\n            <i style=\"font-size: 5rem;\" class=\"ion-android-boat\"></i>\n            <strong>Come Sail With Us!</strong>\n          </h2>\n          <hr>\n          \n        </div>\n      </div>\n    </div>\n  </section>\n  <a id=\"return-to-top\" (click)=\"arrowClick($event)\">\n    <img src=\"../../assets/images/arrow_up.svg\" alt=\"\">\n  </a>\n\n</body>\n\n</html>"

/***/ }),

/***/ "../../../../../src/app/header/header.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var main_service_1 = __webpack_require__("../../../../../src/app/main.service.ts");
var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(_service) {
        this._service = _service;
    }
    HeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._service.checkLogin.subscribe(function (res) {
            console.log(res);
            if (res[0] == "loged") {
                _this.currentuser = "loged";
            }
            else {
                _this.currentuser = "";
            }
        });
        console.log(this.currentuser);
        $('#return-to-top').hide();
        $(window).scroll(function () {
            var scrollval = $(window).scrollTop();
            if (scrollval > 100) {
                $('#return-to-top').fadeIn('slow');
            }
            else {
                $('#return-to-top').fadeOut('fast');
            }
        });
        // $('.textShow').hide();
    };
    HeaderComponent.prototype.arrowClick = function (event) {
        window.scroll({
            top: 0,
            behavior: 'smooth'
        });
    };
    HeaderComponent.prototype.scrollToCalendar = function () {
        this._service.scrollDownFromHeader.next(["Tocalendar"]);
    };
    HeaderComponent.prototype.display_login = function () {
        var data = [{
                user: null,
                mesg: "log"
            }];
        this._service.updateLoginStatus(data);
    };
    HeaderComponent = __decorate([
        core_1.Component({
            selector: 'app-header',
            template: __webpack_require__("../../../../../src/app/header/header.component.html"),
            styles: [__webpack_require__("../../../../../src/app/header/header.component.css")]
        }),
        __metadata("design:paramtypes", [main_service_1.MainService])
    ], HeaderComponent);
    return HeaderComponent;
}());
exports.HeaderComponent = HeaderComponent;


/***/ }),

/***/ "../../../../../src/app/main.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var http_1 = __webpack_require__("../../../http/esm5/http.js");
var BehaviorSubject_1 = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
var MainService = /** @class */ (function () {
    function MainService(_http) {
        this._http = _http;
        this.show = true;
        this.currentUser = null;
        this.currentRegcode = null;
        this.all_events = new BehaviorSubject_1.BehaviorSubject([]);
        this.loginstatus = new BehaviorSubject_1.BehaviorSubject([]);
        this.scrollDownFromHeader = new BehaviorSubject_1.BehaviorSubject([]);
        this.checkLogin = new BehaviorSubject_1.BehaviorSubject([]);
        if (localStorage.currentUser !== undefined) {
            this.currentUser = JSON.parse(localStorage.currentUser);
            console.log('this.currentUser: ', this.currentUser);
            var data = [{
                    user: this.currentUser,
                    mesg: null
                }];
            this.updateLoginStatus(data);
        }
        else {
            var data = [{
                    user: null,
                    mesg: null
                }];
            this.updateLoginStatus(data);
        }
    }
    MainService.prototype.getRegcode = function (callback) {
        console.log('getRegcode');
        this._http.get('/allregcodes').subscribe(function (res) {
            callback(res.json());
        }, function (err) {
            console.log('error 0 ');
        });
    };
    MainService.prototype.getAllEvents = function (callback) {
        this._http.get('/allevents').subscribe(function (res) {
            callback(res.json());
        }, function (err) {
            console.log('error 0 ');
        });
    };
    MainService.prototype.registerCap = function (data, callback) {
        this._http.post('/register', data).subscribe(function (res) {
            console.log('from service register: ', res.json());
            callback(res.json());
            if (res.json().success === 'register pending') {
                console.log('success');
            }
        }, function (err) {
            console.log(err);
        });
    };
    MainService.prototype.registerStudent = function (data, callback) {
        this._http.post('/register', data).subscribe(function (res) {
            console.log('from service register: ', res.json());
            callback(res.json());
            if (res.json().success === 'register pending') {
            }
        }, function (err) {
            console.log(err);
        });
    };
    // #SE410
    MainService.prototype.createCaptainEvent = function (event, callback) {
        this._http.post('/captainevents', { id: this.currentUser._id, event: event })
            .subscribe(function (res) {
            callback(res.json());
        }, function (err) {
            console.log('#SE410 ERROR: ', err);
        });
    };
    // #SE500
    MainService.prototype.event_update = function (id, data, callback) {
        console.log('from service update: ', id, data);
        this._http.put('/update/' + id, { data: data }).subscribe(function (res) {
            console.log('#SE500 RES: ', res);
            callback(res.json());
        }, function (err) {
            console.log('event_update err: ', err);
        });
    };
    // #SE320
    MainService.prototype.createStudentEvent = function (event, callback) {
        var _this = this;
        console.log('#SE320 THIS.CURRENTUSER: ', this.currentUser);
        console.log('#SE320 EVENT: ', event);
        this._http.post('/studentevents', { id: this.currentUser._id, event: event }).subscribe(function (res) {
            callback(res.json());
            var allEvents = res.json();
            _this.updateAllEvents(allEvents);
        }, function (err) {
            console.log('#SE330 ERROR 1: ', err);
        });
    };
    // #SE100 ADD_CREW_SUBMIT() - TS: #CAT1040 - RO: //
    MainService.prototype.add_crew_submit = function (event, data, callback) {
        console.log('#SE110 EVENT:', event, '#SE110 DATA: ', data);
        this._http.put('/updateCrew/' + event, { data: data }).subscribe(function (res) {
            console.log('#SE120 RES FROM RO:', res);
            callback(res.json());
        }, function (err) {
            console.log('#SE130 ERROR FROM RO:', err);
        });
    };
    // SE1300
    MainService.prototype.update_regcode = function (regcode, callback) {
        console.log('SE1300, REGCODE: ', regcode);
        this._http.put('/regcodeupdate', { regcode: regcode }).subscribe(function (res) {
            callback(res.json());
        }, function (err) {
            console.log('update regcode error: ', err);
        });
    };
    MainService.prototype.updateAllEvents = function (data) {
        this.all_events.next(data);
    };
    MainService.prototype.updateLoginStatus = function (data) {
        this.loginstatus.next(data);
    };
    MainService.prototype.getPendingUser = function (token, callback) {
        this._http.get("/activate_new/" + token).subscribe(function (res) {
            callback(res.json());
        }, function (err) {
            console.log('get pending user err: ', err);
        });
    };
    MainService.prototype.login = function (userdata, callback) {
        var _this = this;
        this._http.post('/login', userdata).subscribe(function (res) {
            callback(res.json());
            if (res.json().error === undefined) {
                _this.currentUser = res.json();
                localStorage.currentUser = JSON.stringify(res.json());
                console.log('LOCALSTORAGE.CURRENTUSER: ', localStorage.currentUser);
                console.log('THIS.CURRENTUSER: ', _this.currentUser);
                var data = [{
                        user: _this.currentUser,
                        mesg: null
                    }];
                _this.updateLoginStatus(data);
            }
        }, function (err) {
            console.log('error from login service: ', err);
        });
    };
    MainService.prototype.logout = function () {
        localStorage.removeItem('currentUser');
        console.log('LOCALSTORAGE: ', localStorage.currentUser);
    };
    MainService.prototype.delete_event = function (id, identity, callback) {
        console.log('event_id: ', id);
        this._http.delete("/delete/" + id + "/" + identity).subscribe(function (res) {
            callback(res.json());
        }, function (err) {
            console.log('delete event err: ', err);
        });
    };
    MainService.prototype.delete_user = function (email, callback) {
        console.log('Email: ', email, 'CALLBACK: ', callback);
        this._http.delete("/deleteUser/" + email).subscribe(function (res) {
            callback(res.json());
        }, function (err) {
            console.log('delete user err', err);
        });
    };
    // #SE900
    MainService.prototype.forgetpw = function (email, callback) {
        console.log('#SE910 EMAIL: ', email);
        this._http.post('/forgetpw', { email: email }).subscribe(function (res) {
            callback(res.json());
        }, function (err) {
            if (err) {
                console.log('forget password error', err);
            }
        });
    };
    MainService.prototype.resetpw = function (token, password, callback) {
        this._http.put('resetpw', { token: token, password: password }).subscribe(function (res) {
            callback(res.json());
        }, function (err) {
            console.log('reset password error', err);
        });
    };
    MainService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], MainService);
    return MainService;
}());
exports.MainService = MainService;


/***/ }),

/***/ "../../../../../src/app/navbar/navbar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".btn-primary {\n    color: #00FFFF;\n    background-color: transparent;\n    border: 3px solid #00FFFF;\n    font-weight: bold;\n}\n\n.btn-primary:hover {\n     background-color: #00FFFF;\n     color: #000;\n}\n\n.btn-success {\n    background-color: transparent;\n    color: #15E95C;\n    border: 3px solid #15E95C;\n    font-weight: bold;\n}\n\n.btn-success:hover {\n    background-color: #15E95C;\n    color: #000;\n}\n\nul {\n    list-style: none;\n}\n\nli {\n    display: inline-block;\n    padding: 3px\n}\n\n#menu-icon {\n    display: hidden;\n    width: 50px;\n    height: 50px;\n    background: #ffd701 url(\"/assets/images/menu-icon.png\") center;\n    border-radius: 9px;\n}\n\n/* The Modal (background) */\n\n.modal {\n    display: none;\n    /* Hidden by default */\n    position: fixed;\n    /* Stay in place */\n    z-index: 4;\n    /* Sit on top */\n    padding-top: 100px;\n    /* Location of the box */\n    left: 0;\n    top: 0;\n    width: 100%;\n    /* Full width */\n    height: 100%;\n    /* Full height */\n    overflow: auto;\n    /* Enable scroll if needed */\n    background-color: rgb(0, 0, 0);\n    /* Fallback color */\n    background-color: rgba(0, 0, 0, 0.4);\n    /* Black w/ opacity */\n}\n\n/* Modal Content */\n\n.modal-content {\n    background-color: #fefefe;\n    margin: auto;\n    padding: 20px;\n    border: 1px solid #888;\n    width: 60%;\n}\n\n/* The Close Button */\n\n.close {\n    color: #aaaaaa;\n    float: right;\n    font-size: 28px;\n    font-weight: bold;\n}\n\n.close:hover,\n.close:focus {\n    color: #000;\n    text-decoration: none;\n    cursor: pointer;\n}\n\nli>a {\n    color: #f2f2f2;\n    text-decoration: none;\n    font-weight: bold;\n}\n\nimg {\n    width: 100%;\n    background-color: #fff;\n    border-radius: 15%;\n}\n\nheader {\n    background-color: rgba(38, 37, 34, 0.69);\n    width: 100%;\n    height: 90px;\n    position: fixed;\n    padding: 10px;\n    z-index: 100;\n    -webkit-transition: padding 0.7s;\n    transition: padding 0.7s;\n}\n\n.navbar-brand span {\n    color: whitesmoke;\n    margin-left: 10px;\n    font-size: 17px;\n}\n\n.user_name {\n    font-size: 20px;\n    color: #15E95C;\n}\n\n#logo {\n    z-index: 1;\n    float: left;\n    width: 100px;\n    height: 100px;\n    background: url(\"/assets/images/brand.png\") no-repeat center;\n    display: block;\n}\n\n.navbar-brand img {\n    width: 60px;\n}\n\nnav {\n    float: right;\n    padding: 10px;\n}\n\n#menu-icon {\n    display: hidden;\n    width: 50px;\n    height: 50px;\n    background: #ffd701 url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAAH7+Yj7AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAKtJREFUeNpiYMACGEHEfyBgIAtg1Q4QQPjRfyggz0IU7QABRJozYboZgQCbCSBxJmJNZGKgNgAIIOojgr4GidHGM0RbPRQ8AxBAQyimkQMJOVCIyTEkR8mAupBpNFIGHwAIoFFE5WSDnESITTboSYbqOYVpNFJGI2U0UkYjhS4AIMBG0SgYdE05XCUPrpKFgg4vUU3B0YJmFIxmktFMMgpGM8loJhkFo4CmAABz6IAU+MUGeAAAAABJRU5ErkJggg==\") center;\n    cursor: pointer;\n}\n\na:hover#menu-icon {\n    background-color: #444;\n    border-radius: 4px 4px 0 0;\n}\n\nul {\n    list-style: none;\n}\n\nli {\n    display: inline-block;\n}\n\n.current {\n    color: #2262AD;\n}\n\nsection {\n    margin: 80px auto 40px;\n    max-width: 980px;\n    position: relative;\n    padding: 20px\n}\n\nh1 {\n    font-size: 20px;\n    color: #2262AD;\n    line-height: 1.15em;\n    margin: 20px 0;\n}\n\np {\n    line-height: 1.45em;\n    margin-bottom: 20px;\n}\n\n.forgetpw{\n    text-decoration: underline;\n    color: blue;\n    padding-left: 0;\n}\n\n@media only screen and (max-width: 970px) {\n    header {\n        position: fixed;\n    }\n    .navbar-brand span {\n        display: none;\n    }\n    #menu-icon {\n        display: inline-block;\n    }\n    nav ul,\n    nav:active ul {\n        display: none;\n        position: absolute;\n        padding: 20px;\n        background: #272733;\n        border: 5px solid #444;\n        right: 20px;\n        top: 60px;\n        width: 50%;\n        border-radius: 4px 0 4px 4px;\n    }\n    li>a {\n        color: #ffd701;\n    }\n    nav li {\n        text-align: center;\n        width: 100%;\n        padding: 10px 0;\n        margin: 0;\n    }\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "\n<!-- NV100 The Floating Nav Bar Itself -->\n<div (click)=\"close()\">\n  <header>\n    <span class=\"navbar-brand\">\n      <!-- Sailboat image -->\n      <img alt=\"Brand\" src=\"/assets/images/brand.png\">\n      <span>GPYC SAILING PROGRAM</span>\n    </span>\n    <!-- NV200 If the user is logged in, show the users name -->\n    <span class=\"user_name\" *ngIf=\"logged_user !== undefined \">Hi, {{user_log.identity}} {{logged_user}}</span>\n    <nav>\n      <span id=\"menu-icon\" (click)=\"click()\"></span>\n      <ul id=\"check_box_nav\">\n        <li>\n          <!-- NV300 If the admin_user is logged in (gpycwsp@gmail.com) -->\n            <button *ngIf=\"admin_user !== undefined\" type=\"submit\" class=\"btn btn-primary update-button\" (click)=\"display_code()\">\n                <i class=\"ion-android-boat\"></i>Welcome GPYC Admin, you are one kickass lady... Update Reg Code</button>\n        </li>\n        <li>\n          <!-- NV400 Update Profile Button = NV1200 Modal -->\n            <button *ngIf=\"logged_user !== undefined\" type=\"submit\" class=\"btn btn-primary update-button\" (click)=\"display_update()\">\n                <i class=\"ion-android-boat\"></i>Update Profile</button>\n        </li>\n        <li>\n          <!-- NV500 Scroll to Calendar Button -->\n          <button type=\"button\" class=\"btn btn-primary navbar-btn\" (click)=\"scrollTo()\">\n            <i class=\"icon ion-md-calendar\"></i> Calendar</button>\n        </li>\n        <li>\n           <!-- NV600 Scroll to Calendar Button -->\n          <button *ngIf=\"logged_user == undefined\" type=\"submit\" class=\"btn btn-primary\" (click)=\"display_reg()\">\n            <i class=\"ion-android-boat\"></i> Register</button>\n        </li>\n        <li>\n          <!-- NV700 Login Button -->\n          <button *ngIf=\"logged_user == undefined\" type=\"button\" class=\"btn btn-success\" (click)=\"display_login()\">\n            <i class=\"ion-person\"></i> Login</button>\n        </li>\n        <li>\n           <!-- NV800 Login Button -->\n          <button *ngIf=\"logged_user !== undefined \" type=\"button\" class=\"btn btn-danger\" (click)=\"logout()\">\n            <i class=\"ion-person\"></i> Logout</button>\n        </li>\n      </ul>\n    </nav>\n  </header>\n\n <!-- NVM900 Reg Form Modal -->\n  <div id=\"regForm\" class=\"modal\">\n    <div class=\"modal-content\">\n      <div style=\"text-align: center\">\n        <span id=\"reg1\">\n          <button>Register as Captain</button>\n        </span>\n        <span id=\"reg2\">\n          <button>Register as Student</button>\n        </span>\n      </div>\n\n      <form id=\"cap\" (submit)=\"capReg()\">\n        <h4>Register as Captain</h4>\n        <div class=\"form-group\">\n          <label for=\"\">Your Name</label>\n          <input type=\"text\" class=\"form-control\" name=\"name\" [(ngModel)]=\"captain_reg.name\" placeholder=\"Your name...\" #capName=\"ngModel\"\n            required minlength=\"3\">\n        </div>\n        <div class=\"alert alert-danger\" *ngIf=\"capName.invalid && (capName.dirty || capName.touched)\">\n          <strong>User name is required and must be 3 characters long.\n          </strong>\n        </div>\n\n        <div class=\"form-group\">\n          <label for=\"\">Email Address</label>\n          <input type=\"text\" class=\"form-control\" required pattern=\"[_a-z0-9]+(\\.[_a-z0-9]+)*@[a-z0-9-]*\\.([a-z]{2,4})\" name=\"email\"\n            [(ngModel)]=\"captain_reg.email\" placeholder=\"Your email...\" #capemail=\"ngModel\">\n        </div>\n\n        <div class=\"alert alert-danger\" *ngIf=\"capemail.invalid && (capemail.dirty || capemail.touched)\">\n          <strong>Email is required.</strong>\n        </div>\n        <div class=\"alert alert-danger\" *ngIf=\"err_message.email\">\n          <strong>{{err_message.email}}</strong>\n        </div>\n\n        <div class=\"form-group\">\n          <label for=\"\">Phone</label>\n          <input type=\"tel\" class=\"form-control\" name=\"phone\" [(ngModel)]=\"captain_reg.phone\" placeholder=\"Your Phone...\" required\n            #phone=\"ngModel\">\n        </div>\n\n        <div class=\"form-group\">\n          <label>Your Password</label>\n          <input type=\"password\" class=\"form-control\" required minlength=\"8\" name=\"stupassword\" [(ngModel)]=\"captain_reg.password\"\n            placeholder=\"Password...\" #reghash=\"ngModel\">\n        </div>\n        <div class=\"alert alert-danger\" *ngIf=\"reghash.invalid && (reghash.dirty || reghash.touched)\">\n          <strong>Password is required and must be 8 characters long.</strong>\n        </div>\n\n        <div class=\"form-group\">\n            <label>GPYC WSP Registration Code (Contact: gpycwsp@gmail.com)</label>\n            <input type=\"text\" class=\"form-control\" name=\"regcode\" [(ngModel)]=\"captain_reg.regcode\"\n              placeholder=\"GPYC Registration Code\">\n        </div>\n        <div class=\"alert alert-danger\" *ngIf=\"reghash.invalid && (reghash.dirty || reghash.touched)\">\n          <strong>The registration code is incorrect.</strong>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-danger\" (click)=\"closeReg()\">Close</button>\n          <button type=\"submit\" class=\"btn btn-info\">Submit</button>\n        </div>\n        <br/>\n      </form>\n \n      <!-- NVM1000 Register as Student Modal -->\n      <form id=\"student\" (submit)=\"studentReg()\">\n        <h4>Register as Student</h4>\n        <div class=\"form-group\">\n          <label for=\"\">Your Name</label>\n          <input type=\"text\" class=\"form-control\" name=\"name\" [(ngModel)]=\"student_reg.name\" placeholder=\"Your name...\" #studentName=\"ngModel\"\n            required minlength=\"3\">\n        </div>\n        <div class=\"alert alert-danger\" *ngIf=\"studentName.invalid && (studentName.dirty || studentName.touched)\">\n          <strong>User name is required and must be 3 characters long.\n          </strong>\n        </div>\n\n        <div class=\"form-group\">\n          <label for=\"\">Email Address</label>\n          <input type=\"text\" class=\"form-control\" required pattern=\"[_a-z0-9]+(\\.[_a-z0-9]+)*@[a-z0-9-]*\\.([a-z]{2,4})\" name=\"email\"\n            [(ngModel)]=\"student_reg.email\" placeholder=\"Your email...\" #email=\"ngModel\">\n        </div>\n        <div class=\"alert alert-danger\" *ngIf=\"email.invalid && (email.dirty || email.touched)\">\n          <strong>Email is required and checking the format.</strong>\n        </div>\n        <div class=\"alert alert-danger\" *ngIf=\"err_message.email\">\n          <strong>{{err_message.email}}</strong>\n        </div>\n\n        <div class=\"form-group\">\n          <label for=\"\">Phone</label>\n          <input type=\"tel\" class=\"form-control\" name=\"phone\" [(ngModel)]=\"student_reg.phone\" placeholder=\"Your Phone...\" required\n            #phone=\"ngModel\">\n        </div>\n\n        <div class=\"form-group\">\n          <label>Password</label>\n          <input type=\"password\" class=\"form-control\" required minlength=\"8\" name=\"stupassword\" [(ngModel)]=\"student_reg.password\"\n            placeholder=\"Password...\" #stuhash=\"ngModel\">\n        </div>\n        <div class=\"alert alert-danger\" *ngIf=\"stuhash.invalid && (stuhash.dirty || stuhash.touched)\">\n          <strong>Password is required and must be 8 characters long.</strong>\n        </div>\n\n        <div class=\"form-group\">\n            <label>GPYC WSP Registration Code (Contact: gpycwsp@gmail.com)</label>\n            <input type=\"text\" class=\"form-control\" name=\"regcode\" [(ngModel)]=\"student_reg.regcode\"\n              placeholder=\"GPYC Registration Code\">\n        </div>\n        <div class=\"alert alert-danger\" *ngIf=\"reghash.invalid && (reghash.dirty || reghash.touched)\">\n          <strong>The registration code is incorrect.</strong>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-danger\" (click)=\"closeReg()\">Close</button>\n          <button type=\"submit\" class=\"btn btn-info\">Submit</button>\n        </div>\n        <br/>\n      </form>\n    </div>\n  </div>\n\n<!-- NVM1100 Login Modal -->\n  <div id=\"loginForm\" class=\"modal\">\n    <!-- Modal content -->\n    <div class=\"modal-content\">\n      <form (submit)=\"login()\" #formLogin=\"ngForm\" id=\"login-form\" role=\"form\">\n\n        <div class=\"form-group\">\n          <label for=\"\">Your Email: </label>\n          <input type=\"text\" class=\"form-control\" id=\"login-username\" placeholder=\"Enter Email...\" name=\"email\" [(ngModel)]=\"user_log.email\"\n            #log_email=\"ngModel\" required>\n          <div *ngIf=\"log_email.invalid && (log_email.dirty || log_email.touched)\">\n            <div class=\"alert alert-dismissible alert-danger aler_log\" role=\"alert\">\n              <div *ngIf=\"log_email.errors\">\n                <strong>\n                  <span> Email is required.</span>\n                </strong>\n              </div>\n            </div>\n          </div>\n        </div>\n\n        <div class=\"form-group\">\n          <label>Password</label>\n          <input id=\"login-password\" type=\"password\" class=\"form-control\" placeholder=\"Enter Password...\" name=\"password\" [(ngModel)]=\"user_log.password\"\n            #log_password=\"ngModel\" required>\n          <!-- <p class=\"forgetpw\" [routerLink]=\"['/forgetpw']\">forgot password?</p> -->\n          <div *ngIf=\"log_password.invalid && (log_password.dirty || log_password.touched)\">\n            <div class=\"alert alert-dismissible alert-danger aler_log\" role=\"alert\">\n              <div *ngIf=\"log_password.errors\">\n                <strong>\n                  <span> Password is required.</span>\n                </strong>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-danger\" data-dismiss=\"modal\" (click)=\"closeLogin()\">Close</button>\n          <button id=\"btn-login\" [disabled]=\"formLogin.invalid\" data-toggle=\"modal\" class=\"btn btn-success\">Submit</button>\n        </div>\n        <div class=\"alert alert-dismissible alert-danger aler_log\" role=\"alert\" *ngIf=\"error_message.login != ''\">\n          {{error_message.login}}\n        </div>\n        <br/>\n      </form>\n    </div>\n  </div>\n\n  <app-header></app-header>\n  <app-display></app-display>\n  <app-calendar class=\"calendar\"></app-calendar>\n  <app-footer></app-footer>\n\n  <!-- NVM1200 Update Profile Modal -->\n  <div id=\"updateForm\" class=\"modal\">\n    <div class=\"modal-content\">\n      <h4>Want to Change From Student to Captain (or update your email)? Delete your student profile and create your new Captain profile!</h4>\n      <button class=\"btn btn-danger\" *ngIf=\"logged_user !== undefined\" (click)=\"deleteProfile()\">Delete Your Profile</button>\n      <br>\n      <button type=\"button\" class=\"btn btn-primary\" (click)=\"closeUpdate()\">Close</button>\n      \n    </div>\n  </div>\n\n  <!-- NVM1300 Update Regcode Modal -->\n<div id=\"codeForm\" class=\"modal\">\n    <div class=\"modal-content\">\n        <form (submit)=\"updateCode()\" #formCode=\"ngForm\" id=\"code-form\" role=\"form\">\n          <div class=\"form-group\">\n            <label for=\"\">The Old GPYC REG Code: </label>\n            <input type=\"text\" class=\"form-control\" id=\"oldregcode\" placeholder=\"Enter Old Reg Code\" name=\"oldregcode\" [(ngModel)]=\"regcode.oldregcode\">\n          </div>\n          <div class=\"form-group\">\n              <label for=\"\">The New GPYC REG Code: </label>\n              <input type=\"text\" class=\"form-control\" id=\"newregcode\" placeholder=\"Enter Old Reg Code\" name=\"newregcode\" [(ngModel)]=\"regcode.newregcode\">\n          </div>\n          <button class=\"btn btn-danger\" *ngIf=\"logged_user !== undefined\" (click)=\"updateCode()\">Change the GPYC Reg Code</button>\n          <button type=\"button\" class=\"btn btn-primary\" (click)=\"close_code()\">Close</button>\n          <div class=\"alert alert-danger\" *ngIf=\"successMessage !== undefined \">\n              {{regcode.message}} {{regcode.newregcode}}\n            </div>\n        </form>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/navbar/navbar.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var main_service_1 = __webpack_require__("../../../../../src/app/main.service.ts");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(_service, _router) {
        this._service = _service;
        this._router = _router;
        this.regcode = {
            oldregcode: '',
            newregcode: '',
            message: '',
            regcode: ''
        };
        this.captain_reg = {
            name: '',
            identity: 'captain',
            email: '',
            phone: '',
            address: [],
            experience: '',
            boat_name: '',
            spec: '',
            password: '',
            regcode: ''
        };
        this.student_reg = {
            name: '',
            identity: 'student',
            email: '',
            phone: '',
            password: '',
            regcode: ''
        };
        this.err_message = {
            email: '',
            password: ''
        };
        this.error_message = {
            email: '',
            login: '',
            code: 0
        };
        this.user_log = {
            email: '',
            password: '',
            identity: '',
        };
    }
    NavbarComponent.prototype.capReg = function () {
        var _this = this;
        console.log('cap reg component', this.captain_reg);
        this._service.registerCap(this.captain_reg, function (res) {
            if (res.success === 'register pending') {
                $('.modal-backdrop.show').hide();
                $('.modal').hide();
                _this._router.navigate(['/check_email']);
                _this.captain_reg = {
                    name: '',
                    identity: 'captain',
                    email: '',
                    phone: '',
                    address: [],
                    experience: '',
                    boat_name: '',
                    spec: '',
                    password: '',
                    regcode: ''
                };
                _this.pass_con = '';
            }
            else {
                _this.err_message.email = res.error;
            }
        });
    };
    NavbarComponent.prototype.getReg = function () {
        var _this = this;
        this._service.getRegcode(function (res) {
            if (res.error === undefined) {
                console.log('NO ERRORS!');
                _this.regcodeNow = res;
                console.log('REGCODENOW: ', _this.regcodeNow);
            }
            else {
                _this.error_message.login = res.error;
                if (res.errorCode !== undefined) {
                    _this.error_message.code = res.errorCode;
                }
            }
        });
    };
    NavbarComponent.prototype.studentReg = function () {
        var _this = this;
        console.log('STUDENTREG: ', this.student_reg);
        this._service.registerStudent(this.student_reg, function (res) {
            if (res.success === 'register pending') {
                $('.modal-backdrop.show').hide();
                $('.modal').hide();
                _this._router.navigate(['/check_email']);
            }
            else {
                _this.err_message.email = res.error;
            }
            _this.student_reg = {
                name: '',
                identity: 'student',
                email: '',
                phone: '',
                password: '',
                regcode: '',
            };
        });
    };
    NavbarComponent.prototype.ngOnInit = function () {
        var _this = this;
        var modal0 = document.getElementById('myModal0');
        var modal1 = document.getElementById('myModal1');
        var modal2 = document.getElementById('myModal2');
        var modal3 = document.getElementById('myModal3');
        var modal4 = document.getElementById('myModal4');
        var modal5 = document.getElementById('myModal5');
        var regform = document.getElementById('regForm');
        var loginForm = document.getElementById('loginForm');
        var updateForm = document.getElementById('updateForm');
        window.onclick = function (event) {
            console.log(1111);
            if (event.target === modal1 || event.target === modal2 || event.target === modal3
                || event.target === modal0 || event.target === regform ||
                event.target === loginForm || event.target === modal4 ||
                event.target === modal5 || event.target === updateForm) {
                console.log(2222);
                $('#myModal0').fadeOut();
                $('#myModal1').fadeOut();
                $('#myModal2').fadeOut();
                $('#myModal3').fadeOut();
                $('#myModal4').fadeOut();
                $('#myModal5').fadeOut();
                $('#regForm').fadeOut();
                $('#loginForm').fadeOut();
                $('#updateForm').fadeOut();
            }
        };
        if (this._service.currentUser !== null) {
            console.log('TRUE');
            console.log('THIS.SERVICE.CURRENTUSER: ', this._service.currentUser);
            this.logged_user = this._service.currentUser.name;
            this.user_log.email = this._service.currentUser.email;
            this.user_log.password = this._service.currentUser.password;
            if (this._service.currentUser.identity === 'captain') {
                this.user_log.identity = 'Captain';
            }
            else {
                this.user_log.identity = 'Crewman';
            }
            if (this.user_log.email === 'gpycwsp@gmail.com') {
                this.admin_user = this.logged_user;
                console.log('THIS.ADMIN_USER: ', this.admin_user);
            }
            console.log('THIS.USER_LOG.EMAIL: ', this.user_log.email);
        }
        $('#student').hide();
        $('#reg1').hide();
        $('#reg1').click(function () {
            $('#student').hide(100);
            $('#cap').show(100);
            $('#reg1').hide();
            $('#reg2').show();
        });
        $('#reg2').click(function () {
            $('#student').show(100);
            $('#cap').hide(100);
            $('#reg1').show();
            $('#reg2').hide();
        });
        this._service.loginstatus.subscribe(function (data) {
            if (data[0].mesg === 'reg') {
                _this.display_reg();
            }
            else if (data[0].mesg === 'log') {
                _this.display_login();
            }
        });
        this._service.scrollDownFromHeader.subscribe(function (res) {
            console.log(res);
            if (res.length > 0) {
                _this.scrollTo();
            }
        });
    };
    NavbarComponent.prototype.scrollTo = function () {
        $('html, body').animate({
            scrollTop: $('.calendar').offset().top
        }, 1000);
    };
    NavbarComponent.prototype.display_login = function () {
        $('#loginForm').fadeIn();
    };
    NavbarComponent.prototype.display_reg = function () {
        $('#regForm').fadeIn();
    };
    NavbarComponent.prototype.display_update = function () {
        $('#updateForm').fadeIn();
    };
    NavbarComponent.prototype.display_code = function () {
        console.log('NVT300 -> NVMT1300');
        $('#codeForm').fadeIn();
    };
    NavbarComponent.prototype.close_code = function () {
        $('#codeForm').fadeOut();
    };
    NavbarComponent.prototype.closeLogin = function () {
        $('#loginForm').fadeOut();
    };
    NavbarComponent.prototype.closeReg = function () {
        $('#regForm').fadeOut();
    };
    NavbarComponent.prototype.closeUpdate = function () {
        $('#updateForm').fadeOut();
    };
    NavbarComponent.prototype.login = function () {
        var _this = this;
        this._service.login(this.user_log, function (res) {
            if (res.error === undefined) {
                _this.logged_user = res.name;
                _this.closeLogin();
                _this._service.checkLogin.next(['loged']);
            }
            else {
                _this.error_message.login = res.error;
                if (res.errorCode !== undefined) {
                    _this.error_message.code = res.errorCode;
                }
            }
        });
        this.user_log = {
            email: '',
            password: '',
            identity: '',
        };
    };
    NavbarComponent.prototype.logout = function () {
        this._service.logout();
        this.logged_user = undefined;
        this.admin_user = undefined;
        var data = [{
                user: null,
                mesg: null
            }];
        this._service.updateLoginStatus(data);
        this._service.checkLogin.next(['logout']);
    };
    NavbarComponent.prototype.click = function () {
        if (this._service.show === true) {
            document.getElementById('check_box_nav').style.display = 'block';
            this._service.show = false;
        }
        else {
            document.getElementById('check_box_nav').style.removeProperty('display');
            this._service.show = true;
        }
    };
    NavbarComponent.prototype.close = function () {
        document.addEventListener('click', function (e) {
            var box = document.getElementById('check_box_nav');
            if (box.style.display === 'block') {
                if (e.target !== box && e.target !== document.getElementById('menu-icon')) {
                    box.style.removeProperty('display');
                }
            }
        });
        this._service.show = true;
    };
    NavbarComponent.prototype.deleteProfile = function () {
        var _this = this;
        console.log('click');
        console.log('this._service.currentUser.email: ', this._service.currentUser.email);
        this._service.delete_user(this._service.currentUser.email, function (res) {
            _this.close();
            _this.logout();
        });
    };
    NavbarComponent.prototype.updateCode = function () {
        var _this = this;
        console.log('NVMT1300, THIS.REGCODE: ', this.regcode);
        this._service.update_regcode(this.regcode, function (res) {
            if (res.error === undefined) {
                console.log('NO ERRORS!');
                _this.successMessage = true;
                _this.regcode.message = 'The Reg Code is now: ';
            }
            else {
                _this.error_message.login = res.error;
                if (res.errorCode !== undefined) {
                    _this.error_message.code = res.errorCode;
                }
            }
        });
        this.user_log = {
            email: '',
            password: '',
            identity: '',
        };
    };
    NavbarComponent = __decorate([
        core_1.Component({
            selector: 'app-navbar',
            template: __webpack_require__("../../../../../src/app/navbar/navbar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/navbar/navbar.component.css")]
        }),
        __metadata("design:paramtypes", [main_service_1.MainService, router_1.Router])
    ], NavbarComponent);
    return NavbarComponent;
}());
exports.NavbarComponent = NavbarComponent;


/***/ }),

/***/ "../../../../../src/app/reg-pending/reg-pending.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".centered form {\n    position: fixed;\n    top: 50%;\n    left: 50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%);\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/reg-pending/reg-pending.component.html":
/***/ (function(module, exports) {

module.exports = "\n<div class=\"centered\">\n\n        <form (submit)=\"login()\" #formLogin=\"ngForm\" id=\"login-form\" role=\"form\">\n          <h1>Hi {{user_name}},</h1><br><h2> Welcome back! Please log in.</h2>\n      \n          <div class=\"form-group\">\n            <label for=\"\">Your Email: </label>\n            <input type=\"text\" class=\"form-control\" id=\"login-username\" placeholder=\"Enter Email...\" name=\"email\" [(ngModel)]=\"user_log.email\"\n              #log_email=\"ngModel\" required>\n            <div *ngIf=\"log_email.invalid && (log_email.dirty || log_email.touched)\">\n              <div class=\"alert alert-dismissible alert-danger aler_log\" role=\"alert\">\n                <div *ngIf=\"log_email.errors.required\">\n                  <span> Email is required.</span>\n                </div>\n              </div>\n            </div>\n          </div>\n      \n          <div class=\"form-group\">\n            <label>Password</label>\n            <input id=\"login-password\" type=\"password\" class=\"form-control\" placeholder=\"Enter Password...\" name=\"password\" [(ngModel)]=\"user_log.password\"\n              #log_password=\"ngModel\" required>\n            <div *ngIf=\"log_password.invalid && (log_password.dirty || log_password.touched)\">\n              <div class=\"alert alert-dismissible alert-danger aler_log\" role=\"alert\">\n                <div *ngIf=\"log_password.errors.required\">\n                  <span> Password is required.</span>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"modal-footer\">\n            <button type=\"button\" class=\"btn btn-info\" *ngIf=\"error_message.code == 404\">Send Activation Email</button>\n            <button type=\"button\" class=\"btn btn-danger\" data-dismiss=\"modal\">Close</button>\n            <button id=\"btn-login\" [disabled]=\"formLogin.invalid\" class=\"btn btn-success\">Submit</button>\n          </div>\n          <div class=\"alert alert-dismissible alert-danger aler_log\" role=\"alert\" *ngIf=\"error_message.login != ''\">\n            {{error_message.login}}\n          </div>\n          <br/>\n        </form>\n      "

/***/ }),

/***/ "../../../../../src/app/reg-pending/reg-pending.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var main_service_1 = __webpack_require__("../../../../../src/app/main.service.ts");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var router_2 = __webpack_require__("../../../router/esm5/router.js");
var RegPendingComponent = /** @class */ (function () {
    function RegPendingComponent(_service, _router, _route) {
        this._service = _service;
        this._router = _router;
        this._route = _route;
        this.error_message = {
            email: '',
            login: '',
            code: 0
        };
        this.user_log = {
            email: '',
            password: ''
        };
    }
    RegPendingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route.paramMap.subscribe(function (params) {
            _this._service.getPendingUser(params.get('token'), function (res) {
                _this.user_name = res.user.name;
            });
        });
    };
    RegPendingComponent.prototype.login = function () {
        var _this = this;
        this._service.login(this.user_log, function (res) {
            if (res.error == undefined) {
                _this._service.checkLogin.next(['loged']);
                _this._router.navigate(['/']);
            }
            else {
                _this.error_message.login = res.error;
                if (res.errorCode != undefined) {
                    _this.error_message.code = res.errorCode;
                }
            }
        });
        this.user_log = {
            email: '',
            password: ''
        };
    };
    RegPendingComponent = __decorate([
        core_1.Component({
            selector: 'app-reg-pending',
            template: __webpack_require__("../../../../../src/app/reg-pending/reg-pending.component.html"),
            styles: [__webpack_require__("../../../../../src/app/reg-pending/reg-pending.component.css")]
        }),
        __metadata("design:paramtypes", [main_service_1.MainService, router_1.Router, router_2.ActivatedRoute])
    ], RegPendingComponent);
    return RegPendingComponent;
}());
exports.RegPendingComponent = RegPendingComponent;


/***/ }),

/***/ "../../../../../src/app/resetpassword/resetpassword.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".centered form {\n    position: fixed;\n    top: 50%;\n    left: 50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%);\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/resetpassword/resetpassword.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"centered\">\n    \n      <form (submit)=\"resetpw()\" id=\"login-form\" role=\"form\">\n        <h2>Please enter your new password:</h2>\n    \n        <div class=\"form-group\">\n          <label>Password</label>\n          <input id=\"login-password\" type=\"password\" class=\"form-control\" placeholder=\"Enter Password...\" required minlength=\"8\" #reghash=\"ngModel\" name=\"password\" [(ngModel)]=\"inputpassword\"\n            #log_password=\"ngModel\" required>\n          <div class=\"alert alert-danger\" *ngIf=\"reghash.invalid && (reghash.dirty || reghash.touched)\">\n            <strong>Password is required and must be 8 characters long.</strong>\n          </div>\n        </div>\n    \n        <div class=\"modal-footer\">\n          <button id=\"btn-login\" class=\"btn btn-success\">Submit</button>\n        </div>\n        <p *ngIf=\"passwordset==true\">Your password has been successful reset, you can now login with your new password.</p>\n        \n        <br/>\n      </form>\n    </div>"

/***/ }),

/***/ "../../../../../src/app/resetpassword/resetpassword.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var main_service_1 = __webpack_require__("../../../../../src/app/main.service.ts");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var ResetpasswordComponent = /** @class */ (function () {
    function ResetpasswordComponent(_service, _route, _router) {
        this._service = _service;
        this._route = _route;
        this._router = _router;
        this.error_message = null;
        this.passwordset = false;
    }
    ResetpasswordComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route.paramMap.subscribe(function (params) {
            _this.token = params.get("token");
        });
    };
    ResetpasswordComponent.prototype.resetpw = function () {
        var _this = this;
        console.log(this.inputpassword);
        this._route.paramMap.subscribe(function (params) {
            _this._service.resetpw(_this.token, _this.inputpassword, function (res) {
                _this._router.navigate(["/"]);
            });
        });
    };
    ResetpasswordComponent = __decorate([
        core_1.Component({
            selector: 'app-resetpassword',
            template: __webpack_require__("../../../../../src/app/resetpassword/resetpassword.component.html"),
            styles: [__webpack_require__("../../../../../src/app/resetpassword/resetpassword.component.css")]
        }),
        __metadata("design:paramtypes", [main_service_1.MainService, router_1.ActivatedRoute, router_1.Router])
    ], ResetpasswordComponent);
    return ResetpasswordComponent;
}());
exports.ResetpasswordComponent = ResetpasswordComponent;


/***/ }),

/***/ "../../../../../src/assets/images/confirmPic.png":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "confirmPic.c87970cce1cc178aa3ee.png";

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var platform_browser_dynamic_1 = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
var app_module_1 = __webpack_require__("../../../../../src/app/app.module.ts");
var environment_1 = __webpack_require__("../../../../../src/environments/environment.ts");
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(app_module_1.AppModule)
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ "../../../../moment/locale recursive ^\\.\\/.*$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "../../../../moment/locale/af.js",
	"./af.js": "../../../../moment/locale/af.js",
	"./ar": "../../../../moment/locale/ar.js",
	"./ar-dz": "../../../../moment/locale/ar-dz.js",
	"./ar-dz.js": "../../../../moment/locale/ar-dz.js",
	"./ar-kw": "../../../../moment/locale/ar-kw.js",
	"./ar-kw.js": "../../../../moment/locale/ar-kw.js",
	"./ar-ly": "../../../../moment/locale/ar-ly.js",
	"./ar-ly.js": "../../../../moment/locale/ar-ly.js",
	"./ar-ma": "../../../../moment/locale/ar-ma.js",
	"./ar-ma.js": "../../../../moment/locale/ar-ma.js",
	"./ar-sa": "../../../../moment/locale/ar-sa.js",
	"./ar-sa.js": "../../../../moment/locale/ar-sa.js",
	"./ar-tn": "../../../../moment/locale/ar-tn.js",
	"./ar-tn.js": "../../../../moment/locale/ar-tn.js",
	"./ar.js": "../../../../moment/locale/ar.js",
	"./az": "../../../../moment/locale/az.js",
	"./az.js": "../../../../moment/locale/az.js",
	"./be": "../../../../moment/locale/be.js",
	"./be.js": "../../../../moment/locale/be.js",
	"./bg": "../../../../moment/locale/bg.js",
	"./bg.js": "../../../../moment/locale/bg.js",
	"./bm": "../../../../moment/locale/bm.js",
	"./bm.js": "../../../../moment/locale/bm.js",
	"./bn": "../../../../moment/locale/bn.js",
	"./bn.js": "../../../../moment/locale/bn.js",
	"./bo": "../../../../moment/locale/bo.js",
	"./bo.js": "../../../../moment/locale/bo.js",
	"./br": "../../../../moment/locale/br.js",
	"./br.js": "../../../../moment/locale/br.js",
	"./bs": "../../../../moment/locale/bs.js",
	"./bs.js": "../../../../moment/locale/bs.js",
	"./ca": "../../../../moment/locale/ca.js",
	"./ca.js": "../../../../moment/locale/ca.js",
	"./cs": "../../../../moment/locale/cs.js",
	"./cs.js": "../../../../moment/locale/cs.js",
	"./cv": "../../../../moment/locale/cv.js",
	"./cv.js": "../../../../moment/locale/cv.js",
	"./cy": "../../../../moment/locale/cy.js",
	"./cy.js": "../../../../moment/locale/cy.js",
	"./da": "../../../../moment/locale/da.js",
	"./da.js": "../../../../moment/locale/da.js",
	"./de": "../../../../moment/locale/de.js",
	"./de-at": "../../../../moment/locale/de-at.js",
	"./de-at.js": "../../../../moment/locale/de-at.js",
	"./de-ch": "../../../../moment/locale/de-ch.js",
	"./de-ch.js": "../../../../moment/locale/de-ch.js",
	"./de.js": "../../../../moment/locale/de.js",
	"./dv": "../../../../moment/locale/dv.js",
	"./dv.js": "../../../../moment/locale/dv.js",
	"./el": "../../../../moment/locale/el.js",
	"./el.js": "../../../../moment/locale/el.js",
	"./en-au": "../../../../moment/locale/en-au.js",
	"./en-au.js": "../../../../moment/locale/en-au.js",
	"./en-ca": "../../../../moment/locale/en-ca.js",
	"./en-ca.js": "../../../../moment/locale/en-ca.js",
	"./en-gb": "../../../../moment/locale/en-gb.js",
	"./en-gb.js": "../../../../moment/locale/en-gb.js",
	"./en-ie": "../../../../moment/locale/en-ie.js",
	"./en-ie.js": "../../../../moment/locale/en-ie.js",
	"./en-il": "../../../../moment/locale/en-il.js",
	"./en-il.js": "../../../../moment/locale/en-il.js",
	"./en-nz": "../../../../moment/locale/en-nz.js",
	"./en-nz.js": "../../../../moment/locale/en-nz.js",
	"./eo": "../../../../moment/locale/eo.js",
	"./eo.js": "../../../../moment/locale/eo.js",
	"./es": "../../../../moment/locale/es.js",
	"./es-do": "../../../../moment/locale/es-do.js",
	"./es-do.js": "../../../../moment/locale/es-do.js",
	"./es-us": "../../../../moment/locale/es-us.js",
	"./es-us.js": "../../../../moment/locale/es-us.js",
	"./es.js": "../../../../moment/locale/es.js",
	"./et": "../../../../moment/locale/et.js",
	"./et.js": "../../../../moment/locale/et.js",
	"./eu": "../../../../moment/locale/eu.js",
	"./eu.js": "../../../../moment/locale/eu.js",
	"./fa": "../../../../moment/locale/fa.js",
	"./fa.js": "../../../../moment/locale/fa.js",
	"./fi": "../../../../moment/locale/fi.js",
	"./fi.js": "../../../../moment/locale/fi.js",
	"./fo": "../../../../moment/locale/fo.js",
	"./fo.js": "../../../../moment/locale/fo.js",
	"./fr": "../../../../moment/locale/fr.js",
	"./fr-ca": "../../../../moment/locale/fr-ca.js",
	"./fr-ca.js": "../../../../moment/locale/fr-ca.js",
	"./fr-ch": "../../../../moment/locale/fr-ch.js",
	"./fr-ch.js": "../../../../moment/locale/fr-ch.js",
	"./fr.js": "../../../../moment/locale/fr.js",
	"./fy": "../../../../moment/locale/fy.js",
	"./fy.js": "../../../../moment/locale/fy.js",
	"./gd": "../../../../moment/locale/gd.js",
	"./gd.js": "../../../../moment/locale/gd.js",
	"./gl": "../../../../moment/locale/gl.js",
	"./gl.js": "../../../../moment/locale/gl.js",
	"./gom-latn": "../../../../moment/locale/gom-latn.js",
	"./gom-latn.js": "../../../../moment/locale/gom-latn.js",
	"./gu": "../../../../moment/locale/gu.js",
	"./gu.js": "../../../../moment/locale/gu.js",
	"./he": "../../../../moment/locale/he.js",
	"./he.js": "../../../../moment/locale/he.js",
	"./hi": "../../../../moment/locale/hi.js",
	"./hi.js": "../../../../moment/locale/hi.js",
	"./hr": "../../../../moment/locale/hr.js",
	"./hr.js": "../../../../moment/locale/hr.js",
	"./hu": "../../../../moment/locale/hu.js",
	"./hu.js": "../../../../moment/locale/hu.js",
	"./hy-am": "../../../../moment/locale/hy-am.js",
	"./hy-am.js": "../../../../moment/locale/hy-am.js",
	"./id": "../../../../moment/locale/id.js",
	"./id.js": "../../../../moment/locale/id.js",
	"./is": "../../../../moment/locale/is.js",
	"./is.js": "../../../../moment/locale/is.js",
	"./it": "../../../../moment/locale/it.js",
	"./it.js": "../../../../moment/locale/it.js",
	"./ja": "../../../../moment/locale/ja.js",
	"./ja.js": "../../../../moment/locale/ja.js",
	"./jv": "../../../../moment/locale/jv.js",
	"./jv.js": "../../../../moment/locale/jv.js",
	"./ka": "../../../../moment/locale/ka.js",
	"./ka.js": "../../../../moment/locale/ka.js",
	"./kk": "../../../../moment/locale/kk.js",
	"./kk.js": "../../../../moment/locale/kk.js",
	"./km": "../../../../moment/locale/km.js",
	"./km.js": "../../../../moment/locale/km.js",
	"./kn": "../../../../moment/locale/kn.js",
	"./kn.js": "../../../../moment/locale/kn.js",
	"./ko": "../../../../moment/locale/ko.js",
	"./ko.js": "../../../../moment/locale/ko.js",
	"./ky": "../../../../moment/locale/ky.js",
	"./ky.js": "../../../../moment/locale/ky.js",
	"./lb": "../../../../moment/locale/lb.js",
	"./lb.js": "../../../../moment/locale/lb.js",
	"./lo": "../../../../moment/locale/lo.js",
	"./lo.js": "../../../../moment/locale/lo.js",
	"./lt": "../../../../moment/locale/lt.js",
	"./lt.js": "../../../../moment/locale/lt.js",
	"./lv": "../../../../moment/locale/lv.js",
	"./lv.js": "../../../../moment/locale/lv.js",
	"./me": "../../../../moment/locale/me.js",
	"./me.js": "../../../../moment/locale/me.js",
	"./mi": "../../../../moment/locale/mi.js",
	"./mi.js": "../../../../moment/locale/mi.js",
	"./mk": "../../../../moment/locale/mk.js",
	"./mk.js": "../../../../moment/locale/mk.js",
	"./ml": "../../../../moment/locale/ml.js",
	"./ml.js": "../../../../moment/locale/ml.js",
	"./mn": "../../../../moment/locale/mn.js",
	"./mn.js": "../../../../moment/locale/mn.js",
	"./mr": "../../../../moment/locale/mr.js",
	"./mr.js": "../../../../moment/locale/mr.js",
	"./ms": "../../../../moment/locale/ms.js",
	"./ms-my": "../../../../moment/locale/ms-my.js",
	"./ms-my.js": "../../../../moment/locale/ms-my.js",
	"./ms.js": "../../../../moment/locale/ms.js",
	"./mt": "../../../../moment/locale/mt.js",
	"./mt.js": "../../../../moment/locale/mt.js",
	"./my": "../../../../moment/locale/my.js",
	"./my.js": "../../../../moment/locale/my.js",
	"./nb": "../../../../moment/locale/nb.js",
	"./nb.js": "../../../../moment/locale/nb.js",
	"./ne": "../../../../moment/locale/ne.js",
	"./ne.js": "../../../../moment/locale/ne.js",
	"./nl": "../../../../moment/locale/nl.js",
	"./nl-be": "../../../../moment/locale/nl-be.js",
	"./nl-be.js": "../../../../moment/locale/nl-be.js",
	"./nl.js": "../../../../moment/locale/nl.js",
	"./nn": "../../../../moment/locale/nn.js",
	"./nn.js": "../../../../moment/locale/nn.js",
	"./pa-in": "../../../../moment/locale/pa-in.js",
	"./pa-in.js": "../../../../moment/locale/pa-in.js",
	"./pl": "../../../../moment/locale/pl.js",
	"./pl.js": "../../../../moment/locale/pl.js",
	"./pt": "../../../../moment/locale/pt.js",
	"./pt-br": "../../../../moment/locale/pt-br.js",
	"./pt-br.js": "../../../../moment/locale/pt-br.js",
	"./pt.js": "../../../../moment/locale/pt.js",
	"./ro": "../../../../moment/locale/ro.js",
	"./ro.js": "../../../../moment/locale/ro.js",
	"./ru": "../../../../moment/locale/ru.js",
	"./ru.js": "../../../../moment/locale/ru.js",
	"./sd": "../../../../moment/locale/sd.js",
	"./sd.js": "../../../../moment/locale/sd.js",
	"./se": "../../../../moment/locale/se.js",
	"./se.js": "../../../../moment/locale/se.js",
	"./si": "../../../../moment/locale/si.js",
	"./si.js": "../../../../moment/locale/si.js",
	"./sk": "../../../../moment/locale/sk.js",
	"./sk.js": "../../../../moment/locale/sk.js",
	"./sl": "../../../../moment/locale/sl.js",
	"./sl.js": "../../../../moment/locale/sl.js",
	"./sq": "../../../../moment/locale/sq.js",
	"./sq.js": "../../../../moment/locale/sq.js",
	"./sr": "../../../../moment/locale/sr.js",
	"./sr-cyrl": "../../../../moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "../../../../moment/locale/sr-cyrl.js",
	"./sr.js": "../../../../moment/locale/sr.js",
	"./ss": "../../../../moment/locale/ss.js",
	"./ss.js": "../../../../moment/locale/ss.js",
	"./sv": "../../../../moment/locale/sv.js",
	"./sv.js": "../../../../moment/locale/sv.js",
	"./sw": "../../../../moment/locale/sw.js",
	"./sw.js": "../../../../moment/locale/sw.js",
	"./ta": "../../../../moment/locale/ta.js",
	"./ta.js": "../../../../moment/locale/ta.js",
	"./te": "../../../../moment/locale/te.js",
	"./te.js": "../../../../moment/locale/te.js",
	"./tet": "../../../../moment/locale/tet.js",
	"./tet.js": "../../../../moment/locale/tet.js",
	"./tg": "../../../../moment/locale/tg.js",
	"./tg.js": "../../../../moment/locale/tg.js",
	"./th": "../../../../moment/locale/th.js",
	"./th.js": "../../../../moment/locale/th.js",
	"./tl-ph": "../../../../moment/locale/tl-ph.js",
	"./tl-ph.js": "../../../../moment/locale/tl-ph.js",
	"./tlh": "../../../../moment/locale/tlh.js",
	"./tlh.js": "../../../../moment/locale/tlh.js",
	"./tr": "../../../../moment/locale/tr.js",
	"./tr.js": "../../../../moment/locale/tr.js",
	"./tzl": "../../../../moment/locale/tzl.js",
	"./tzl.js": "../../../../moment/locale/tzl.js",
	"./tzm": "../../../../moment/locale/tzm.js",
	"./tzm-latn": "../../../../moment/locale/tzm-latn.js",
	"./tzm-latn.js": "../../../../moment/locale/tzm-latn.js",
	"./tzm.js": "../../../../moment/locale/tzm.js",
	"./ug-cn": "../../../../moment/locale/ug-cn.js",
	"./ug-cn.js": "../../../../moment/locale/ug-cn.js",
	"./uk": "../../../../moment/locale/uk.js",
	"./uk.js": "../../../../moment/locale/uk.js",
	"./ur": "../../../../moment/locale/ur.js",
	"./ur.js": "../../../../moment/locale/ur.js",
	"./uz": "../../../../moment/locale/uz.js",
	"./uz-latn": "../../../../moment/locale/uz-latn.js",
	"./uz-latn.js": "../../../../moment/locale/uz-latn.js",
	"./uz.js": "../../../../moment/locale/uz.js",
	"./vi": "../../../../moment/locale/vi.js",
	"./vi.js": "../../../../moment/locale/vi.js",
	"./x-pseudo": "../../../../moment/locale/x-pseudo.js",
	"./x-pseudo.js": "../../../../moment/locale/x-pseudo.js",
	"./yo": "../../../../moment/locale/yo.js",
	"./yo.js": "../../../../moment/locale/yo.js",
	"./zh-cn": "../../../../moment/locale/zh-cn.js",
	"./zh-cn.js": "../../../../moment/locale/zh-cn.js",
	"./zh-hk": "../../../../moment/locale/zh-hk.js",
	"./zh-hk.js": "../../../../moment/locale/zh-hk.js",
	"./zh-tw": "../../../../moment/locale/zh-tw.js",
	"./zh-tw.js": "../../../../moment/locale/zh-tw.js"
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "../../../../moment/locale recursive ^\\.\\/.*$";

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map